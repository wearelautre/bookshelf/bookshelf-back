# Bookshelf BACK
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=wearelautre_bookshelf-back&metric=alert_status)](https://sonarcloud.io/dashboard?id=wearelautre_bookshelf-back)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=wearelautre_bookshelf-back&metric=coverage)](https://sonarcloud.io/dashboard?id=wearelautre_bookshelf-back)

# Tools
## MVNW
### Update
`mvn -N io.takari:maven:0.7.7:wrapper`

# Environment var

## Covers storage
- __COVER_PATH__ : Folder where the covers saved will be stored  
- __COVER_SEARCH_PATH__ : Folder where the covers waiting to be saved will be stored

## Admin account
- __ADMIN_EMAIL__ : The email of the bookshelf admin

# Development
## Run dev database
__Prerequisite__: Docker should be in swarm mode, run `docker swarm init` to initialise the swarm

`export ENV=local && docker deploy --compose-file docker-compose-local.yml bookshelf-local`

# Launch the database
docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=bookshelf -p 3306:3306 -d mysql

# DB Backup
We use mysqldump to backup and restor the data.
## Backup
`docker exec <container_id> sh -c 'mysqldump -u root -p$MYSQL_ROOT_PASSWORD --databases bookshelf' > dump_bookshelf_yyyymmdd_v<app_version>.sql`
