-- @formatter:off
INSERT INTO role (id, name) VALUES (1, 'role 1');
INSERT INTO role (id, name) VALUES (2, 'role 2');
INSERT INTO role (id, name) VALUES (3, 'role 3');
INSERT INTO role (id, name) VALUES (4, 'role 4');
INSERT INTO role (id, name) VALUES (5, 'role 5');
INSERT INTO role (id, name) VALUES (6, 'role 6');
INSERT INTO role (id, name) VALUES (7, 'role 7');
INSERT INTO role (id, name) VALUES (8, 'role 8');
INSERT INTO role (id, name) VALUES (9, 'role 9');
INSERT INTO role (id, name) VALUES (10, 'role 10');

INSERT INTO book_type (id, name) VALUES (1, 'bookType 1');

INSERT INTO editor (id, name) VALUES (1, 'editor 1');

INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (1, 'series 1', 1, '2022-07-14 19:38:26', 1, false);

-- 0000000001111 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001111', 'title 1', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001111', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001111', null, 1, 1, null);
-- 0000000001112 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001112', 'title 2', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001112', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001112', null, 2, 1, null);

INSERT INTO artist (id, name) VALUES (1, 'artist 1');
INSERT INTO artist (id, name) VALUES (2, 'artist 2');
INSERT INTO artist (id, name) VALUES (3, 'artist 3');
INSERT INTO artist (id, name) VALUES (4, 'artist 4');

INSERT INTO contract (role_id, book_isbn) VALUE (1, '0000000001111');
INSERT INTO contract (role_id, book_isbn) VALUE (1, '0000000001112');
INSERT INTO contract (role_id, book_isbn) VALUE (2, '0000000001111');
INSERT INTO contract (role_id, book_isbn) VALUE (2, '0000000001112');
INSERT INTO contract (role_id, book_isbn) VALUE (3, '0000000001111');
INSERT INTO contract (role_id, book_isbn) VALUE (3, '0000000001112');

INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (2, 1, '0000000001111');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (2, 1, '0000000001112');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (1, 1, '0000000001111');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (1, 1, '0000000001112');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (3, 1, '0000000001111');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (3, 1, '0000000001112');

INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (1, 2, '0000000001111');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (1, 2, '0000000001112');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (2, 2, '0000000001111');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (2, 2, '0000000001112');

INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (1, 3, '0000000001111');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (2, 3, '0000000001111');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (3, 3, '0000000001111');
INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (3, 3, '0000000001112');

INSERT INTO contract_artists (contracts_role_id, artists_id, contracts_book_isbn) VALUES (2, 4, '0000000001111');