-- @formatter:off
INSERT INTO book_type (id, name) VALUES (1, 'bookType 1');

INSERT INTO editor (id, name) VALUES (1, 'editor 1');

INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (1, 'series 1', 1, '2022-07-14 19:38:26', 1, false);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (2, 'series 2', 1, '2022-07-14 19:38:26', 1, true);

INSERT INTO series_cycle (id, name, series_id) VALUES (1, 'series cycle 1', 1);
INSERT INTO series_cycle (id, name, series_id) VALUES (2, 'series cycle 2', 1);
INSERT INTO series_cycle (id, name, series_id) VALUES (3, 'series cycle 3', 2);
INSERT INTO series_cycle (id, name, series_id) VALUES (4, 'series cycle 4', 2);
