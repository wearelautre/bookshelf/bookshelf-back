-- @formatter:off
INSERT INTO book_type (id, name) VALUES (1, 'bookType 1');

INSERT INTO editor (id, name) VALUES (1, 'editor 1');

INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (1, 'series 1', 1, '2022-07-14 19:38:26', 1, false);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (2, 'series 2', 1, '2022-07-14 19:38:26', 1, true);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (3, 'series 3', 1, '2022-07-14 19:38:26', 1, false);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (4, 'series 4', 1, '2022-07-14 19:38:26', 1, true);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (5, 'series 5', 1, '2022-07-14 19:38:26', 1, false);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (6, 'series 6', 1, '2022-07-14 19:38:26', 1, true);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (7, 'series 7', 1, '2022-07-14 19:38:26', 1, false);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (8, 'series 8', 1, '2022-07-14 19:38:26', 1, true);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (9, 'series 9', 1, '2022-07-14 19:38:26', 1, false);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (10, 'series 10', 1, '2022-07-14 19:38:26', 1, true);
