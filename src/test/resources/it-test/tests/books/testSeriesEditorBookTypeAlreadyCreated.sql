-- @formatter:off
INSERT INTO book_type (id, name) VALUES (10, 'bookType 1');
INSERT INTO editor (id, name) VALUES (10, 'editor 1');
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (10, 'series 1', 10, '2022-07-14 19:38:26', 10, false);
