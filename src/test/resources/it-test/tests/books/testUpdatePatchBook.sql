-- @formatter:off
INSERT INTO book_type (id, name) VALUES (1, 'bookType 1');
INSERT INTO editor (id, name) VALUES (1, 'editor 1');
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (1, 'series 1', 1, '2022-07-14 19:38:26', 1, false);
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000000000', 'title 1', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000000000', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000000000', null, 1, 1, null);

