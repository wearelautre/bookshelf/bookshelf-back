-- @formatter:off
INSERT INTO book_type (id, name) VALUES (1, 'bookType 1');
INSERT INTO book_type (id, name) VALUES (2, 'bookType 2');

INSERT INTO editor (id, name) VALUES (1, 'editor 1');
INSERT INTO editor (id, name) VALUES (2, 'editor 2');

INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot, published_count, published_status) VALUES (1, 'series 1', 1, '2022-07-14 19:38:26', 1, false, 5, 'UNKNOWN');
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot, published_count, published_status) VALUES (2, 'series 2', 2, '2022-07-14 19:38:26', 2, false, 5, 'UNKNOWN');

-- 0000000000000 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001111', 'title 1', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001111', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001111', null, 1, 1, null);
-- 0000000000001 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001112', 'title 2', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001112', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001112', null, 2, 1, null);
-- 0000000000002 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001113', 'title 3', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001113', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001113', null, 3, 1, null);
-- 0000000000003 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001114', 'title 4', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001114', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001114', null, 4, 1, null);
-- 0000000000004 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001115', 'title 5', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001115', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001115', null, 5, 1, null);

-- 0000000000010 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000002221', 'title 1', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000002221', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002221', null, 1, 2, null);
-- 0000000000011 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000002222', 'title 2', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000002222', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002222', null, 2, 2, null);
-- 0000000000012 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000002223', 'title 3', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000002223', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002223', null, 3, 2, null);
-- 0000000000013 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000002224', 'title 4', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000002224', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002224', null, 4, 2, null);
-- 0000000000014 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000002225', 'title 5', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000002225', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002225', null, 5, 2, null);
