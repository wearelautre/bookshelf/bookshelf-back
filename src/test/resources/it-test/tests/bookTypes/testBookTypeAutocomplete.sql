-- @formatter:off
INSERT INTO book_type (id, name) VALUES (1, 'bookType 1');
INSERT INTO book_type (id, name) VALUES (2, 'bookType 2');
INSERT INTO book_type (id, name) VALUES (3, 'bookType 3');
INSERT INTO book_type (id, name) VALUES (4, 'bookType 4');
INSERT INTO book_type (id, name) VALUES (5, 'bookType 5');
INSERT INTO book_type (id, name) VALUES (6, 'bookType 6');
INSERT INTO book_type (id, name) VALUES (7, 'bookType 7');
INSERT INTO book_type (id, name) VALUES (8, 'bookType 8');
INSERT INTO book_type (id, name) VALUES (9, 'bookType 9');
INSERT INTO book_type (id, name) VALUES (10, 'bookType 10');

INSERT INTO editor (id, name) VALUES (1, 'editor 1');

INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (1, 'series 1', 1, '2022-07-14 19:38:26', 5, false);
