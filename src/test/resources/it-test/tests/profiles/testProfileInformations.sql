-- @formatter:off
INSERT INTO book_type (id, name) VALUES (1, 'bookType 1');
INSERT INTO book_type (id, name) VALUES (2, 'bookType 2');
INSERT INTO book_type (id, name) VALUES (3, 'bookType 3');

INSERT INTO editor (id, name) VALUES (1, 'editor 1');
INSERT INTO editor (id, name) VALUES (2, 'editor 2');
INSERT INTO editor (id, name) VALUES (3, 'editor 3');

INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot, published_count) VALUES (1, 'series 1', 1, '2022-07-14 19:38:26', 1, false, 5);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot, published_count) VALUES (2, 'series 2', 2, '2022-07-14 19:38:26', 2, false, 9);
INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot, published_count) VALUES (3, 'series 3', 3, '2022-07-14 19:38:26', 3, false, 6);

-- 0000000000000 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:01', '0000000001111', 'READ', 'title 1', '2022', '2022-07-14 00:00:01', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000001111', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001111', null, 1, 1, null);
-- 0000000000001 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:02', '0000000001112', 'UNREAD', 'title 2', '2022', '2022-07-14 00:00:02', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000001112', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001112', null, 2, 1, null);
-- 0000000000002 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:03', '0000000001113', 'UNREAD', 'title 3', '2022', '2022-07-14 00:00:03', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000001113', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001113', null, 3, 1, null);
-- 0000000000003 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:04', '0000000001114', 'UNREAD', 'title 4', '2022', '2022-07-14 00:00:04', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000001114', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001114', null, 4, 1, null);
-- 0000000000004 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:05', '0000000001115', 'UNREAD', 'title 5', '2022', '2022-07-14 00:00:05', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000001115', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001115', null, 5, 1, null);

-- 0000000000010 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:11', '0000000002221', 'READ', 'title 1', '2022', '2022-07-14 00:00:11', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000002221', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002221', null, 1, 2, null);
-- 0000000000011 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:12', '0000000002222', 'READ', 'title 2', '2022', '2022-07-14 00:00:12', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000002222', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002222', null, 2, 2, null);
-- 0000000000012 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:13', '0000000002223', 'UNREAD', 'title 3', '2022', '2022-07-14 00:00:13', null, 'SOLD', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000002223', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002223', null, 3, 2, null);
-- 0000000000013 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:14', '0000000002224', 'UNREAD', 'title 4', '2022', '2022-07-14 00:00:14', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000002224', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002224', null, 4, 2, null);
-- 0000000000014 series 2
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:15', '0000000002225', 'UNREAD', 'title 5', '2022', '2022-07-14 00:00:15', null, 'POSSESSED', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000002225', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000002225', null, 5, 2, null);

-- 0000000000000 series 3
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, status, title, year, last_update_date, current_loan_id, possession_status, list_type) VALUES ('manual-test', null, null, '2022-07-14 00:00:01', '0000000003331', 'UNREAD', 'title 1', '2022', '2022-07-14 00:00:01', null, 'SOLD', 'LIBRARY');
INSERT INTO book_metadata (book_isbn, acquisition_date, offered, original_release_date, page_count, price, price_currency) VALUES ('0000000003331', null, false, null, 0, null, null);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000003331', null, 1, 3, null);