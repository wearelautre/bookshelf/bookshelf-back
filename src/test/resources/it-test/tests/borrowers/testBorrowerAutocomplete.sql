-- @formatter:off
INSERT INTO borrower (id, name) VALUES (1, 'borrower 1');
INSERT INTO borrower (id, name) VALUES (2, 'borrower 2');
INSERT INTO borrower (id, name) VALUES (3, 'borrower 3');
INSERT INTO borrower (id, name) VALUES (4, 'borrower 4');
INSERT INTO borrower (id, name) VALUES (5, 'borrower 5');
INSERT INTO borrower (id, name) VALUES (6, 'borrower 6');
INSERT INTO borrower (id, name) VALUES (7, 'borrower 7');
INSERT INTO borrower (id, name) VALUES (8, 'borrower 8');
INSERT INTO borrower (id, name) VALUES (9, 'borrower 9');
INSERT INTO borrower (id, name) VALUES (10, 'borrower 10');

INSERT INTO account (id, email, login, role) VALUES (2, 'account@log.com', 'account', 'ROLE_ADMIN');

INSERT INTO book_type (id, name) VALUES (1, 'bookType 1');

INSERT INTO editor (id, name) VALUES (1, 'editor 1');

INSERT INTO series (id, name, editor_id, last_read_book_date, book_type_id, one_shot) VALUES (1, 'series 1', 1, '2022-07-14 19:38:26', 1, false);

-- 0000000001111 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001111', 'title 1', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001111', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001111', null, 1, 1, null);
-- 0000000001112 series 1
INSERT INTO book (ark_id, collection, cover, creation_date, isbn, title, year, last_update_date) VALUES ('manual-test', null, null, '2022-07-14 00:00:00', '0000000001112', 'title 2', '2022', '2022-07-14 00:00:00');
INSERT INTO book_metadata (book_isbn, page_count) VALUES ('0000000001112', 0);
INSERT INTO book_series (book_isbn, cycle_tome, series_tome, series_id, series_cycle_id) VALUES ('0000000001112', null, 2, 1, null);

INSERT INTO library_item (id, account_id, isbn, possession_status, status) VALUES (1, 2, '0000000001111', 'POSSESSED', 'UNREAD');
INSERT INTO library_item (id, account_id, isbn, possession_status, status) VALUES (2, 2, '0000000001112', 'POSSESSED', 'UNREAD');

INSERT INTO loan (id, borrow_date, return_date, library_item_id, borrower_id) VALUEs (1, '2022-07-14', null, 1, 1);
INSERT INTO loan (id, borrow_date, return_date, library_item_id, borrower_id) VALUEs (2, '2022-07-14', null, 2, 1);

