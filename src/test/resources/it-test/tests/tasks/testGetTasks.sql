-- @formatter:off
INSERT INTO task (id, create_date, extra, status, type) VALUES (1, '2022-07-14 02:00:00', 'extra 1', 'TODO', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (2, '2022-07-14 02:00:00', 'extra 2', 'TODO', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (3, '2022-07-14 02:00:00', 'extra 3', 'TODO', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (4, '2022-07-14 02:00:00', 'extra 4', 'TODO', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (5, '2022-07-14 02:00:00', 'extra 5', 'DONE', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (6, '2022-07-14 02:00:00', 'extra 6', 'DONE', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (7, '2022-07-14 02:00:00', 'extra 7', 'DONE', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (8, '2022-07-14 02:00:00', 'extra 8', 'DONE', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (9, '2022-07-14 02:00:00', 'extra 9', 'DONE', 'ADD_BOOK');
INSERT INTO task (id, create_date, extra, status, type) VALUES (10, '2022-07-14 02:00:00', 'extra 10', 'DONE', 'ADD_BOOK');

INSERT INTO task (id, create_date, extra, status, type) VALUES (11, '2022-07-14 02:00:00', 'extra 11', 'TODO', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (12, '2022-07-14 02:00:00', 'extra 12', 'TODO', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (13, '2022-07-14 02:00:00', 'extra 13', 'TODO', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (14, '2022-07-14 02:00:00', 'extra 14', 'TODO', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (15, '2022-07-14 02:00:00', 'extra 15', 'DONE', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (16, '2022-07-14 02:00:00', 'extra 16', 'DONE', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (17, '2022-07-14 02:00:00', 'extra 17', 'DONE', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (18, '2022-07-14 02:00:00', 'extra 18', 'DONE', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (19, '2022-07-14 02:00:00', 'extra 19', 'DONE', 'OTHER');
INSERT INTO task (id, create_date, extra, status, type) VALUES (20, '2022-07-14 02:00:00', 'extra 20', 'DONE', 'OTHER');

