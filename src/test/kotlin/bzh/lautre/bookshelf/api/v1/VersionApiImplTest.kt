package bzh.lautre.bookshelf.api.v1

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.testcontainers.junit.jupiter.Testcontainers

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@Testcontainers
@TestPropertySource("classpath:git.properties")
internal class  VersionApiImplTest @Autowired constructor(
    val mockMvc: MockMvc
) {
    val baseUrl = "/version"

    @Nested
    @DisplayName("GET /version")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetVersion {
        @Test
        fun `should return the current version`() {
         mockMvc.get(baseUrl)
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                    }
                    jsonPath("$.projectVersion") {value("test") }
                    jsonPath("$.pipelineId") { isString() }
                    jsonPath("$.jobId") { isString() }
                    jsonPath("$.commitSha") {value("test") }
                }
        }
    }
}
