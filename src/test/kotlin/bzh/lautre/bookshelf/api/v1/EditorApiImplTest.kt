package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.model.EditorDTO
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.DaoTestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
class EditorApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper,
) {
    val baseUrl = "/editors"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    private fun mapToEditors(index: Long): EditorDTO {
        return EditorDTO(
            name = "editor $index",
            id = index,
        )
    }

    @Nested
    @DisplayName("GET /editors")
    inner class GetEditors {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 editors without search`() {
            DaoTestUtils.executeScripts(arrayOf("editors/testGet1stPageOf5Editors.sql"))

            val editors = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToEditors(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(editors), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get second page of 5 editors without search`() {
            DaoTestUtils.executeScripts(arrayOf("editors/testGet1stPageOf5Editors.sql"))

            val editors = mutableListOf<Long>(5, 6, 7, 8, 9).map { mapToEditors(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "1"
            parameters["size"] = "5"
            parameters["direction"] = "ASC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(editors), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 editors without search order desc`() {
            DaoTestUtils.executeScripts(arrayOf("editors/testGet1stPageOf5Editors.sql"))

            val editors = mutableListOf<Long>(9, 8, 7, 6, 5).map { mapToEditors(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["direction"] = "DESC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(editors), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 editors with 'or' search on name`() {
            DaoTestUtils.executeScripts(arrayOf("editors/testGet1stPageOf5Editors.sql"))

            val editors = mutableListOf<Long>(2, 9).map { mapToEditors(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*2|'name:*9"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(editors), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 editors with search on name`() {
            DaoTestUtils.executeScripts(arrayOf("editors/testGet1stPageOf5Editors.sql"))

            val editors = mutableListOf<Long>(6).map { mapToEditors(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*6"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(editors), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /editors/autocomplete")
    inner class GetEditorsAutoComplete {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 editors autocomplete no filter`() {
            DaoTestUtils.executeScripts(arrayOf("editors/testGet1stPageOf5Editors.sql"))

            val editors = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToEditors(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(editors), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 1 editors autocomplete with filter on name`() {
            DaoTestUtils.executeScripts(arrayOf("editors/testGet1stPageOf5Editors.sql"))

            val editors = mutableListOf<Long>(3).map { mapToEditors(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["search"] = "name:*3"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(editors), true)
                    }
                }
        }
    }

}
