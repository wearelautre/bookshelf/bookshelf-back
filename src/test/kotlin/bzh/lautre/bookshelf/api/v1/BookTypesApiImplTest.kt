package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.model.BookTypeDTO
import bzh.lautre.bookshelf.api.v1.model.ErrorDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalBookTypeDTO
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.DaoTestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.*
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
class BookTypesApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper,
) {
    val baseUrl = "/bookTypes"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    private fun mapToBookType(index: Long): BookTypeDTO {
        return BookTypeDTO(index, "bookType $index", 0)
    }

    private fun mapToMinimalBookTypes(index: Long): MinimalBookTypeDTO {
        return MinimalBookTypeDTO(index, "bookType $index")
    }

    @Nested
    @DisplayName("GET /bookTypes")
    inner class GetBookTypes {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 bookTypes without search`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testGet1stPageOf5BookTypes.sql"))

            val bookTypes = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToBookType(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(bookTypes), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get second page of 5 bookTypes without search`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testGet1stPageOf5BookTypes.sql"))

            val bookTypes = mutableListOf<Long>(5, 6, 7, 8, 9).map { mapToBookType(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "1"
            parameters["size"] = "5"
            parameters["direction"] = "ASC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(bookTypes), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 bookTypes without search order desc`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testGet1stPageOf5BookTypes.sql"))

            val bookTypes = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToBookType(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["direction"] = "DESC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(bookTypes), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 bookTypes with 'or' search on name`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testGet1stPageOf5BookTypes.sql"))

            val bookTypes = mutableListOf<Long>(2, 9).map { mapToBookType(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*2|'name:*9"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(bookTypes), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 bookTypes with search on name`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testGet1stPageOf5BookTypes.sql"))

            val bookTypes = mutableListOf<Long>(6).map { mapToBookType(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*6"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(bookTypes), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("POST /bookTypes")
    inner class PostBookTypes {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = BookTypeDTO(id = null, "bookType 1", 36)
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = BookTypeDTO(id = null, "bookType 1", 36)
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should create a bookType`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookTypeDTO(id = null, "bookType 1", 36))
            }
                .andExpect {
                    status { isCreated() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(BookTypeDTO(id = 1, "bookType 1", 0)), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not recreate a bookType with same name will just return the already created bookType`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testBookTypeCreate.sql"))

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookTypeDTO(id = null, "bookType 4", 36))
            }
                .andExpect {
                    status { isCreated() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(BookTypeDTO(id = 4, "bookType 4", 2)), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /bookTypes/autocomplete")
    inner class GetBookTypesAutoComplete {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 bookTypes autocomplete no filter`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testBookTypeAutocomplete.sql"))

            val bookTypes = mutableListOf<Long>(9, 8, 7, 6, 5).map { mapToMinimalBookTypes(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(bookTypes), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 1 bookType autocomplete with filter on name`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testBookTypeAutocomplete.sql"))

            val bookTypes = mutableListOf<Long>(3).map { mapToMinimalBookTypes(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["search"] = "name:*3"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(bookTypes), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /bookTypes/{id}")
    inner class GetBookTypeById {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get an bookType by id`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testBookTypeAutocomplete.sql"))

            mockMvc.get("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BookTypeDTO(
                                    5,
                                    "bookType 5",
                                    nbSeries = 1
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get not get bookType by id not found`() {

            mockMvc.get("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BOOK_TYPE 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("PUT /bookTypes/{id}")
    inner class PutBookType {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.put("$baseUrl/5") {
                content = BookTypeDTO(5, "new bookType 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.put("$baseUrl/5") {
                content = BookTypeDTO(5, "new bookType 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not update an bookType path id and body id not same `() {
            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    BookTypeDTO(
                        6,
                        "new bookType 5"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an bookType by id`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testBookTypeUpdate.sql"))

            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    BookTypeDTO(
                        5,
                        "new bookType 5"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BookTypeDTO(
                                    5,
                                    "new bookType 5",
                                    nbSeries = 0
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an bookType by id with weblinks already existing`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testBookTypeUpdate.sql"))

            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    BookTypeDTO(
                        5, "new bookType 5"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BookTypeDTO(
                                    5, "new bookType 5",
                                    nbSeries = 0
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an bookType by id not found`() {

            mockMvc.put("$baseUrl/1") {
                content = objectMapper.writeValueAsString(
                    BookTypeDTO(
                        1, "new bookType 1"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }

                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BOOK_TYPE 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("DELETE /bookTypes/{id}")
    inner class DeleteBookType {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.delete("$baseUrl/5") {
                content = BookTypeDTO(5, "new bookType 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.delete("$baseUrl/5") {
                content = BookTypeDTO(5, "new bookType 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an bookType by id`() {
            DaoTestUtils.executeScripts(arrayOf("bookTypes/testBookTypeUpdate.sql"))

            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an bookType by id not found`() {

            mockMvc.delete("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BOOK_TYPE 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }
}
