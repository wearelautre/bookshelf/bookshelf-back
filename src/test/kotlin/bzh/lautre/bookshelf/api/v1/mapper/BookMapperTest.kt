package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.business.EditorBusiness
import org.junit.jupiter.api.BeforeEach
import org.mapstruct.factory.Mappers
import org.mockito.Mockito
import java.lang.reflect.Constructor


internal class BookMapperTest {

    lateinit var bookMapper: BookMapper

    @BeforeEach
    fun init() {
        val constructor: Constructor<out BookMapper> = Mappers.getMapperClass(BookMapper::class.java)
            .getConstructor(
                SeriesMapper::class.java,
                RoleMapper::class.java,
                ArtistMapper::class.java,
                EditorMapper::class.java
            )
        bookMapper = constructor
            .newInstance(
                Mockito.mock(SeriesMapper::class.java),
                Mockito.mock(RoleMapper::class.java),
                Mockito.mock(ArtistMapper::class.java),
                Mockito.mock(EditorMapper::class.java)
            )

        bookMapper.editorBusiness = Mockito.mock(EditorBusiness::class.java)
    }
}