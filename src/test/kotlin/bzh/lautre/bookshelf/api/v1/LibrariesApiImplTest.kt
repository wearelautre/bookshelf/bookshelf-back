package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.TestSecurityConfig
import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.BooksDescription
import bzh.lautre.bookshelf.utils.BooksSeriesDescription
import bzh.lautre.bookshelf.utils.DaoTestUtils
import bzh.lautre.bookshelf.utils.LibrariesItemsDescription
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.http.HttpHeaders
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.*
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers
import java.math.BigDecimal


@ActiveProfiles("test")
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [TestSecurityConfig::class])
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
class LibrariesApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper,
) {
    val baseUrl = "/libraries/{accountId}"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    fun mapToMinimalLibrariesItem(item: LibrariesItemsDescription): MinimalLibraryItemDTO {
        return MinimalLibraryItemDTO(
            id = item.id,
            status = MinimalLibraryItemDTO.Status.valueOf(item.status),
            possessionStatus = MinimalLibraryItemDTO.PossessionStatus.valueOf(item.possessionStatus),
            book = mapToBook(item.book),
        )
    }

    fun mapToLibrariesItem(item: LibrariesItemsDescription): LibraryItemDTO {
        return LibraryItemDTO(
            id = item.id,
            status = LibraryItemDTO.Status.valueOf(item.status),
            possessionStatus = LibraryItemDTO.PossessionStatus.valueOf(item.possessionStatus),
            book = mapToBook(item.book),
            lent = false,
            borrower = null,
            metadata = null
        )
    }

    fun mapToLibrariesItemWithCover(item: LibrariesItemsDescription): MinimalLibraryItemWithCoverDTO {
        return MinimalLibraryItemWithCoverDTO(
            id = item.id,
            status = MinimalLibraryItemWithCoverDTO.Status.valueOf(item.status),
            possessionStatus = MinimalLibraryItemWithCoverDTO.PossessionStatus.valueOf(item.possessionStatus),
            book = mapToBookWithCover(item.book),
        )
    }

    fun mapToBook(book: BooksDescription): MinimalBookDTO {
        val series = BookSeriesDTO(
            bookType = MinimalBookTypeDTO(name = "bookType ${book.bookType}", id = book.bookType),
            tome = book.tome,
            seriesCycle = null,
            name = "series ${book.series.id}",
            status = SeriesStatusDTO(
                totalCount = book.series.total,
                status = SeriesStatusDTO.Status.valueOf(book.series.status)
            ),
            id = book.series.id,
            oneShot = false,
        )

        return MinimalBookDTO(
            editor = MinimalEditorDTO(name = "editor ${book.editor}"),
            title = "title ${book.tome}",
            isbn = "000000000${book.editor}${book.series.id}${book.bookType}${book.tome}",
            series = series,
        )
    }

    fun mapToBookWithCover(book: BooksDescription): MinimalBookWithCoverDTO {
        val series = BookSeriesDTO(
            bookType = MinimalBookTypeDTO(name = "bookType ${book.bookType}", id = book.bookType),
            tome = book.tome,
            seriesCycle = null,
            name = "series ${book.series.id}",
            status = SeriesStatusDTO(
                totalCount = book.series.total,
                status = SeriesStatusDTO.Status.valueOf(book.series.status)
            ),
            id = book.series.id,
            oneShot = false,
        )

        return MinimalBookWithCoverDTO(
            editor = MinimalEditorDTO(name = "editor ${book.editor}"),
            title = "title ${book.tome}",
            isbn = "000000000${book.editor}${book.series.id}${book.bookType}${book.tome}",
            series = series,
            cover = null,
        )
    }

    @Nested
    @DisplayName("GET /libraries/{accountId}/items")
    inner class GetLibrariesItems {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 items without search`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val librariesItems = mutableListOf(
                LibrariesItemsDescription(1, BooksDescription(1), "READ", "POSSESSED"),
                LibrariesItemsDescription(4, BooksDescription(2), "UNREAD", "NOT_POSSESSED"),
                LibrariesItemsDescription(5, BooksDescription(3), "UNREAD", "POSSESSED"),
                LibrariesItemsDescription(6, BooksDescription(5), "UNREAD", "POSSESSED"),
                LibrariesItemsDescription(
                    7,
                    BooksDescription(1, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
            ).map { mapToMinimalLibrariesItem(it) }


            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/items") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(librariesItems), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 items with search -- possessionStatus`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val librariesItems = mutableListOf(
                LibrariesItemsDescription(4, BooksDescription(2), "UNREAD", "NOT_POSSESSED"),
            ).map { mapToMinimalLibrariesItem(it) }


            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            parameters["search"] = "possessionStatus:NOT_POSSESSED"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/items") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(librariesItems), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 items with search -- status`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val librariesItems = mutableListOf(
                LibrariesItemsDescription(4, BooksDescription(2), "UNREAD", "NOT_POSSESSED"),
                LibrariesItemsDescription(5, BooksDescription(3), "UNREAD", "POSSESSED"),
                LibrariesItemsDescription(6, BooksDescription(5), "UNREAD", "POSSESSED"),
                LibrariesItemsDescription(
                    7,
                    BooksDescription(1, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
                LibrariesItemsDescription(
                    8,
                    BooksDescription(3, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
            ).map { mapToMinimalLibrariesItem(it) }


            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            parameters["search"] = "status:UNREAD"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/items") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(librariesItems), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 items with search -- editor`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val librariesItems = mutableListOf(
                LibrariesItemsDescription(1, BooksDescription(1), "READ", "POSSESSED"),
                LibrariesItemsDescription(4, BooksDescription(2), "UNREAD", "NOT_POSSESSED"),
                LibrariesItemsDescription(5, BooksDescription(3), "UNREAD", "POSSESSED"),
                LibrariesItemsDescription(6, BooksDescription(5), "UNREAD", "POSSESSED"),
            ).map { mapToMinimalLibrariesItem(it) }


            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            parameters["search"] = "editor:*1"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/items") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(librariesItems), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 items without search with order -- by series`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val librariesItems = mutableListOf(
                LibrariesItemsDescription(
                    9,
                    BooksDescription(5, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
                LibrariesItemsDescription(
                    8,
                    BooksDescription(3, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
                LibrariesItemsDescription(
                    7,
                    BooksDescription(1, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
                LibrariesItemsDescription(6, BooksDescription(5), "UNREAD", "POSSESSED"),
                LibrariesItemsDescription(5, BooksDescription(3), "UNREAD", "POSSESSED"),
            ).map { mapToMinimalLibrariesItem(it) }


            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            parameters["sort"] = "series"
            parameters["direction"] = "desc"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/items") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(librariesItems), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 items with search -- series`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val librariesItems = mutableListOf(
                LibrariesItemsDescription(
                    7,
                    BooksDescription(1, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
                LibrariesItemsDescription(
                    8,
                    BooksDescription(3, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
                LibrariesItemsDescription(
                    9,
                    BooksDescription(5, BooksSeriesDescription(2), 2, 2),
                    "UNREAD",
                    "POSSESSED"
                ),
            ).map { mapToMinimalLibrariesItem(it) }


            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            parameters["search"] = "series:*2"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/items") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(librariesItems), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 items with search -- bookType`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val librariesItems = mutableListOf(
                LibrariesItemsDescription(1, BooksDescription(1), "READ", "POSSESSED"),
                LibrariesItemsDescription(4, BooksDescription(2), "UNREAD", "NOT_POSSESSED"),
                LibrariesItemsDescription(5, BooksDescription(3), "UNREAD", "POSSESSED"),
                LibrariesItemsDescription(6, BooksDescription(5), "UNREAD", "POSSESSED"),
            ).map { mapToMinimalLibrariesItem(it) }


            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            parameters["search"] = "bookType:*1"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/items") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(librariesItems), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 items with search -- possessionStatus -- error`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val librariesItems = mutableListOf(
                LibrariesItemsDescription(1, BooksDescription(1), "READ", "POSSESSED"),
                LibrariesItemsDescription(4, BooksDescription(2), "UNREAD", "NOT_POSSESSED"),
                LibrariesItemsDescription(5, BooksDescription(3), "UNREAD", "POSSESSED"),
                LibrariesItemsDescription(6, BooksDescription(5), "UNREAD", "POSSESSED"),
            ).map { mapToMinimalLibrariesItem(it) }


            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            parameters["search"] = "possessionStatus:SOLD"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/items") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isBadRequest() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    "400",
                                    "SOLD is not a correct value for the parameter [possessionStatus]",
                                    "WRONG_SEARCH_PARAMETER_VALUE"
                                )
                            ), true
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /libraries/{accountId}")
    inner class GetProfileInformation {
        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get global status of the accounts librarie`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))

            mockMvc.get(baseUrl.replace("{accountId}", "1")) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                LibraryDTO(
                                    lastAddedItems = PageOfMinimalLibraryItemDTO(
                                        0, 2, 7, mutableListOf(
                                            LibrariesItemsDescription(1, BooksDescription(1), "READ", "POSSESSED"),
                                            LibrariesItemsDescription(
                                                4,
                                                BooksDescription(2),
                                                "UNREAD",
                                                "NOT_POSSESSED"
                                            ),
                                            LibrariesItemsDescription(5, BooksDescription(3), "UNREAD", "POSSESSED"),
                                            LibrariesItemsDescription(6, BooksDescription(5), "UNREAD", "POSSESSED"),
                                            LibrariesItemsDescription(
                                                7,
                                                BooksDescription(
                                                    1,
                                                    editor = 2,
                                                    series = BooksSeriesDescription(2),
                                                    bookType = 2
                                                ),
                                                "UNREAD",
                                                "POSSESSED"
                                            ),
                                        ).map { mapToMinimalLibrariesItem(it) }.toMutableList()
                                    ),
                                    lastReadItems = null,
                                    nextItemsToRead = PageOfMinimalLibraryItemWithCoverDTO(
                                        0, 1, 2, mutableListOf(
                                            LibrariesItemsDescription(
                                                4,
                                                BooksDescription(2),
                                                "UNREAD",
                                                "NOT_POSSESSED"
                                            ),
                                            LibrariesItemsDescription(
                                                7,
                                                BooksDescription(1, BooksSeriesDescription(id = 2), 2, editor = 2),
                                                "UNREAD",
                                                "POSSESSED"
                                            ),
                                        ).map { mapToLibrariesItemWithCover(it) }.toMutableList()
                                    ),
                                    itemsStatInfo = StatInfoDTO(1, 7),
                                    libraryCount = LibraryCountDTO(
                                        booksCount = 7,
                                        bookTypesCount = 2,
                                        bookTypesBooks = mutableListOf(
                                            KeyValueDTO("bookType 1", BigDecimal(4)),
                                            KeyValueDTO("bookType 2", BigDecimal(3))
                                        )
                                    )
                                )
                            ), true
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /libraries/{accountId}/last-added")
    inner class GetLastAddedItems {
        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get a page of last added book in librarie`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            parameters["page"] = "1"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/last-added") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                PageOfMinimalLibraryItemDTO(
                                    1, 2, 7, mutableListOf(
                                        LibrariesItemsDescription(
                                            8,
                                            BooksDescription(
                                                3,
                                                editor = 2,
                                                series = BooksSeriesDescription(2),
                                                bookType = 2
                                            ),
                                            "UNREAD",
                                            "POSSESSED"
                                        ),
                                        LibrariesItemsDescription(
                                            9,
                                            BooksDescription(
                                                5,
                                                editor = 2,
                                                series = BooksSeriesDescription(2),
                                                bookType = 2
                                            ),
                                            "UNREAD",
                                            "POSSESSED"
                                        ),
                                    ).map { mapToMinimalLibrariesItem(it) }.toMutableList()
                                )
                            ), true
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /libraries/{accountId}/next-to-read")
    inner class GetNextItemsToRead {
        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get a page of last added book in librarie`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))
            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "1"
            parameters["page"] = "1"

            mockMvc.get("${baseUrl.replace("{accountId}", "1")}/next-to-read") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                PageOfMinimalLibraryItemWithCoverDTO(
                                    1, 2, 2, mutableListOf(
                                        LibrariesItemsDescription(
                                            7,
                                            BooksDescription(1, BooksSeriesDescription(id = 2), 2, editor = 2),
                                            "UNREAD",
                                            "POSSESSED"
                                        ),
                                    ).map { mapToLibrariesItemWithCover(it) }.toMutableList()
                                ),
                            ), true
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("PATCH /libraries/{accountId}/item/{id}")
    inner class PatchBookInLibrary {
        @Test
        fun `should get a page of last added book in librarie`() {
            DaoTestUtils.executeScripts(arrayOf("librariesItems/testGet1stPageOf5Items.sql"))

            val body = LibraryItemDTO(
                id = 2,
                status = LibraryItemDTO.Status.UNREAD
            )

            mockMvc.patch(
                "${baseUrl.replace("{accountId}", "2")}/items/{id}"
                    .replace("{id}", "2"),
                body
            ) {
                content = objectMapper.writeValueAsString(body)
                contentType = MediaType.APPLICATION_JSON
                header(HttpHeaders.AUTHORIZATION, "Bearer token")
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                mapToLibrariesItem(
                                    LibrariesItemsDescription(2, BooksDescription(1), "UNREAD", "POSSESSED"),
                                )
                            ), true
                        )
                    }
                }
        }
    }
}