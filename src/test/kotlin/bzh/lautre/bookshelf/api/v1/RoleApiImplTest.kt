package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.model.RoleDTO
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.DaoTestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
class RoleApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper,
) {
    val baseUrl = "/roles"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    private fun mapToRoles(index: Long): RoleDTO {
        return RoleDTO(
            name = "role $index",
            id = index,
        )
    }

    @Nested
    @DisplayName("GET /roles")
    inner class GetRoles {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 roles without search`() {
            DaoTestUtils.executeScripts(arrayOf("roles/testGet1stPageOf5Roles.sql"))

            val roles = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToRoles(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get second page of 5 roles without search`() {
            DaoTestUtils.executeScripts(arrayOf("roles/testGet1stPageOf5Roles.sql"))

            val roles = mutableListOf<Long>(5, 6, 7, 8, 9).map { mapToRoles(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "1"
            parameters["size"] = "5"
            parameters["direction"] = "ASC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 roles without search order desc`() {
            DaoTestUtils.executeScripts(arrayOf("roles/testGet1stPageOf5Roles.sql"))

            val roles = mutableListOf<Long>(9, 8, 7, 6, 5).map { mapToRoles(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["direction"] = "DESC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 roles with 'or' search on name`() {
            DaoTestUtils.executeScripts(arrayOf("roles/testGet1stPageOf5Roles.sql"))

            val roles = mutableListOf<Long>(2, 9).map { mapToRoles(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*2|'name:*9"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 roles with search on name`() {
            DaoTestUtils.executeScripts(arrayOf("roles/testGet1stPageOf5Roles.sql"))

            val roles = mutableListOf<Long>(6).map { mapToRoles(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*6"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /roles/autocomplete")
    inner class GetRolesAutoComplete {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 roles autocomplete no filter`() {
            DaoTestUtils.executeScripts(arrayOf("roles/testGet1stPageOf5Roles.sql"))

            val roles = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToRoles(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["sort"] = "name"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 1 roles autocomplete with filter on name`() {
            DaoTestUtils.executeScripts(arrayOf("roles/testGet1stPageOf5Roles.sql"))

            val roles = mutableListOf<Long>(3).map { mapToRoles(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["search"] = "name:*3"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 roles autocomplete with sorted by count then by name`() {
            DaoTestUtils.executeScripts(arrayOf("roles/testGet1stPageOf5Roles.sql"))

            val roles = mutableListOf<Long>(2, 1, 3, 10, 4).map { mapToRoles(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles), true)
                    }
                }
        }
    }

}
