package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.BooksSeriesDescription
import bzh.lautre.bookshelf.utils.DaoTestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.put
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
class SeriesApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper,
) {
    val baseUrl = "/series"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    private fun mapToSeries(booksSeriesDescription: BooksSeriesDescription): SeriesDTO {
        return SeriesDTO(
            name = "series ${booksSeriesDescription.id}",
            id = booksSeriesDescription.id,
            status = SeriesStatusDTO(
                totalCount = booksSeriesDescription.total,
                status = SeriesStatusDTO.Status.UNKNOWN,
            ),
            oneShot = (booksSeriesDescription.id % 2) == 0L,
            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
            cycles = mutableListOf(),
            editor = "editor 1"
        )
    }

    private fun mapToSeriesCycle(booksSeriesDescription: BooksSeriesDescription): SeriesCycleDTO {
        return SeriesCycleDTO(
            name = "series cycle ${booksSeriesDescription.id}",
            id = booksSeriesDescription.id,
        )
    }

    @Nested
    @DisplayName("GET /series")
    inner class GetSeries {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 series without search`() {
            DaoTestUtils.executeScripts(arrayOf("series/testGet1stPageOf5Series.sql"))

            val series = mutableListOf(
                BooksSeriesDescription(1, count = 0, total = 0),
                BooksSeriesDescription(10, count = 0, total = 0),
                BooksSeriesDescription(2, count = 0, total = 0),
                BooksSeriesDescription(3, count = 0, total = 0),
                BooksSeriesDescription(4, count = 0, total = 0)
            ).map { mapToSeries(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(series), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get second page of 5 series without search`() {
            DaoTestUtils.executeScripts(arrayOf("series/testGet1stPageOf5Series.sql"))

            val series = mutableListOf(
                BooksSeriesDescription(5, count = 0, total = 0),
                BooksSeriesDescription(6, count = 0, total = 0),
                BooksSeriesDescription(7, count = 0, total = 0),
                BooksSeriesDescription(8, count = 0, total = 0),
                BooksSeriesDescription(9, count = 0, total = 0)
            ).map { mapToSeries(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "1"
            parameters["size"] = "5"
            parameters["direction"] = "ASC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(series), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 series without search order desc`() {
            DaoTestUtils.executeScripts(arrayOf("series/testGet1stPageOf5Series.sql"))

            val series = mutableListOf(
                BooksSeriesDescription(9, count = 0, total = 0),
                BooksSeriesDescription(8, count = 0, total = 0),
                BooksSeriesDescription(7, count = 0, total = 0),
                BooksSeriesDescription(6, count = 0, total = 0),
                BooksSeriesDescription(5, count = 0, total = 0)
            ).map { mapToSeries(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["direction"] = "DESC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(series), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 series with 'or' search on name`() {
            DaoTestUtils.executeScripts(arrayOf("series/testGet1stPageOf5Series.sql"))

            val series = mutableListOf(
                BooksSeriesDescription(2, count = 0, total = 0),
                BooksSeriesDescription(9, count = 0, total = 0)
            ).map { mapToSeries(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*2|'name:*9"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(series), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 series with search on name`() {
            DaoTestUtils.executeScripts(arrayOf("series/testGet1stPageOf5Series.sql"))

            val series = mutableListOf(BooksSeriesDescription(6, count = 0, total = 0)).map { mapToSeries(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*6"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(series), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /series/autocomplete")
    inner class GetSeriesAutoComplete {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 series autocomplete no filter`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesAutocomplete.sql"))

            val series = mutableListOf(
                BooksSeriesDescription(1, count = 0, total = 0),
                BooksSeriesDescription(10, count = 0, total = 0),
                BooksSeriesDescription(2, count = 0, total = 0),
                BooksSeriesDescription(3, count = 0, total = 0),
                BooksSeriesDescription(4, count = 0, total = 0)
            ).map { mapToSeries(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(series), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 1 series autocomplete with filter on name`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesAutocomplete.sql"))

            val series = mutableListOf(BooksSeriesDescription(3, count = 0, total = 0)).map { mapToSeries(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["search"] = "name:*3"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(series), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /series/{id}/series-cycle/autocomplete")
    inner class GetSeriesCycleAutocomplete {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 series cycle autocomplete no filter`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesCyclesAutocomplete.sql"))

            val seriesCycle = mutableListOf(
                BooksSeriesDescription(1),
                BooksSeriesDescription(2)
            ).map { mapToSeriesCycle(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            mockMvc.get("$baseUrl/1/series-cycle/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(seriesCycle), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 1 series cycle autocomplete with filter on name`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesCyclesAutocomplete.sql"))

            val series = mutableListOf(BooksSeriesDescription(3)).map { mapToSeriesCycle(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["search"] = "name:*3"
            mockMvc.get("$baseUrl/2/series-cycle/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(series), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 0 series cycle autocomplete of unknown series`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesCyclesAutocomplete.sql"))


            mockMvc.get("$baseUrl/6/series-cycle/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(mutableListOf<SeriesCycleDTO>()), true)
                    }
                }
        }
    }


    @Nested
    @DisplayName("DELETE /series/{id}")
    inner class DeleteSeries {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an series by id`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesUpdate.sql"))

            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an series by id not found`() {

            mockMvc.delete("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: SERIES 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }


    @Nested
    @DisplayName("PUT /series/{id}")
    inner class PutSeries {

        @Test
        fun `should throw unauthorized 401`() {
            val index = 5L
            mockMvc.put("$baseUrl/5") {
                content =
                    objectMapper.writeValueAsString(
                        SeriesDTO(
                            name = "new series $index",
                            id = index,
                            status = SeriesStatusDTO(
                                totalCount = 2,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                            oneShot = (index % 2) == 0L,
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                            cycles = mutableListOf(),
                            editor = "editor 1"
                        )
                    )

                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            val index = 5L
            mockMvc.put("$baseUrl/5") {
                content =
                    objectMapper.writeValueAsString(
                        SeriesDTO(
                            name = "new series $index",
                            id = index,
                            status = SeriesStatusDTO(
                                totalCount = 2,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                            oneShot = (index % 2) == 0L,
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                            cycles = mutableListOf(),
                            editor = "editor 1"
                        )
                    )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not update an series path id and body id not same `() {
            val index = 6L
            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    SeriesDTO(
                        name = "new series $index",
                        id = index,
                        status = SeriesStatusDTO(
                            totalCount = 0,
                            status = SeriesStatusDTO.Status.UNKNOWN
                        ),
                        oneShot = (index % 2) == 0L,
                        bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                        cycles = mutableListOf(),
                        editor = "editor 1"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an series by id`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesUpdate.sql"))
            val index = 5L
            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    SeriesDTO(
                        name = "new series $index",
                        id = index,
                        status = SeriesStatusDTO(
                            totalCount = 0,
                            status = SeriesStatusDTO.Status.UNKNOWN
                        ),
                        oneShot = (index % 2) == 0L,
                        bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                        cycles = mutableListOf(
                            SeriesCycleDTO(name = "series cycle 2"),
                        ),
                        editor = "editor 1"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                SeriesDTO(
                                    name = "new series $index",
                                    id = index,
                                    status = SeriesStatusDTO(
                                        totalCount = 0,
                                        status = SeriesStatusDTO.Status.UNKNOWN
                                    ),
                                    oneShot = (index % 2) == 0L,
                                    bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                                    cycles = mutableListOf(
                                        SeriesCycleDTO(id = 2, name = "series cycle 2"),
                                    ),
                                    editor = "editor 1"
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an series by id with seriesCycle already existing add`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesUpdate.sql"))

            val index = 4L
            mockMvc.put("$baseUrl/4") {
                content = objectMapper.writeValueAsString(
                    SeriesDTO(
                        name = "new series $index",
                        id = index,
                        status = SeriesStatusDTO(
                            totalCount = 0,
                            status = SeriesStatusDTO.Status.UNKNOWN
                        ),
                        oneShot = (index % 2) == 0L,
                        bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                        cycles = mutableListOf(
                            SeriesCycleDTO(name = "series cycle 2"),
                            SeriesCycleDTO(id = 1, name = "series cycle 1"),
                        ),
                        editor = "editor 1"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                SeriesDTO(
                                    name = "new series $index",
                                    id = index,
                                    status = SeriesStatusDTO(
                                        totalCount = 0,
                                        status = SeriesStatusDTO.Status.UNKNOWN
                                    ),
                                    oneShot = (index % 2) == 0L,
                                    bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                                    cycles = mutableListOf(
                                        SeriesCycleDTO(id = 2, name = "series cycle 2"),
                                        SeriesCycleDTO(id = 1, name = "series cycle 1"),
                                    ),
                                    editor = "editor 1"
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an series by id with seriesCycle already existing replace`() {
            DaoTestUtils.executeScripts(arrayOf("series/testSeriesUpdate.sql"))

            val index = 4L
            mockMvc.put("$baseUrl/4") {
                content = objectMapper.writeValueAsString(
                    SeriesDTO(
                        name = "new series $index",
                        id = index,
                        status = SeriesStatusDTO(
                            totalCount = 0,
                            status = SeriesStatusDTO.Status.UNKNOWN
                        ),
                        oneShot = (index % 2) == 0L,
                        bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                        cycles = mutableListOf(
                            SeriesCycleDTO(name = "series cycle 2"),
                        ),
                        editor = "editor 1"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                SeriesDTO(
                                    name = "new series $index",
                                    id = index,
                                    status = SeriesStatusDTO(
                                        totalCount = 0,
                                        status = SeriesStatusDTO.Status.UNKNOWN
                                    ),
                                    oneShot = (index % 2) == 0L,
                                    bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                                    cycles = mutableListOf(
                                        SeriesCycleDTO(id = 2, name = "series cycle 2"),
                                    ),
                                    editor = "editor 1"
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an series by id not found`() {
            val index = 121L
            mockMvc.put("$baseUrl/121") {
                content = objectMapper.writeValueAsString(
                    SeriesDTO(
                        name = "new series $index",
                        id = index,
                        status = SeriesStatusDTO(
                            totalCount = 2,
                            status = SeriesStatusDTO.Status.UNKNOWN
                        ),
                        oneShot = (index % 2) == 0L,
                        bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                        cycles = mutableListOf(),
                        editor = "editor 1"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }

                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: SERIES 121",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

}
