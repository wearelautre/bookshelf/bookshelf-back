package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.DaoTestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.*
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers
import java.math.BigDecimal
import java.time.LocalDate


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
class BorrowersApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper,
) {
    val baseUrl = "/borrowers"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    private fun mapToBorrowers(index: Long): BorrowerDTO {
        val count = if (index == 1L) BigDecimal(2) else BigDecimal.ZERO
        return BorrowerDTO(index, "borrower $index", count)
    }

    @Nested
    @DisplayName("GET /borrowers")
    inner class GetBorrowers {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 borrowers without search`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testGet1stPageOf5Borrowers.sql"))

            val borrowers = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToBorrowers(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(borrowers), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get second page of 5 borrowers without search`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testGet1stPageOf5Borrowers.sql"))

            val borrowers = mutableListOf<Long>(5, 6, 7, 8, 9).map { mapToBorrowers(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "1"
            parameters["size"] = "5"
            parameters["direction"] = "ASC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(borrowers), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 borrowers without search order desc`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testGet1stPageOf5Borrowers.sql"))

            val borrowers = mutableListOf<Long>(9, 8, 7, 6, 5).map { mapToBorrowers(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["direction"] = "DESC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(borrowers), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 borrowers with 'or' search on name`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testGet1stPageOf5Borrowers.sql"))

            val borrowers = mutableListOf<Long>(2, 9).map { mapToBorrowers(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*2|'name:*9"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(borrowers), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 borrowers with search on name`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testGet1stPageOf5Borrowers.sql"))

            val borrowers = mutableListOf<Long>(6).map { mapToBorrowers(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*6"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(borrowers), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /borrowers/autocomplete")
    inner class GetBorrowersAutoComplete {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 borrowers autocomplete no filter`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testBorrowerAutocomplete.sql"))

            val borrowers = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToBorrowers(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(borrowers), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 1 borrower autocomplete with filter on name`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testBorrowerAutocomplete.sql"))

            val borrowers = mutableListOf<Long>(3).map { mapToBorrowers(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["search"] = "name:*3"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(borrowers), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /borrowers/{id}")
    inner class GetBorrowerById {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get an borrower by id`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testBorrowerAutocomplete.sql"))

            mockMvc.get("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BorrowerDTO(
                                    5,
                                    "borrower 5",
                                    BigDecimal.ZERO
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should not get borrower by id not found`() {

            mockMvc.get("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BORROWER 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("PUT /borrowers/{id}")
    inner class PutBorrower {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.put("$baseUrl/5") {
                content = BorrowerDTO(5, "new borrower 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.put("$baseUrl/5") {
                content = BorrowerDTO(5, "new borrower 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not update an borrower path id and body id not same `() {
            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    BorrowerDTO(
                        6,
                        "new borrower 5",
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an borrower by id`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testBorrowerUpdate.sql"))

            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    BorrowerDTO(
                        5,
                        "new borrower 5"
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BorrowerDTO(
                                    5,
                                    "new borrower 5",
                                    BigDecimal.ZERO

                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an borrower by id not found`() {

            mockMvc.put("$baseUrl/1") {
                content = objectMapper.writeValueAsString(BorrowerDTO(1, "new borrower 1"))
                contentType = MediaType.APPLICATION_JSON
            }

                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BORROWER 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("DELETE /borrowers/{id}")
    inner class DeleteBorrower {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete a borrower by id`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testBorrowerUpdate.sql"))

            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete a borrower by id not found`() {

            mockMvc.delete("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BORROWER 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("POST /borrowers")
    inner class CreateBorrower {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should create an borrower by id`() {
            // DaoTestUtils.executeScripts(arrayOf("borrowers/testBorrowerUpdate.sql"))
            val borrower = mapToBorrowers(1)
            borrower.id = null
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(borrower)

            }
                .andExpect {
                    status { isCreated() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should create an borrower but id already created`() {
            // DaoTestUtils.executeScripts(arrayOf("borrowers/testBorrowerUpdate.sql"))

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(mapToBorrowers(1))
            }
                .andExpect {
                    status { isCreated() }
                }
        }
    }

    @Nested
    @DisplayName("POST /borrowers/{id}/loans")
    inner class CreateLoans {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should create a loan for a borrower`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoans.sql"))
            val date = LocalDate.now()

            val loan = LoanDTO(borrowDate = date, libraryItem = 3)
            mockMvc.post("$baseUrl/1/loans") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(loan)

            }
                .andExpect {
                    status { isCreated() }
                    content {
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        json(
                            objectMapper.writeValueAsString(
                                LoanDTO(
                                    id = 3,
                                    borrowDate = date,
                                    libraryItem = 3
                                )
                            )
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not create a loan if the borrower doesn't exist`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoans.sql"))
            val date = LocalDate.now()

            val loan = LoanDTO(borrowDate = date, libraryItem = 1)
            mockMvc.post("$baseUrl/20/loans") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(loan)

            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BORROWER 20",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not create a loan for a not possessed book`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoans.sql"))
            val date = LocalDate.now()

            val loan = LoanDTO(borrowDate = date, libraryItem = 4)
            mockMvc.post("$baseUrl/1/loans") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(loan)

            }
                .andExpect {
                    status { isBadRequest() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The book is sold: 0000000001114",
                                    code = "400",
                                    error = "BOOK_SOLD"
                                )
                            )
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not create a loan for an already lent book`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoans.sql"))
            val date = LocalDate.now()

            val loan = LoanDTO(borrowDate = date, libraryItem = 2)
            mockMvc.post("$baseUrl/1/loans") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(loan)

            }
                .andExpect {
                    status { isConflict() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The book [0000000001112] is already borrowed",
                                    code = "409",
                                    error = "BOOK_ALREADY_BORROWED"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /borrowers/{id}/loans")
    inner class GetLoans {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should return a list of loans`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoansDelete.sql"))
            val date = LocalDate.parse("2022-07-14")

            val loans = mutableListOf(
                LoanWithBookDetailsDTO(
                    id = 1,
                    borrowDate = date,
                    libraryItem = MinimalLibraryItemDTO(
                        id = 1,
                        status = MinimalLibraryItemDTO.Status.UNREAD,
                        lent = true,
                        possessionStatus = MinimalLibraryItemDTO.PossessionStatus.POSSESSED,
                        book = MinimalBookDTO(
                            isbn = "0000000001111",
                            title = "title 1",
                            series = BookSeriesDTO(
                                name = "series 1",
                                id = 1,
                                status = SeriesStatusDTO(
                                    totalCount = 0,
                                    status = SeriesStatusDTO.Status.UNKNOWN
                                ),
                                oneShot = false,
                                tome = 1,
                                bookType = MinimalBookTypeDTO(
                                    id = 1,
                                    name = "bookType 1"
                                ),
                                seriesCycle = null
                            ),
                            editor = MinimalEditorDTO(name = "editor 1")
                        ),
                    )
                ),
                LoanWithBookDetailsDTO(
                    id = 2,
                    borrowDate = date,
                    libraryItem = MinimalLibraryItemDTO(
                        id = 2,
                        status = MinimalLibraryItemDTO.Status.UNREAD,
                        lent = true,
                        possessionStatus = MinimalLibraryItemDTO.PossessionStatus.POSSESSED,
                        book = MinimalBookDTO(
                            isbn = "0000000001112",
                            title = "title 2",
                            series = BookSeriesDTO(
                                name = "series 1",
                                id = 1,
                                status = SeriesStatusDTO(
                                    totalCount = 0,
                                    status = SeriesStatusDTO.Status.UNKNOWN
                                ),
                                oneShot = false,
                                tome = 2,
                                bookType = MinimalBookTypeDTO(
                                    id = 1,
                                    name = "bookType 1"
                                ),
                                seriesCycle = null
                            ),
                            editor = MinimalEditorDTO(name = "editor 1")
                        )
                    )
                )
            )

            mockMvc.get("$baseUrl/1/loans") {
                contentType = MediaType.APPLICATION_JSON

            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        json(objectMapper.writeValueAsString(loans))
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should return a list of loans borrower doesn't exist`() {

            mockMvc.get("$baseUrl/20/loans") {
                contentType = MediaType.APPLICATION_JSON

            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BORROWER 20",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("PUT /borrowers/{id}/loans")
    inner class UpdateLoans {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.put(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.put(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update a borrower's loan by ids borrower not found`() {
            val loan = LoanDTO(
                borrowDate = LocalDate.parse("2022-07-14"),
                returnDate = LocalDate.parse("2022-08-14"),
                libraryItem = 1
            )

            mockMvc.put("$baseUrl/9/loans/2") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(loan)
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BORROWER 9",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update a borrower's loan by ids loan not found`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoansDelete.sql"))
            val loan = LoanDTO(
                id = 20,
                borrowDate = LocalDate.parse("2022-07-14"),
                returnDate = LocalDate.parse("2022-08-14"),
                libraryItem = 1
            )

            mockMvc.put("$baseUrl/1/loans/20") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(loan)
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: LOAN 20",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update a loan by id`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoansDelete.sql"))
            val loan = LoanDTO(
                id = 1,
                borrowDate = LocalDate.parse("2022-07-14"),
                returnDate = LocalDate.parse("2022-08-14"),
                libraryItem = 1
            )

            val loanWithBookDetailsDTO = LoanWithBookDetailsDTO(
                id = 1,
                borrowDate = LocalDate.parse("2022-07-14"),
                returnDate = LocalDate.parse("2022-08-14"),
                libraryItem = MinimalLibraryItemDTO(
                    id = 1,
                    status = MinimalLibraryItemDTO.Status.UNREAD,
                    lent = false,
                    possessionStatus = MinimalLibraryItemDTO.PossessionStatus.POSSESSED,
                    book = MinimalBookDTO(
                        isbn = "0000000001111",
                        title = "title 1",
                        series = BookSeriesDTO(
                            name = "series 1",
                            id = 1,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                            oneShot = false,
                            tome = 1,
                            bookType = MinimalBookTypeDTO(
                                id = 1,
                                name = "bookType 1"
                            ),
                            seriesCycle = null
                        ),
                        editor = MinimalEditorDTO(name = "editor 1")
                    )
                )
            )


            mockMvc.put("$baseUrl/1/loans/1") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(loan)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        json(objectMapper.writeValueAsString(loanWithBookDetailsDTO))
                    }
                }
        }
    }

    @Nested
    @DisplayName("DELETE /borrowers/{id}/loans{id}")
    inner class DeleteLoans {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.delete("$baseUrl/5/loans/2") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.delete("$baseUrl/5/loans/2") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete a loan by id`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoansDelete.sql"))

            mockMvc.delete("$baseUrl/1/loans/2") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete a borrower's loan by ids borrower not found`() {

            mockMvc.delete("$baseUrl/9/loans/2") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BORROWER 9",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete a borrower's loan by ids loan not found`() {
            DaoTestUtils.executeScripts(arrayOf("borrowers/testLoansDelete.sql"))

            mockMvc.delete("$baseUrl/1/loans/20") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: LOAN 20",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

}
