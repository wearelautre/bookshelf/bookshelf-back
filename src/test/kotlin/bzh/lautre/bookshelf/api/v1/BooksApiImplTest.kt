package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.BooksDescription
import bzh.lautre.bookshelf.utils.BooksSeriesDescription
import bzh.lautre.bookshelf.utils.DaoTestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.*
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers
import java.math.BigDecimal


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
class BooksApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper,
) {
    val baseUrl = "/books"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    private fun mapToBookFull(book: BooksDescription): BookDTO {
        val series = BookSeriesDTO(
            bookType = MinimalBookTypeDTO(name = "bookType ${book.bookType}", id = book.bookType),
            tome = book.tome,
            seriesCycle = null,
            name = "series ${book.series.id}",
            status = SeriesStatusDTO(
                totalCount = book.series.total,
                status = SeriesStatusDTO.Status.valueOf(book.series.status)
            ),
            id = book.series.id,
            oneShot = false,
        )

        return BookDTO(
            year = "2022",
            editor = EditorDTO(id = 1, name = "editor ${book.editor}"),
            title = "title ${book.tome}",
            isbn = "000000000${book.editor}${book.series.id}${book.bookType}${book.tome}",
            series = series,
            contracts = mutableListOf(),
            metadata = BookMetadataDTO(arkId = "manual-test", pageCount = BigDecimal.ZERO),
        )
    }


    fun mapToBook(book: BooksDescription): MinimalBookDTO {
        val series = BookSeriesDTO(
            bookType = MinimalBookTypeDTO(name = "bookType ${book.bookType}", id = book.bookType),
            tome = book.tome,
            seriesCycle = null,
            name = "series ${book.series.id}",
            status = SeriesStatusDTO(
                totalCount = book.series.total,
                status = SeriesStatusDTO.Status.valueOf(book.series.status)
            ),
            id = book.series.id,
            oneShot = false,
        )

        return MinimalBookDTO(
            editor = MinimalEditorDTO(name = "editor ${book.editor}"),
            title = "title ${book.tome}",
            isbn = "000000000${book.editor}${book.series.id}${book.bookType}${book.tome}",
            series = series,
        )
    }

    @Nested
    @DisplayName("GET /books")
    inner class GetBooks {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 books without search`() {
            DaoTestUtils.executeScripts(arrayOf("books/testGet1stPageOf5Books.sql"))
            val books = mutableListOf(
                BooksDescription(1),
                BooksDescription(2),
                BooksDescription(3),
                BooksDescription(4),
                BooksDescription(5)
            ).map { mapToBook(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(books), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get second page of 5 books without search`() {
            DaoTestUtils.executeScripts(arrayOf("books/testGet1stPageOf5Books.sql"))
            val books = mutableListOf(
                BooksDescription(1, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(2, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(3, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(4, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(5, BooksSeriesDescription(2, count = 4), 2, 2)
            ).map { mapToBook(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "1"
            parameters["size"] = "5"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(books), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 books without search order desc`() {
            DaoTestUtils.executeScripts(arrayOf("books/testGet1stPageOf5Books.sql"))
            val books = mutableListOf(
                BooksDescription(5, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(4, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(3, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(2, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(1, BooksSeriesDescription(2, count = 4), 2, 2),
            ).map { mapToBook(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["direction"] = "DESC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(books), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 books without search order desc sort editor`() {
            DaoTestUtils.executeScripts(arrayOf("books/testGet1stPageOf5Books.sql"))
            val books = mutableListOf(
                BooksDescription(2, BooksSeriesDescription(2, count = 4), 2, 2),
                BooksDescription(2, BooksSeriesDescription(1), 1, 1),
            ).map { mapToBook(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["direction"] = "DESC"
            parameters["search"] = "title:*2"
            parameters["sort"] = "editor"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(books), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get second page of 5 books with search on series and title`() {
            DaoTestUtils.executeScripts(arrayOf("books/testGet1stPageOf5Books.sql"))
            val books = mutableListOf(
                BooksDescription(1, BooksSeriesDescription(2, count = 4), 2, 2),
            ).map { mapToBook(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "title:*1|series:*2"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(books), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 books with search on editor and status`() {
            DaoTestUtils.executeScripts(arrayOf("books/testGet1stPageOf5Books.sql"))
            val books = mutableListOf(
                BooksDescription(1),
                BooksDescription(2),
                BooksDescription(3),
                BooksDescription(4),
                BooksDescription(5),
            ).map { mapToBook(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "editor:*1"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(books), true)
                    }
                }
        }
    }


    @Nested
    @DisplayName("GET /books")
    inner class GetBooksByIsbn {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should return the books by isbn`() {
            DaoTestUtils.executeScripts(arrayOf("books/testGet1stPageOf5Books.sql"))

            mockMvc.get("$baseUrl/0000000001111") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                mapToBookFull(
                                    BooksDescription(
                                        1
                                    )
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should return the books by isbn not found`() {
            DaoTestUtils.executeScripts(arrayOf("books/testGet1stPageOf5Books.sql"))

            val isbn = "0000000000000"
            mockMvc.get("$baseUrl/$isbn") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BOOK $isbn",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            ), true
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("POST /books")
    inner class PostBooks {

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should create a book with all element assigned`() {

            val isbn = "0000000000000"
            val seriesName = "series 1"
            val bookTypeName = "bookType 1"
            val editorName = "editor 1"


            val bookDTO = BookDTO(
                isbn = isbn,
                year = "2022",
                collection = null,
                cover = null,
                contracts = mutableListOf(
                    ContractDTO(
                        artists = mutableListOf(MinimalArtistDTO(name = "artist 1")),
                        role = RoleDTO(name = "role 1")
                    )
                ),
                metadata = BookMetadataDTO(
                    arkId = "manual-test",
                    pageCount = BigDecimal(567)
                ),
                editor = EditorDTO(name = editorName),
                series = BookSeriesDTO(
                    bookType = MinimalBookTypeDTO(name = bookTypeName),
                    tome = 1,
                    seriesCycle = null,
                    name = seriesName,
                    oneShot = false
                ),
            )

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isCreated() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                            tome = 1,
                            seriesCycle = null,
                            name = "series 1",
                            oneShot = false,
                            id = 1,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        val createdContractDTO = ContractDTO(
                            artists = mutableListOf(MinimalArtistDTO(id = 1, name = "artist 1")),
                            role = RoleDTO(id = 1, name = "role 1")
                        )

                        val createdBookDTO = BookDTO()
                        createdBookDTO.metadata = BookMetadataDTO(
                            arkId = "manual-test",
                            pageCount = BigDecimal(567)
                        )
                        createdBookDTO.isbn = "0000000000000"
                        createdBookDTO.year = "2022"
                        createdBookDTO.collection = null
                        createdBookDTO.cover = null
                        createdBookDTO.contracts = mutableListOf(createdContractDTO)
                        createdBookDTO.editor = EditorDTO(id = 1, name = "editor 1")
                        createdBookDTO.series = createdBookSeriesDTO

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(createdBookDTO))
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should create a book with series, editor and book already created`() {

            DaoTestUtils.executeScripts(arrayOf("books/testSeriesEditorBookTypeAlreadyCreated.sql"))

            val isbn = "0000000000000"
            val seriesName = "series 1"
            val bookTypeName = "bookType 1"
            val editorName = "editor 1"

            val bookSeriesDTO = BookSeriesDTO(
                bookType = MinimalBookTypeDTO(name = bookTypeName),
                tome = 1,
                seriesCycle = null,
                name = seriesName,
                oneShot = false
            )

            val bookDTO = BookDTO()
            bookDTO.metadata = BookMetadataDTO(
                arkId = "manual-test"
            )
            bookDTO.isbn = isbn
            bookDTO.year = "2022"
            bookDTO.collection = null
            bookDTO.cover = null
            bookDTO.contracts = mutableListOf()
            bookDTO.editor = EditorDTO(name = editorName)
            bookDTO.series = bookSeriesDTO

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isCreated() }
                    content {

                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 10, name = bookTypeName),
                            tome = 1,
                            seriesCycle = null,
                            name = seriesName,
                            oneShot = false,
                            id = 10,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        val createdBookDTO = BookDTO()
                        createdBookDTO.metadata = BookMetadataDTO(
                            arkId = "manual-test",
                            pageCount = BigDecimal(0)
                        )
                        createdBookDTO.isbn = isbn
                        createdBookDTO.year = "2022"
                        createdBookDTO.collection = null
                        createdBookDTO.cover = null
                        createdBookDTO.contracts = mutableListOf()
                        createdBookDTO.editor = EditorDTO(id = 10, name = editorName)
                        createdBookDTO.series = createdBookSeriesDTO

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(createdBookDTO))
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not create a book because it already exists`() {

            DaoTestUtils.executeScripts(arrayOf("books/testConflictBookAlreadyCreated.sql"))

            val isbn = "0000000000000"
            val seriesName = "series 1"
            val bookTypeName = "bookType 1"
            val editorName = "editor 1"

            val bookSeriesDTO = BookSeriesDTO(
                bookType = MinimalBookTypeDTO(name = bookTypeName),
                tome = 1,
                seriesCycle = null,
                name = seriesName,
                oneShot = false
            )

            val bookDTO = BookDTO()
            bookDTO.metadata = BookMetadataDTO(
                arkId = "manual-test"
            )
            bookDTO.isbn = isbn
            bookDTO.year = "2022"
            bookDTO.collection = null
            bookDTO.cover = null
            bookDTO.contracts = mutableListOf()
            bookDTO.editor = EditorDTO(name = editorName)
            bookDTO.series = bookSeriesDTO

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isConflict() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not create due to no series`() {

            val isbn = "0000000000000"
            val editorName = "editor 1"

            val bookSeriesDTO = null

            val bookDTO = BookDTO()
            bookDTO.metadata = BookMetadataDTO(
                arkId = "manual-test"
            )
            bookDTO.isbn = isbn
            bookDTO.year = "2022"
            bookDTO.collection = null
            bookDTO.cover = null
            bookDTO.contracts = mutableListOf()
            bookDTO.editor = EditorDTO(name = editorName)
            bookDTO.series = bookSeriesDTO

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not create due to no editor`() {

            val isbn = "0000000000000"
            val seriesName = "series 1"
            val bookTypeName = "bookType 1"

            val bookSeriesDTO = BookSeriesDTO(
                bookType = MinimalBookTypeDTO(name = bookTypeName),
                tome = 1,
                seriesCycle = null,
                name = seriesName,
                oneShot = false
            )

            val bookDTO = BookDTO()
            bookDTO.isbn = isbn
            bookDTO.year = "2022"
            bookDTO.collection = null
            bookDTO.cover = null
            bookDTO.contracts = mutableListOf()
            bookDTO.metadata = BookMetadataDTO(
                arkId = "manual-test"
            )
            bookDTO.editor = null
            bookDTO.series = bookSeriesDTO

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should create a book with all element assigned with series cycle`() {

            val isbn = "0000000000001"
            val seriesName = "series 2"
            val seriesCycleName = "cycle 2"
            val bookTypeName = "bookType 2"
            val editorName = "editor 2"

            val bookSeriesDTO = BookSeriesDTO(
                bookType = MinimalBookTypeDTO(name = bookTypeName),
                tome = 1,
                seriesCycle = BookSeriesCycleDTO(tome = 1, name = seriesCycleName),
                name = seriesName,
                oneShot = false
            )

            val bookDTO = BookDTO()
            bookDTO.isbn = isbn
            bookDTO.year = "2022"
            bookDTO.collection = null
            bookDTO.cover = null
            bookDTO.contracts = mutableListOf()
            bookDTO.metadata = BookMetadataDTO(
                arkId = "manual-test"
            )
            bookDTO.editor = EditorDTO(name = editorName)
            bookDTO.series = bookSeriesDTO

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isCreated() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 1, name = bookTypeName),
                            tome = 1,
                            seriesCycle = BookSeriesCycleDTO(tome = 1, id = 1, name = seriesCycleName),
                            name = seriesName,
                            oneShot = false,
                            id = 1,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        val createdBookDTO = BookDTO()
                        createdBookDTO.isbn = isbn
                        createdBookDTO.year = "2022"
                        createdBookDTO.collection = null
                        createdBookDTO.cover = null
                        createdBookDTO.contracts = mutableListOf()
                        createdBookDTO.metadata = BookMetadataDTO(
                            arkId = "manual-test",
                            pageCount = BigDecimal(0)

                        )
                        createdBookDTO.editor = EditorDTO(id = 1, name = editorName)
                        createdBookDTO.series = createdBookSeriesDTO

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(createdBookDTO))
                    }

                }
        }
    }

    @Nested
    @DisplayName("PUT /books/{id}")
    inner class PutBooks {

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update a book with isbn`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdateBook.sql"))

            val isbn = "0000000000000"
            val seriesName = "series 2"
            val bookTypeName = "bookType 1"
            val editorName = "editor 1"

            val bookSeriesDTO = BookSeriesDTO(
                bookType = MinimalBookTypeDTO(name = bookTypeName),
                tome = 1,
                seriesCycle = null,
                name = seriesName,
                oneShot = false
            )

            val contractDTOList = mutableListOf(
                ContractDTO(
                    role = RoleDTO(name = "role 1"),
                    artists = mutableListOf(
                        MinimalArtistDTO(name = "artist 1"),
                        MinimalArtistDTO(name = "artist 2")
                    )
                )
            )

            val bookDTO = BookDTO(
                isbn = isbn,
                year = "2022",
                collection = null,
                cover = null,
                contracts = contractDTOList,
                metadata = BookMetadataDTO(
                    arkId = "manual-test",
                    pageCount = BigDecimal(0)
                ),
                editor = EditorDTO(name = editorName),
                series = bookSeriesDTO,
            )

            mockMvc.put("$baseUrl/${isbn}") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                            tome = 1,
                            seriesCycle = null,
                            name = "series 2",
                            oneShot = false,
                            id = 2,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        val createdContractDTOList = mutableListOf(
                            ContractDTO(
                                role = RoleDTO(id = 1, name = "role 1"),
                                artists = mutableListOf(
                                    MinimalArtistDTO(id = 1, name = "artist 1"),
                                    MinimalArtistDTO(id = 2, name = "artist 2")
                                )
                            )
                        )

                        val createdBookDTO = BookDTO(
                            isbn = "0000000000000",
                            year = "2022",
                            collection = null,
                            cover = null,
                            contracts = createdContractDTOList,
                            metadata = BookMetadataDTO(
                                arkId = "manual-test",
                                pageCount = BigDecimal(0)
                            ),

                            editor = EditorDTO(id = 1, name = "editor 1"),
                            series = createdBookSeriesDTO,
                        )

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(createdBookDTO))
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update a book with isbn contract with id`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdateBook.sql"))

            val isbn = "0000000000000"
            val seriesName = "series 2"
            val bookTypeName = "bookType 1"
            val editorName = "editor 1"

            val bookSeriesDTO = BookSeriesDTO(
                bookType = MinimalBookTypeDTO(name = bookTypeName),
                tome = 1,
                seriesCycle = null,
                name = seriesName,
                oneShot = false
            )

            val contractDTOList = mutableListOf(
                ContractDTO(
                    role = RoleDTO(id = 1, name = "role 1"),
                    artists = mutableListOf(
                        MinimalArtistDTO(id = 1, name = "artist 1"),
                        MinimalArtistDTO(id = 2, name = "artist 2")
                    )
                )
            )

            val bookDTO = BookDTO(
                isbn = isbn,
                year = "2022",
                collection = null,
                cover = null,
                contracts = contractDTOList,
                metadata = BookMetadataDTO(
                    arkId = "manual-test",
                    pageCount = BigDecimal(0)
                ),
                editor = EditorDTO(name = editorName),
                series = bookSeriesDTO,
            )

            mockMvc.put("$baseUrl/${isbn}") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                            tome = 1,
                            seriesCycle = null,
                            name = "series 2",
                            oneShot = false,
                            id = 2,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        val createdContractDTOList = mutableListOf(
                            ContractDTO(
                                role = RoleDTO(id = 1, name = "role 1"),
                                artists = mutableListOf(
                                    MinimalArtistDTO(id = 1, name = "artist 1"),
                                    MinimalArtistDTO(id = 2, name = "artist 2")
                                )
                            )
                        )

                        val createdBookDTO = BookDTO(
                            isbn = "0000000000000",
                            year = "2022",
                            collection = null,
                            cover = null,
                            contracts = createdContractDTOList,
                            metadata = BookMetadataDTO(
                                arkId = "manual-test",
                                pageCount = BigDecimal(0)
                            ),
                            editor = EditorDTO(id = 1, name = "editor 1"),
                            series = createdBookSeriesDTO,
                        )

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(createdBookDTO))
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update a book with isbn new contract id`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdateBook.sql"))

            val isbn = "0000000000000"
            val seriesName = "series 2"
            val bookTypeName = "bookType 1"
            val editorName = "editor 1"

            val bookSeriesDTO = BookSeriesDTO(
                bookType = MinimalBookTypeDTO(name = bookTypeName),
                tome = 1,
                seriesCycle = null,
                name = seriesName,
                oneShot = false
            )

            val contractDTOList = mutableListOf(
                ContractDTO(
                    role = RoleDTO(id = 1, name = "role 1"),
                    artists = mutableListOf(
                        MinimalArtistDTO(id = 1, name = "artist 1"),
                        MinimalArtistDTO(id = 2, name = "artist 2")
                    )
                ),
                ContractDTO(
                    role = RoleDTO(id = 2, name = "role 2"),
                    artists = mutableListOf(
                        MinimalArtistDTO(id = 1, name = "artist 1"),
                        MinimalArtistDTO(name = "artist 3")
                    )
                ),
                ContractDTO(
                    role = RoleDTO(name = "role 3"),
                    artists = mutableListOf(
                        MinimalArtistDTO(id = 1, name = "artist 1"),
                        MinimalArtistDTO(id = 4, name = "artist 4")
                    )
                )
            )

            val bookDTO = BookDTO(
                isbn = isbn,
                year = "2022",
                collection = null,
                cover = null,
                contracts = contractDTOList,
                metadata = BookMetadataDTO(
                    arkId = "manual-test"
                ),
                editor = EditorDTO(name = editorName),
                series = bookSeriesDTO,
            )

            mockMvc.put("$baseUrl/${isbn}") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                            tome = 1,
                            seriesCycle = null,
                            name = "series 2",
                            oneShot = false,
                            id = 2,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        val createdContractDTOList = mutableListOf(
                            ContractDTO(
                                role = RoleDTO(id = 1, name = "role 1"),
                                artists = mutableListOf(
                                    MinimalArtistDTO(id = 1, name = "artist 1"),
                                    MinimalArtistDTO(id = 2, name = "artist 2")
                                )
                            ),
                            ContractDTO(
                                role = RoleDTO(id = 2, name = "role 2"),
                                artists = mutableListOf(
                                    MinimalArtistDTO(id = 1, name = "artist 1"),
                                    MinimalArtistDTO(id = 3, name = "artist 3")
                                )
                            ),
                            ContractDTO(
                                role = RoleDTO(id = 3, name = "role 3"),
                                artists = mutableListOf(
                                    MinimalArtistDTO(id = 1, name = "artist 1"),
                                    MinimalArtistDTO(id = 4, name = "artist 4")
                                )
                            )
                        )

                        val createdBookDTO = BookDTO(
                            isbn = "0000000000000",
                            year = "2022",
                            collection = null,
                            cover = null,
                            contracts = createdContractDTOList,
                            metadata = BookMetadataDTO(
                                arkId = "manual-test",
                                pageCount = BigDecimal(0)
                            ),
                            editor = EditorDTO(id = 1, name = "editor 1"),
                            series = createdBookSeriesDTO,
                        )

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(createdBookDTO))
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not update a book path isbn null`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdateBook.sql"))

            mockMvc.put("$baseUrl/null") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookDTO(isbn = "0000000000000"))
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not update a book body isbn null`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdateBook.sql"))

            mockMvc.put("$baseUrl/null") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookDTO(isbn = null))
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not update a book isbns not equal`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdateBook.sql"))

            mockMvc.put("$baseUrl/0000000000000") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookDTO(isbn = "0000000000001"))
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not update a book not found`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdateBook.sql"))

            val isbn = "0000000000001"

            mockMvc.put("$baseUrl/$isbn") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookDTO(isbn = isbn))
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BOOK $isbn",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("PATCH /books/{id}")
    inner class PatchBooks {

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should patch a book with isbn`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdatePatchBook.sql"))

            val isbn = "0000000000000"

            val bookDTO = BookDTO(
                isbn = isbn,
            )

            mockMvc.patch("$baseUrl/${isbn}") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                            tome = 1,
                            seriesCycle = null,
                            name = "series 1",
                            oneShot = false,
                            id = 1,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BookDTO(
                                    title = "title 1",
                                    isbn = isbn,
                                    year = "2022",
                                    collection = null,
                                    cover = null,
                                    contracts = mutableListOf(),
                                    metadata = BookMetadataDTO(
                                        arkId = "manual-test",
                                        pageCount = BigDecimal(0)
                                    ),
                                    editor = EditorDTO(id = 1, name = "editor 1"),
                                    series = createdBookSeriesDTO,
                                )
                            )
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should patch a book with isbn and series`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdatePatchBook.sql"))

            val bookSeriesDTO = BookSeriesDTO(
                id = 1,
                tome = 1,
                seriesCycle = BookSeriesCycleDTO(name = "seriesCycle 2.1", tome = 1),
                name = "series 1",
                oneShot = false,
                bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1")
            )

            val isbn = "0000000000000"

            val bookDTO = BookDTO(
                isbn = isbn,
                series = bookSeriesDTO,
            )

            mockMvc.patch("$baseUrl/${isbn}") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                            tome = 1,
                            seriesCycle = BookSeriesCycleDTO(id = 1, name = "seriesCycle 2.1", tome = 1),
                            name = "series 1",
                            oneShot = false,
                            id = 1,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BookDTO(
                                    title = "title 1",
                                    isbn = isbn,
                                    year = "2022",
                                    collection = null,
                                    cover = null,
                                    contracts = mutableListOf(),
                                    metadata = BookMetadataDTO(
                                        arkId = "manual-test",
                                        pageCount = BigDecimal(0)
                                    ),
                                    editor = EditorDTO(id = 1, name = "editor 1"),
                                    series = createdBookSeriesDTO,
                                )
                            )
                        )
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should patch a book with isbn and series new bookType`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdatePatchBook.sql"))

            val bookSeriesDTO = BookSeriesDTO(
                id = 1,
                tome = 1,
                seriesCycle = BookSeriesCycleDTO(name = "seriesCycle 2.1", tome = 1),
                name = "series 1",
                oneShot = false,
                bookType = MinimalBookTypeDTO(name = "bookType 2")
            )

            val isbn = "0000000000000"

            val bookDTO = BookDTO(
                isbn = isbn,
                series = bookSeriesDTO,
            )

            mockMvc.patch("$baseUrl/${isbn}") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 2, name = "bookType 2"),
                            tome = 1,
                            seriesCycle = BookSeriesCycleDTO(id = 1, name = "seriesCycle 2.1", tome = 1),
                            name = "series 1",
                            oneShot = false,
                            id = 1,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BookDTO(
                                    title = "title 1",
                                    isbn = isbn,
                                    year = "2022",
                                    collection = null,
                                    cover = null,
                                    contracts = mutableListOf(),
                                    metadata = BookMetadataDTO(
                                        arkId = "manual-test",
                                        pageCount = BigDecimal(0)

                                    ),
                                    editor = EditorDTO(id = 1, name = "editor 1"),
                                    series = createdBookSeriesDTO,
                                )
                            )
                        )
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should patch a book with isbn and series update bookType`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdatePatchBook.sql"))

            val bookSeriesDTO = BookSeriesDTO(
                id = 1,
                tome = 1,
                seriesCycle = BookSeriesCycleDTO(name = "seriesCycle 2.1", tome = 1),
                name = "series 1",
                oneShot = false,
                bookType = MinimalBookTypeDTO(id = 1, name = "bookType 2")
            )

            val isbn = "0000000000000"

            val bookDTO = BookDTO(
                isbn = isbn,
                series = bookSeriesDTO,
            )

            mockMvc.patch("$baseUrl/${isbn}") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(bookDTO)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        val createdBookSeriesDTO = BookSeriesDTO(
                            bookType = MinimalBookTypeDTO(id = 1, name = "bookType 2"),
                            tome = 1,
                            seriesCycle = BookSeriesCycleDTO(id = 1, name = "seriesCycle 2.1", tome = 1),
                            name = "series 1",
                            oneShot = false,
                            id = 1,
                            status = SeriesStatusDTO(
                                totalCount = 0,
                                status = SeriesStatusDTO.Status.UNKNOWN
                            ),
                        )

                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                BookDTO(
                                    title = "title 1",
                                    isbn = isbn,
                                    year = "2022",
                                    collection = null,
                                    cover = null,
                                    contracts = mutableListOf(),
                                    metadata = BookMetadataDTO(
                                        arkId = "manual-test",
                                        pageCount = BigDecimal(0)

                                    ),
                                    editor = EditorDTO(id = 1, name = "editor 1"),
                                    series = createdBookSeriesDTO,
                                )
                            )
                        )
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not patch a book path isbn null`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdatePatchBook.sql"))

            mockMvc.put("$baseUrl/null") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookDTO(isbn = "0000000000000"))
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not patch a book body isbn null`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdatePatchBook.sql"))

            mockMvc.put("$baseUrl/null") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookDTO(isbn = null))
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not patch a book isbns not equal`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdatePatchBook.sql"))

            mockMvc.put("$baseUrl/0000000000000") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookDTO(isbn = "0000000000001"))
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not patch a book not found`() {

            DaoTestUtils.executeScripts(arrayOf("books/testUpdatePatchBook.sql"))

            val isbn = "0000000000001"

            mockMvc.put("$baseUrl/$isbn") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(BookDTO(isbn = isbn))
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BOOK $isbn",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("DELETE /books/{id}")
    inner class DeleteBook {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an book by id`() {
            DaoTestUtils.executeScripts(arrayOf("books/testUpdateBook.sql"))

            mockMvc.delete("$baseUrl/0000000000000") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an book by id not found`() {

            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: BOOK 5",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }
}
