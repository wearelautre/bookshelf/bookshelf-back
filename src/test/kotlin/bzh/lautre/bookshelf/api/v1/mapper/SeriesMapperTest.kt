package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.BookSeriesDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalBookTypeDTO
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.business.EditorBusiness
import bzh.lautre.bookshelf.business.SeriesBusiness
import bzh.lautre.bookshelf.exception.BadParameterException
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.model.Series
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mapstruct.factory.Mappers
import org.mockito.Mockito
import java.util.*

internal class SeriesMapperTest {

    lateinit var seriesMapper: SeriesMapper

    @BeforeEach
    fun init() {
        seriesMapper = Mappers.getMapper(SeriesMapper::class.java)
        seriesMapper.seriesBusiness = Mockito.mock(SeriesBusiness::class.java)
        seriesMapper.editorBusiness = Mockito.mock(EditorBusiness::class.java)
        seriesMapper.bookTypeBusiness = Mockito.mock(BookTypeBusiness::class.java)
    }

    @Test
    fun `missing bookType`() {
        val bookSeriesDTO = BookSeriesDTO(id = 1, name = "series 1")
        /* Given */
        Mockito.`when`(
            seriesMapper.seriesBusiness.findById(1)
        ).thenReturn(Optional.empty())

        val exception: BadParameterException = assertThrows(BadParameterException::class.java) { seriesMapper.map(bookSeriesDTO) }

        assertEquals("the BookType is mandatory", exception.message)
        assertEquals("BAD_PARAMETER", exception.error)
    }

    @Test
    fun `getSeries no series id`() {
        val seriesExpected = Series(name = "series 1", oneShot = false, bookType = BookType(name = "bookType 1"))

        val series = seriesMapper.map(BookSeriesDTO(name = "series 1", bookType = MinimalBookTypeDTO(name = "bookType 1")))

        assertEquals(seriesExpected.name, series.name, "name")
        assertEquals(seriesExpected.oneShot, series.oneShot, "oneShot")
        assertEquals(seriesExpected.bookType.name, series.bookType.name, "bookType.name")
    }


    @Test
    fun `getSeries series id exist but not found`() {
        val bookSeriesDTO = BookSeriesDTO(id = 1, name = "series 1", bookType = MinimalBookTypeDTO(name = "bookType 1"))

        Mockito.`when`(
            seriesMapper.seriesBusiness.findById(1)
        ).thenReturn(Optional.empty())

        val seriesExpected = Series(name = "series 1", oneShot = false, bookType = BookType(name = "bookType 1"))

        val series = seriesMapper.map(bookSeriesDTO)

        assertEquals(seriesExpected.name, series.name, "name")
        assertEquals(seriesExpected.oneShot, series.oneShot, "oneShot")
        assertEquals(seriesExpected.bookType.name, series.bookType.name, "bookType.name")
    }

    @Test
    fun `getSeries series id and bookType id exist but not found`() {
        val bookSeriesDTO = BookSeriesDTO(id = 1, name = "series 1", bookType = MinimalBookTypeDTO(id = 1,name = "bookType 1"))

        Mockito.`when`(
            seriesMapper.seriesBusiness.findById(1)
        ).thenReturn(Optional.empty())

        Mockito.`when`(
            seriesMapper.bookTypeBusiness.findById(1)
        ).thenReturn(Optional.empty())

        val seriesExpected = Series(name = "series 1", oneShot = false, bookType = BookType(name = "bookType 1"))

        val series = seriesMapper.map(bookSeriesDTO)

        assertEquals(seriesExpected.name, series.name, "name")
        assertEquals(seriesExpected.oneShot, series.oneShot, "oneShot")
        assertEquals(seriesExpected.bookType.name, series.bookType.name, "bookType.name")
    }

    @Test
    fun `getSeries series id and bookType id exist`() {
        val bookSeriesDTO = BookSeriesDTO(id = 1, name = "series 2", oneShot = true,bookType = MinimalBookTypeDTO(id = 1,name = "bookType 1"))

        Mockito.`when`(
            seriesMapper.seriesBusiness.findById(1)
        ).thenReturn(Optional.of(Series(name = "series 1", oneShot = false, bookType = BookType(id= 1, name = "bookType 0"))))

        Mockito.`when`(
            seriesMapper.bookTypeBusiness.findById(1)
        ).thenReturn(Optional.of(BookType(id= 1, name = "bookType 1")))

        val seriesExpected = Series(id = 1, name = "series 2", oneShot = true, bookType = BookType(id = 1, name = "bookType 1"))

        val series = seriesMapper.map(bookSeriesDTO)

        assertEquals(seriesExpected.name, series.name, "name")
        assertEquals(seriesExpected.oneShot, series.oneShot, "oneShot")
        assertEquals(seriesExpected.bookType.name, series.bookType.name, "bookType.name")
    }


}