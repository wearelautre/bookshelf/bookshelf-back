package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.DaoTestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.put
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
class ArtistsApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper,
) {
    val baseUrl = "/artists"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    private fun mapToArtists(index: Long): ArtistDTO {
        return ArtistDTO(index, "artist $index", mutableListOf(), mutableListOf())
    }

    private fun mapToMinimalArtists(index: Long): MinimalArtistDTO {
        return MinimalArtistDTO(index, "artist $index")
    }

    @Nested
    @DisplayName("GET /artists")
    inner class GetArtists {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 artists without search`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testGet1stPageOf5Artists.sql"))

            val artists = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {

                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get second page of 5 artists without search`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testGet1stPageOf5Artists.sql"))

            val artists = mutableListOf<Long>(5, 6, 7, 8, 9).map { mapToArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "1"
            parameters["size"] = "5"
            parameters["direction"] = "ASC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 artists without search order desc`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testGet1stPageOf5Artists.sql"))

            val artists = mutableListOf<Long>(1, 10, 2, 3, 4).map { mapToArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["direction"] = "DESC"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 artists with 'or' search on name`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testGet1stPageOf5Artists.sql"))

            val artists = mutableListOf<Long>(2, 9).map { mapToArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*2|'name:*9"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get first page of 5 artists with search on name`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testGet1stPageOf5Artists.sql"))

            val artists = mutableListOf<Long>(6).map { mapToArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "0"
            parameters["size"] = "5"
            parameters["search"] = "name:*6"
            mockMvc.get(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /artists/autocomplete")
    inner class GetArtistsAutoComplete {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 artists autocomplete no filter`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            val artists = mutableListOf<Long>(5, 1, 2, 3, 4).map { mapToMinimalArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 1 artist autocomplete with filter on name`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            val artists = mutableListOf<Long>(3).map { mapToMinimalArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["name"] = "3"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 artists autocomplete with roles order`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            val artists = mutableListOf<Long>(1, 4, 5, 2, 3).map { mapToMinimalArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["withRolesFirst"] = "role 1"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 artists autocomplete with roles order and name filter`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            val artists = mutableListOf<Long>(3).map { mapToMinimalArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["name"] = "3"
            parameters["withRolesFirst"] = "role 1"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 artists autocomplete with series order`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            val artists = mutableListOf<Long>(5, 1, 2, 3, 4).map { mapToMinimalArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["withSeriesFirst"] = "series 2"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 artists autocomplete with series order and name filter`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            val artists = mutableListOf<Long>(5).map { mapToMinimalArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["name"] = "5"
            parameters["withSeriesFirst"] = "series 1"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 artists autocomplete with series and role order`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            val artists = mutableListOf<Long>(5, 3, 1, 2, 4).map { mapToMinimalArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["withSeriesFirst"] = "series 2"
            parameters["withRolesFirst"] = "role 3"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get 5 artists autocomplete with series and role order and name filter`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            val artists = mutableListOf<Long>(5).map { mapToMinimalArtists(it) }

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["name"] = "5"
            parameters["withSeriesFirst"] = "series 1"
            parameters["withRolesFirst"] = "role 2"
            mockMvc.get("$baseUrl/autocomplete") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(artists), true)
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /artists/{id}")
    inner class GetArtistById {

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get an artist by id`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistAutocomplete.sql"))

            mockMvc.get("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ArtistDTO(
                                    5,
                                    "artist 5",
                                    mutableListOf(),
                                    mutableListOf("role 1", "role 2", "role 3")
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = [])
        fun `should get not get artist by id not found`() {

            mockMvc.get("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: ARTIST 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("PUT /artists/{id}")
    inner class PutArtist {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.put("$baseUrl/5") {
                content = ArtistDTO(5, "new artist 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.put("$baseUrl/5") {
                content = ArtistDTO(5, "new artist 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should not update an artist path id and body id not same `() {
            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    ArtistDTO(
                        6,
                        "new artist 5",
                        mutableListOf(WebLinksDTO(value = "toto", type = WebLinksDTO.Type.TWITTER))
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isBadRequest() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an artist by id`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistUpdate.sql"))

            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    ArtistDTO(
                        5,
                        "new artist 5",
                        mutableListOf(WebLinksDTO(value = "toto", type = WebLinksDTO.Type.TWITTER))
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ArtistDTO(
                                    5,
                                    "new artist 5",
                                    mutableListOf(WebLinksDTO(id = 2, value = "toto", type = WebLinksDTO.Type.TWITTER)),
                                    mutableListOf("role 1", "role 2", "role 3")
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an artist by id with weblinks already existing`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistUpdate.sql"))

            mockMvc.put("$baseUrl/5") {
                content = objectMapper.writeValueAsString(
                    ArtistDTO(
                        5, "new artist 5", mutableListOf(
                            WebLinksDTO(value = "toto", type = WebLinksDTO.Type.TWITTER),
                            WebLinksDTO(id = 1, value = "test", type = WebLinksDTO.Type.TWITTER)
                        )
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ArtistDTO(
                                    5, "new artist 5",
                                    mutableListOf(
                                        WebLinksDTO(id = 2, value = "toto", type = WebLinksDTO.Type.TWITTER),
                                        WebLinksDTO(id = 1, value = "test", type = WebLinksDTO.Type.TWITTER)
                                    ),
                                    mutableListOf("role 1", "role 2", "role 3")
                                )
                            ), true
                        )
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should update an artist by id not found`() {

            mockMvc.put("$baseUrl/1") {
                content = objectMapper.writeValueAsString(
                    ArtistDTO(
                        1, "new artist 1", mutableListOf(
                            WebLinksDTO(value = "toto", type = WebLinksDTO.Type.TWITTER),
                            WebLinksDTO(id = 1, value = "test", type = WebLinksDTO.Type.TWITTER)
                        )
                    )
                )
                contentType = MediaType.APPLICATION_JSON
            }

                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: ARTIST 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("DELETE /artists/{id}")
    inner class DeleteArtist {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an artist by id`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testArtistUpdate.sql"))

            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an artist by id not found`() {

            mockMvc.delete("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: ARTIST 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }

    @Nested
    @DisplayName("GET /artists/{id}/contracts")
    inner class GetArtistContracts {

        private fun getSeries(tome: Long): BookSeriesDTO {
            return BookSeriesDTO(
                name = "series 1",
                id = 1,
                status = SeriesStatusDTO(
                    totalCount = 2,
                    status = SeriesStatusDTO.Status.UNKNOWN
                ),
                oneShot = false,
                tome = tome,
                bookType = MinimalBookTypeDTO(id = 1, name = "bookType 1"),
                seriesCycle = null
            )
        }

        @Test
        fun `should get an artist's contracts by id`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testGetArtistContract.sql"))

            val editor = MinimalEditorDTO(name = "editor 1")

            val roles = mutableListOf(
                BookWithRoleDTO(
                    role = RoleDTO(1, "role 1"),
                    books = mutableListOf(
                        MinimalBookDTO(
                            isbn = "0000000001112",
                            title = "title 2",
                            series = getSeries(2),
                            editor = editor,
                        ),
                    )
                ),
                BookWithRoleDTO(
                    role = RoleDTO(2, "role 2"),
                    books = mutableListOf(
                        MinimalBookDTO(
                            isbn = "0000000001111",
                            title = "title 1",
                            series = getSeries(1),
                            editor = editor,
                        ),
                    )
                )
            )

            mockMvc.get("$baseUrl/1/contracts") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(roles))
                    }
                }
        }

        @Test
        fun `should get an artist's contracts by id empty`() {
            DaoTestUtils.executeScripts(arrayOf("artists/testGetArtistContract.sql"))

            mockMvc.get("$baseUrl/2/contracts") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(objectMapper.writeValueAsString(mutableListOf<BookWithRoleDTO>()))
                    }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete an artist by id not found`() {

            mockMvc.delete("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: ARTIST 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }
}
