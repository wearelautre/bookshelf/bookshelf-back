package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.api.v1.model.manual.TaskCountDTO
import bzh.lautre.bookshelf.config.DockerMySQLDataSourceInitializer
import bzh.lautre.bookshelf.utils.DaoTestUtils
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.util.LinkedMultiValueMap
import org.testcontainers.junit.jupiter.Testcontainers
import java.math.BigDecimal
import java.time.LocalDate


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = [DockerMySQLDataSourceInitializer::class])
@AutoConfigureMockMvc
@Testcontainers
internal class TasksApiImplTest @Autowired constructor(
    val mockMvc: MockMvc,
    val objectMapper: ObjectMapper
) {
    val baseUrl = "/tasks"

    companion object {
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            DaoTestUtils.performLiquibaseUpdate()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            DaoTestUtils.performLiquibaseDrop()
        }
    }

    @BeforeEach
    @Throws(Exception::class)
    fun beforeTests() {
        DaoTestUtils.executeScripts(arrayOf("all/cleanup.sql"))
    }

    private fun mapToTasks(index: Long): TaskDTO {
        return TaskDTO(
            id = index,
            createDate = LocalDate.parse("2022-07-14"),
            extra = "extra $index",
            type = if (index < 10) TaskTypeEnumDTO.ADD_BOOK else TaskTypeEnumDTO.OTHER,
            status = if (index in 1..5 || index in 11..15) TaskStatusEnumDTO.TODO else TaskStatusEnumDTO.DONE
        )
    }

    @Nested
    @DisplayName("POST /tasks")
    inner class PostTasks {
        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should add a task`() {
            val date = LocalDate.now()
            val task = TaskDTO(
                extra = "bestExtra",
                createDate = date,
                type = TaskTypeEnumDTO.ADD_BOOK,
                status = TaskStatusEnumDTO.TODO
            )

            mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(task)
            }
                .andExpect {
                    status { isOk() }
                    content {
                        val createdTask = TaskDTO(
                            id = 1,
                            extra = "bestExtra",
                            createDate = date,
                            type = TaskTypeEnumDTO.ADD_BOOK,
                            status = TaskStatusEnumDTO.TODO
                        )

                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        json(objectMapper.writeValueAsString(createdTask))
                    }

                }
        }
    }

    @Nested
    @DisplayName("GET /tasks/{status}/count")
    inner class GetTasksByStatusTasks {
        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should get all TODO tasks per types`() {
            DaoTestUtils.executeScripts(arrayOf("tasks/testGetTasks.sql"))

            val taskCountDTO = TaskCountDTO()
            taskCountDTO["ALL"] = BigDecimal(8)
            taskCountDTO["ADD_BOOK"] = BigDecimal(4)
            taskCountDTO["OTHER"] = BigDecimal(4)

            mockMvc.get("$baseUrl/TODO/count") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        json(objectMapper.writeValueAsString(taskCountDTO))
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should get all DONE tasks per types`() {
            DaoTestUtils.executeScripts(arrayOf("tasks/testGetTasks.sql"))

            val taskCountDTO = TaskCountDTO()
            taskCountDTO["ALL"] = BigDecimal(12)
            taskCountDTO["ADD_BOOK"] = BigDecimal(6)
            taskCountDTO["OTHER"] = BigDecimal(6)

            mockMvc.get("$baseUrl/DONE/count") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        json(objectMapper.writeValueAsString(taskCountDTO))
                    }

                }
        }
    }

    @Nested
    @DisplayName("GET /tasks")
    inner class GetTasks {
        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should get ADD_BOOK TODO tasks paginated`() {
            DaoTestUtils.executeScripts(arrayOf("tasks/testGetTasks.sql"))

            val page = PageOfTaskDTO(
                currentPage = 0,
                totalPages = 1,
                totalElements = 4,
                list = mutableListOf<Long>(1, 2, 3, 4).map { mapToTasks(it) }.toMutableList()
            )

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["size"] = "5"
            mockMvc.get("$baseUrl/ADD_BOOK/TODO") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        json(objectMapper.writeValueAsString(page))
                    }

                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should get OTHER DONE tasks paginated page 2`() {
            DaoTestUtils.executeScripts(arrayOf("tasks/testGetTasks.sql"))

            val page = PageOfTaskDTO(
                currentPage = 1,
                totalPages = 2,
                totalElements = 6,
                list = mutableListOf<Long>(20).map { mapToTasks(it) }.toMutableList()
            )

            val parameters = LinkedMultiValueMap<String, String>()
            parameters["page"] = "1"
            parameters["size"] = "5"
            mockMvc.get("$baseUrl/OTHER/DONE") {
                contentType = MediaType.APPLICATION_JSON
                params = parameters
            }
                .andExpect {
                    status { isOk() }
                    content {
                        contentType(MediaType.APPLICATION_JSON_VALUE)
                        json(objectMapper.writeValueAsString(page))
                    }

                }
        }
    }

    @Nested
    @DisplayName("DELETE /tasks")
    inner class DeleteTasks {

        @Test
        fun `should throw unauthorized 401`() {
            mockMvc.delete("$baseUrl/5") {
                content = BookTypeDTO(5, "new bookType 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isUnauthorized() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["TOTO"])
        fun `should throw forbidden`() {
            mockMvc.delete("$baseUrl/5") {
                content = BookTypeDTO(5, "new bookType 5")
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isForbidden() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete a tasks by id`() {
            DaoTestUtils.executeScripts(arrayOf("tasks/testGetTasks.sql"))

            mockMvc.delete("$baseUrl/5") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNoContent() }
                }
        }

        @Test
        @WithMockUser(username = "mockedUser", roles = ["ADMIN"])
        fun `should delete a tasks by id not found`() {

            mockMvc.delete("$baseUrl/1") {
                contentType = MediaType.APPLICATION_JSON
            }
                .andExpect {
                    status { isNotFound() }
                    content {
                        contentType(MediaType.APPLICATION_JSON)
                        json(
                            objectMapper.writeValueAsString(
                                ErrorDTO(
                                    message = "The resource doesn't exists: TASK 1",
                                    code = "404",
                                    error = "RESOURCE_NOT_FOUND"
                                )
                            )
                        )
                    }
                }
        }
    }
}
