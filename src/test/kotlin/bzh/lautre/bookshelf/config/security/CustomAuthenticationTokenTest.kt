package bzh.lautre.bookshelf.config.security

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class CustomAuthenticationTokenTest {

    val user = UserDetails(
        id = 2,
        email = "email@toto.com",
        name = "Login"
    )

    @Nested
    inner class IsAuthenticated {
        @Test
        fun `true`() {
            assertTrue(CustomAuthenticationToken(user).isAuthenticated)
        }
    }

    @Nested
    inner class GetPrincipal {
        @Test
        fun getPrincipal() {
            assertEquals(CustomAuthenticationToken(user).principal, user)
        }
    }

    @Nested
    inner class GetCredentials {
        @Test
        fun getCredentials() {
            assertEquals(CustomAuthenticationToken(user).credentials, "email@toto.com")
        }
    }
}