package bzh.lautre.bookshelf.config

import org.testcontainers.containers.MySQLContainer

object MySQLContainerManager {
    var MYSQL_CONTAINER: MySQLContainer<MySQLTestContainer>? = null

    init {
        MYSQL_CONTAINER = MySQLTestContainer.instance
        MYSQL_CONTAINER!!.start()
    }
}