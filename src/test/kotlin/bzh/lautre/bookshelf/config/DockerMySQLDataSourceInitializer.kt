package bzh.lautre.bookshelf.config

import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.support.TestPropertySourceUtils

class DockerMySQLDataSourceInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
    override fun initialize(applicationContext: ConfigurableApplicationContext) {
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
            applicationContext,
            "spring.datasource.url=" + MySQLContainerManager.MYSQL_CONTAINER!!.jdbcUrl,
            "spring.datasource.username=" + MySQLContainerManager.MYSQL_CONTAINER!!.username,
            "spring.datasource.password=" + MySQLContainerManager.MYSQL_CONTAINER!!.password
        )
    }
}