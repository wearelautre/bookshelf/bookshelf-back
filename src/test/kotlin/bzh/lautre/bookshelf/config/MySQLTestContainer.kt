package bzh.lautre.bookshelf.config

import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.MySQLContainer


class MySQLTestContainer private constructor() : MySQLContainer<MySQLTestContainer>(IMAGE_VERSION) {
    override fun start() {
        super.start()
        System.setProperty("DB_URL", container?.jdbcUrl!!)
        System.setProperty("DB_USERNAME", container?.username!!)
        System.setProperty("DB_PASSWORD", container?.password!!)
    }

    override fun stop() {
        //do nothing, JVM handles shut down
    }

    companion object {
        private const val IMAGE_VERSION = "mysql:8.0"
        private var container: MySQLTestContainer? = null

        @JvmStatic
        val instance: MySQLTestContainer
            get() {
                if (container == null) {
                    container = MySQLTestContainer()
                        .withDatabaseName("bookshelf")
                        .withCommand("mysqld", "--log_bin_trust_function_creators=1")
                }
                return container as MySQLTestContainer
            }

        @JvmStatic
        @DynamicPropertySource
        fun properties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", container!!::getJdbcUrl)
            registry.add("spring.datasource.driver-class-name") { "com.mysql.cj.jdbc.Driver" }
            registry.add("spring.datasource.username", container!!::getUsername)
            registry.add("spring.datasource.password", container!!::getPassword)

            registry.add("spring.liquibase.url", container!!::getJdbcUrl)
            registry.add("spring.liquibase.default-schema", container!!::getDatabaseName)
            registry.add("spring.liquibase.user", container!!::getUsername)
            registry.add("spring.liquibase.password", container!!::getPassword)
        }
    }
}
