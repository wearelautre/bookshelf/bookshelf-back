package bzh.lautre.bookshelf

import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.jwt.JwtDecoder
import java.time.Instant


@TestConfiguration
class TestSecurityConfig {
    companion object {
        const val AUTH0_TOKEN: String = "token"
    }

    @Bean
    fun jwtDecoder(): JwtDecoder {
        return JwtDecoder {
            Jwt(
                AUTH0_TOKEN,
                Instant.now(),
                Instant.now().plusSeconds(30),
                mapOf("header" to "header1"),
                mapOf("email" to "account.2@mail.com"),
            )
        }
    }
}