package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.model.Task
import bzh.lautre.bookshelf.model.TaskStatusEnum
import bzh.lautre.bookshelf.model.TaskTypeEnum
import bzh.lautre.bookshelf.repository.TaskRepository
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.verify
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class TaskBusinessImplTest {
    @Mock
    lateinit var mockTaskRepository: TaskRepository

    @InjectMocks
    lateinit var taskBusiness: TaskBusinessImpl

    @Test
    fun `search all results per status`() {
        val list = listOf(Task())

        /* Given */
        `when`(mockTaskRepository.findAllByStatusOrderByCreateDateDesc(
            TaskStatusEnum.DONE)).thenReturn(list)

        /* When */
        assertEquals(list, taskBusiness.getAllTasksPerStatus(TaskStatusEnum.DONE))

        /* Then */
        verify(mockTaskRepository).findAllByStatusOrderByCreateDateDesc(
            TaskStatusEnum.DONE
        )
    }

    @Test
    fun findById() {
        val task = Task()
        task.id = 1

        /* Given */
        `when`(mockTaskRepository.findById(task.id!!)).thenReturn(Optional.of(task))

        /* When */
        assertEquals(task, taskBusiness.findById(task.id!!).get())

        /* Then */
        verify(mockTaskRepository).findById(task.id!!)
    }


    @Test
    fun `delete success`() {
        val task = Task()
        task.id = 1

        /* Given */
        org.mockito.kotlin.doNothing().`when`(mockTaskRepository).delete(task)
        `when`(mockTaskRepository.findById(task.id!!))
            .thenReturn(Optional.empty())

        /* When */
        assertFalse(taskBusiness.delete(task))

        /* Then */
        verify(mockTaskRepository).delete(task)
    }

    @Test
    fun `delete error`() {
        val task = Task()
        task.id = 1

        /* Given */
        org.mockito.kotlin.doNothing().`when`(mockTaskRepository).delete(task)
        `when`(mockTaskRepository.findById(task.id!!))
            .thenReturn(Optional.of(task))

        /* When */
        assertTrue(taskBusiness.delete(task))

        /* Then */
        verify(mockTaskRepository).delete(task)
    }

    @Test
    fun `set task as done true`() {
        /* Given */
        `when`(mockTaskRepository.setTaskStatus(1, TaskStatusEnum.DONE)).thenReturn(1)

        /* When */
        assertTrue(taskBusiness.setTaskAsDone(1))

        /* Then */
        verify(mockTaskRepository).setTaskStatus(
            1,
            TaskStatusEnum.DONE
        )
    }

    @Test
    fun `set task as done false`() {
        /* Given */
        `when`(mockTaskRepository.setTaskStatus(1, TaskStatusEnum.DONE)).thenReturn(0)

        /* When */
        assertFalse(taskBusiness.setTaskAsDone(1))

        /* Then */
        verify(mockTaskRepository).setTaskStatus(
            1,
            TaskStatusEnum.DONE
        )
    }

    @Test
    fun `cleanAddBookTask exist`() {
        val isbn = "isbn"
        val optional = Optional.of(Task(id = 3))
        /* Given */
        `when`(mockTaskRepository.findByTypeIsAndExtraIs(TaskTypeEnum.ADD_BOOK, isbn)).thenReturn(optional)

        /* When */
        taskBusiness.cleanAddBookTask(isbn)

        /* Then */
        verify(mockTaskRepository).findByTypeIsAndExtraIs(
            TaskTypeEnum.ADD_BOOK,
            isbn
        )

        /* Then */
        verify(mockTaskRepository).setTaskStatus(
            3,
            TaskStatusEnum.DONE
        )
    }

    @Test
    fun `cleanAddBookTask doesn't exist`() {
        val isbn = "isbn"
        val optional = Optional.empty<Task>()
        /* Given */
        `when`(mockTaskRepository.findByTypeIsAndExtraIs(TaskTypeEnum.ADD_BOOK, isbn)).thenReturn(optional)

        /* When */
        taskBusiness.cleanAddBookTask(isbn)

        /* Then */
        verify(mockTaskRepository).findByTypeIsAndExtraIs(
            TaskTypeEnum.ADD_BOOK,
            isbn
        )

        /* Then */
        verify(mockTaskRepository, never()).setTaskStatus(
            3,
            TaskStatusEnum.DONE
        )
    }


    /*
    *
     override fun cleanAddBookTask(isbn: String) {
        taskRepository.findByTypeIsAndExtraIs(TaskTypeEnum.ADD_BOOK, isbn)
            .ifPresent {
                taskRepository.setTaskStatus(it.id!!, TaskStatusEnum.DONE)
            }
    }
    *
    *
    * */
}
