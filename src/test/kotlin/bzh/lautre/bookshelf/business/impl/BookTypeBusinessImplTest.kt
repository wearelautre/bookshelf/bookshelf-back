package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.repository.BookTypeRepository
import bzh.lautre.bookshelf.specification.BookTypeSpecification
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.*
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.domain.Specification
import java.util.*


@ExtendWith(MockitoExtension::class)
internal class BookTypeBusinessImplTest {
    @Mock
    lateinit var mockBookTypeRepository: BookTypeRepository

    @InjectMocks
    lateinit var bookTypeBusiness: BookTypeBusinessImpl

    @Test
    fun allBookTypes() {
        val list = listOf(BookType("name", 1))

        /* Given */
        Mockito.`when`(mockBookTypeRepository.findAll()).thenReturn(list)

        /* When */
        assertEquals(list, bookTypeBusiness.allBookTypes)
        /* Then */
        verify(mockBookTypeRepository).findAll()
    }

    @Test
    fun search() {
        val bookType = BookType("name", 1)
        val page = PageImpl(listOf(bookType))
        val specification = BookTypeSpecification(SpecSearchCriteria())

        Mockito.`when`(
            mockBookTypeRepository.findAll(
                any<Specification<BookType>>(),
                eq(PageRequest.of(0, 1))
            )
        ).thenReturn(page)

        /* When */
        assertEquals(page, bookTypeBusiness.search(specification, 0, 1))

        /* Then */
        verify(mockBookTypeRepository).findAll(
            Mockito.any<Specification<BookType>>(),
            Mockito.eq(PageRequest.of(0, 1))
        )
    }

    @Test
    fun `get by name doesn't exist`() {
        /* Given */
        Mockito.`when`(mockBookTypeRepository.findBookTypeByName("name")).thenReturn(Optional.empty())

        /* When */
        assertEquals("name", bookTypeBusiness.getByName("name").name)
        /* Then */
        verify(mockBookTypeRepository).findBookTypeByName("name")
    }

    @Test
    fun `get by name exist`() {
        val bookType = BookType("name", 1)

        /* Given */
        Mockito.`when`(mockBookTypeRepository.findBookTypeByName(bookType.name)).thenReturn(Optional.of(bookType))

        /* When */
        assertEquals(bookType, bookTypeBusiness.getByName("name"))

        /* Then */
        verify(mockBookTypeRepository).findBookTypeByName("name")
    }

    @Test
    fun `save without id name not present`(){
        val bookType = BookType("name  ", null)

        val savedBookType = BookType("name", 1)

        Mockito.`when`(mockBookTypeRepository.findBookTypeByName(bookType.name.trim())).thenReturn(Optional.empty())
        Mockito.`when`(mockBookTypeRepository.save(bookType)).thenReturn(savedBookType)
        assertEquals(savedBookType, bookTypeBusiness.save(bookType))

        verify(mockBookTypeRepository).findBookTypeByName(bookType.name.trim())
        verify(mockBookTypeRepository).save(bookType)
    }

    @Test
    fun `save without id name present`(){
        val bookType = BookType("name  ", null)

        val savedBookType = BookType("name", 1)

        Mockito.`when`(mockBookTypeRepository.findBookTypeByName(bookType.name.trim())).thenReturn(Optional.of(savedBookType))
        assertEquals(savedBookType, bookTypeBusiness.save(bookType))

        verify(mockBookTypeRepository).findBookTypeByName(bookType.name.trim())
    }

    @Test
    fun `save with id`(){
        val bookType = BookType("name  ", 1)

        val savedBookType = BookType("name", 1)

        Mockito.`when`(mockBookTypeRepository.save(bookType)).thenReturn(savedBookType)
        assertEquals(savedBookType, bookTypeBusiness.save(bookType))

        verify(mockBookTypeRepository).save(bookType)
    }

    @Test
    fun `save with DataIntegrityViolationException name present`(){
        val bookType = BookType("name  ", 1)

        val savedBookType = BookType("name", 1)
        Mockito.`when`(mockBookTypeRepository.findBookTypeByName(bookType.name.trim())).thenReturn(Optional.of(savedBookType))
        Mockito.`when`(mockBookTypeRepository.save(bookType))
            .thenThrow(DataIntegrityViolationException(""))
        assertEquals(savedBookType, bookTypeBusiness.save(bookType))

        verify(mockBookTypeRepository).inOrder {
            mockBookTypeRepository.save(bookType)
            mockBookTypeRepository.findBookTypeByName(bookType.name.trim())
        }
    }

    @Test
    fun `save with DataIntegrityViolationException name not present`(){
        val bookType = BookType("name  ", 1)

        val savedBookType = BookType("name", 1)

        Mockito.`when`(mockBookTypeRepository.save(bookType))
            .thenThrow(DataIntegrityViolationException(""))
            .thenReturn(savedBookType)
        Mockito.`when`(mockBookTypeRepository.findBookTypeByName(bookType.name.trim())).thenReturn(Optional.empty())
        assertEquals(savedBookType, bookTypeBusiness.save(bookType))

        verify(mockBookTypeRepository, times(2)).save(bookType)
        verify(mockBookTypeRepository).findBookTypeByName(bookType.name.trim())
    }

    @Test
    fun findById() {
        val bookType = BookType("name", 1)

        /* Given */
        Mockito.`when`(mockBookTypeRepository.findById(bookType.id!!)).thenReturn(Optional.of(bookType))

        /* When */
        assertEquals(bookType, bookTypeBusiness.findById(bookType.id!!).get())

        /* Then */
        verify(mockBookTypeRepository).findById(bookType.id!!)
    }

    @Test
    fun `delete success`() {
        val bookType = BookType("name", 1)

        /* Given */
        doNothing().`when`(mockBookTypeRepository).delete(bookType)
        Mockito.`when`(mockBookTypeRepository.findById(bookType.id!!)).thenReturn(Optional.empty())

        /* When */
        assertFalse(bookTypeBusiness.delete(bookType))

        /* Then */
        verify(mockBookTypeRepository).delete(bookType)
    }

    @Test
    fun `delete error`() {
        val bookType = BookType("name", 1)

        /* Given */
        doNothing().`when`(mockBookTypeRepository).delete(bookType)
        Mockito.`when`(mockBookTypeRepository.findById(bookType.id!!)).thenReturn(Optional.of(bookType))

        /* When */
        assertTrue(bookTypeBusiness.delete(bookType))

        /* Then */
        verify(mockBookTypeRepository).delete(bookType)
    }
}
