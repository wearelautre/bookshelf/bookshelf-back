package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.BookBusiness
import bzh.lautre.bookshelf.business.RoleBusiness
import bzh.lautre.bookshelf.model.Borrower
import bzh.lautre.bookshelf.repository.BorrowerRepository
import bzh.lautre.bookshelf.specification.BorrowerSpecification
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.*
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.jpa.domain.Specification
import java.util.*


@ExtendWith(MockitoExtension::class)
internal class BorrowerBusinessImplTest {
    @Mock
    lateinit var mockBorrowerRepository: BorrowerRepository

    @Mock
    lateinit var mockRoleBusiness: RoleBusiness

    @Mock
    lateinit var mockBookBusiness: BookBusiness

    @InjectMocks
    lateinit var borrowerBusiness: BorrowerBusinessImpl

    @Test
    fun search() {
        val borrower = Borrower("name", 1)
        val page = PageImpl(listOf(borrower))
        val specification = BorrowerSpecification(SpecSearchCriteria("", "name", SearchOperation.EQUALITY, "batou"))

        /* Given */
        Mockito.`when`(
            mockBorrowerRepository.findAll(
                any<Specification<Borrower>>(),
                eq(PageRequest.of(0, 1))
            )
        ).thenReturn(page)

        /* When */
        assertEquals(page, borrowerBusiness.search(specification, 0, 1))

        /* Then */
        verify(mockBorrowerRepository).findAll(
            Mockito.any<Specification<Borrower>>(),
            Mockito.eq(PageRequest.of(0, 1))
        )
    }

    @Test
    fun `save without id name not present`(){
        val borrower = Borrower("name", null)
        val savedBorrower = Borrower("name", 1)

        Mockito.`when`(mockBorrowerRepository.findByName(borrower.name!!)).thenReturn(Optional.empty())
        Mockito.`when`(mockBorrowerRepository.save(borrower)).thenReturn(savedBorrower)
        assertEquals(savedBorrower, borrowerBusiness.save(borrower))

        verify(mockBorrowerRepository).findByName(borrower.name!!)
        verify(mockBorrowerRepository).save(borrower)
    }

    @Test
    fun `save without id name present`(){
        val borrower = Borrower("name", null)
        val savedBorrower = Borrower("name", 1)

        Mockito.`when`(mockBorrowerRepository.findByName(borrower.name!!)).thenReturn(Optional.of(savedBorrower))
        assertEquals(savedBorrower, borrowerBusiness.save(borrower))

        verify(mockBorrowerRepository).findByName(borrower.name!!)
    }

    @Test
    fun `save with id`(){
        val borrower = Borrower("name", 1)
        val savedBorrower = Borrower("name", 1)

        Mockito.`when`(mockBorrowerRepository.save(borrower)).thenReturn(savedBorrower)
        assertEquals(savedBorrower, borrowerBusiness.save(borrower))

        verify(mockBorrowerRepository).save(borrower)
    }

    @Test
    fun `save with DataIntegrityViolationException name present`(){
        val borrower = Borrower("name", 1)
        val savedBorrower = Borrower("name", 1)

        Mockito.`when`(mockBorrowerRepository.findByName(borrower.name!!)).thenReturn(Optional.of(savedBorrower))
        Mockito.`when`(mockBorrowerRepository.save(borrower))
            .thenThrow(DataIntegrityViolationException(""))
        assertEquals(savedBorrower, borrowerBusiness.save(borrower))

        verify(mockBorrowerRepository).inOrder {
            mockBorrowerRepository.save(borrower)
            mockBorrowerRepository.findByName(borrower.name!!)
        }
    }

    @Test
    fun `save with DataIntegrityViolationException name not present`(){
        val borrower = Borrower("name", 1)
        val savedBorrower = Borrower("name", 1)
        Mockito.`when`(mockBorrowerRepository.save(borrower))
            .thenThrow(DataIntegrityViolationException(""))
            .thenReturn(savedBorrower)
        Mockito.`when`(mockBorrowerRepository.findByName(borrower.name!!)).thenReturn(Optional.empty())
        assertEquals(savedBorrower, borrowerBusiness.save(borrower))

        verify(mockBorrowerRepository, times(2)).save(borrower)
        verify(mockBorrowerRepository).findByName(borrower.name!!)
    }

    @Test
    fun findById() {
        val borrower = Borrower("name", 1)

        /* Given */
        Mockito.`when`(mockBorrowerRepository.findById(borrower.id!!)).thenReturn(Optional.of(borrower))

        /* When */
        assertEquals(borrower, borrowerBusiness.findById(borrower.id!!).get())

        /* Then */
        verify(mockBorrowerRepository).findById(borrower.id!!)
    }

    @Test
    fun `delete success`() {
        val borrower = Borrower("name", 1)

        /* Given */
        doNothing().`when`(mockBorrowerRepository).delete(borrower)
        Mockito.`when`(mockBorrowerRepository.findById(borrower.id!!)).thenReturn(Optional.empty())

        /* When */
        assertFalse(borrowerBusiness.delete(borrower))

        /* Then */
        verify(mockBorrowerRepository).delete(borrower)
    }

    @Test
    fun `delete error`() {
        val borrower = Borrower("name", 1)

        /* Given */
        doNothing().`when`(mockBorrowerRepository).delete(borrower)
        Mockito.`when`(mockBorrowerRepository.findById(borrower.id!!)).thenReturn(Optional.of(borrower))

        /* When */
        assertTrue(borrowerBusiness.delete(borrower))

        /* Then */
        verify(mockBorrowerRepository).delete(borrower)
    }
}
