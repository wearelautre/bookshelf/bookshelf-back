package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.VersionService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.test.util.ReflectionTestUtils

@ExtendWith(MockitoExtension::class)
internal class VersionServiceImplTest {

    @InjectMocks
    lateinit var versionService: VersionServiceImpl

    @Test
    fun `should return the version information with projectVersion equal to first tag`() {
        ReflectionTestUtils.setField(versionService, "tags", listOf("tag-1", "tag-2"))
        ReflectionTestUtils.setField(versionService, "pipelineId", "9")
        ReflectionTestUtils.setField(versionService, "jobId", "1")
        ReflectionTestUtils.setField(versionService, "commitSha", "fef1454")

        val information: VersionService.VersionInformation = versionService.getVersionInformation()

        assert(information.projectVersion == "tag-1")
        assert(information.pipelineId == "9")
        assert(information.jobId == "1")
        assert(information.commitSha == "fef1454")
    }

    @Test
    fun `should return the version information with projectVersion equal to commitSha`() {
        ReflectionTestUtils.setField(versionService, "pipelineId", "9")
        ReflectionTestUtils.setField(versionService, "jobId", "1")
        ReflectionTestUtils.setField(versionService, "commitSha", "fef1454")

        val information: VersionService.VersionInformation = versionService.getVersionInformation()

        assert(information.projectVersion == "fef1454")
        assert(information.pipelineId == "9")
        assert(information.jobId == "1")
        assert(information.commitSha == "fef1454")
    }



    @Test
    fun `should return the version information with pipelineId and jobId equal 0 if not set`() {
        ReflectionTestUtils.setField(versionService, "jobId", "@env.CI_JOB_ID@")
        ReflectionTestUtils.setField(versionService, "pipelineId", "@env.CI_PIPELINE_ID@")
        ReflectionTestUtils.setField(versionService, "commitSha", "fef1454")

        val information: VersionService.VersionInformation = versionService.getVersionInformation()

        assert(information.projectVersion == "fef1454")
        assert(information.pipelineId == "0")
        assert(information.jobId == "0")
        assert(information.commitSha == "fef1454")
    }
}