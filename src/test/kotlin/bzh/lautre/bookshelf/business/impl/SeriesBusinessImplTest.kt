package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Series
import bzh.lautre.bookshelf.repository.SeriesRepository
import bzh.lautre.bookshelf.specification.SeriesSpecification
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.verify
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class SeriesBusinessImplTest {
    @Mock
    lateinit var mockSeriesRepository: SeriesRepository

    @InjectMocks
    lateinit var seriesBusiness: SeriesBusinessImpl


    @Test
    fun `search all results`() {
        val series = Series()
        val page = PageImpl(listOf(series))
        val specification = SeriesSpecification(SpecSearchCriteria("", "name", SearchOperation.EQUALITY, "batou"))

        /* Given */
        `when`(mockSeriesRepository.findAll(
            any<Specification<Series>>(),
            eq(Pageable.unpaged()))
        ).thenReturn(page)

        /* When */
        assertEquals(page, seriesBusiness.search(specification, 0, 1, Sort.Direction.ASC, true))

        /* Then */
        verify(mockSeriesRepository).findAll(
            any<Specification<Series>>(),
            eq(Pageable.unpaged())
        )
    }


    @Test
    fun `search not all results`() {
        val series = Series()
        val page = PageImpl(listOf(series))
        val specification = SeriesSpecification(SpecSearchCriteria("", "name", SearchOperation.EQUALITY, "batou"))

        /* Given */
        `when`(mockSeriesRepository.findAll(
            any<Specification<Series>>(),
            eq(PageRequest.of(0,1)))
        ).thenReturn(page)

        /* When */
        assertEquals(page, seriesBusiness.search(specification, 0, 1, Sort.Direction.DESC, false))

        /* Then */
        verify(mockSeriesRepository).findAll(
            any<Specification<Series>>(),
            eq(PageRequest.of(0,1))
        )
    }

    @Test
    fun allBookTypes() {
        val list = listOf(Series())

        /* Given */
        `when`(mockSeriesRepository.findAll()).thenReturn(list)

        /* When */
        assertEquals(list, seriesBusiness.allSeries)

        /* Then */
        verify(mockSeriesRepository).findAll()
    }

    @Test
    fun count() {
        /* Given */
        `when`(mockSeriesRepository.count()).thenReturn(56)

        /* When */
        assertEquals(56, seriesBusiness.count)

        /* Then */
        verify(mockSeriesRepository).count()
    }


    @Test
    fun findById() {
        val series = Series()
        series.id = 1

        /* Given */
        `when`(mockSeriesRepository.findById(series.id!!)).thenReturn(Optional.of(series))

        /* When */
        assertEquals(series, seriesBusiness.findById(series.id!!).get())

        /* Then */
        verify(mockSeriesRepository).findById(series.id!!)
    }


    @Test
    fun `delete success`() {
        val series = Series()
        series.id = 1

        /* Given */
        doNothing().`when`(mockSeriesRepository).delete(series)
        `when`(mockSeriesRepository.findById(series.id!!))
            .thenReturn(Optional.empty())

        /* When */
        assertFalse(seriesBusiness.delete(series))

        /* Then */
        verify(mockSeriesRepository).delete(series)
    }

    @Test
    fun `delete error`() {
        val series = Series()
        series.id = 1

        /* Given */
        doNothing().`when`(mockSeriesRepository).delete(series)
        `when`(mockSeriesRepository.findById(series.id!!))
            .thenReturn(Optional.of(series))

        /* When */
        assertTrue(seriesBusiness.delete(series))

        /* Then */
        verify(mockSeriesRepository).delete(series)
    }
}
