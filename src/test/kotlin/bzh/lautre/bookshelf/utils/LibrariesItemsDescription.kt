package bzh.lautre.bookshelf.utils

data class LibrariesItemsDescription(
    val id: Long,
    val book: BooksDescription,
    val status: String = "UNREAD",
    val possessionStatus: String = "POSSESSED",
)
