package bzh.lautre.bookshelf.utils

import bzh.lautre.bookshelf.config.MySQLContainerManager
import liquibase.Contexts
import liquibase.LabelExpression
import liquibase.Liquibase
import liquibase.Scope
import liquibase.changelog.ChangeLogParameters
import liquibase.changelog.visitor.ChangeExecListener
import liquibase.command.CommandScope
import liquibase.command.core.UpdateCommandStep
import liquibase.command.core.helpers.ChangeExecListenerCommandStep
import liquibase.command.core.helpers.DatabaseChangelogCommandStep
import liquibase.command.core.helpers.DbUrlConnectionCommandStep
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import java.io.IOException
import java.io.OutputStream
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.SQLException
import java.util.*


/**
 * Classe abstraite pour l'excution de scripts sur la base de donnes.
 */
object DaoTestUtils {
    @Throws(ClassNotFoundException::class)
    fun executeScripts(sqlFiles: Array<String>) {
        if (sqlFiles.isNotEmpty()) {
            for (sqlScript in sqlFiles) {
                executeScript(sqlScript)
            }
        }
    }

    @Throws(ClassNotFoundException::class)
    private fun executeScript(sqlScript: String) {
        Class.forName("com.mysql.cj.jdbc.Driver")
        try {
            println("Executing script: $sqlScript")
            DriverManager.getConnection(
                System.getProperty("DB_URL"),
                System.getProperty("DB_USERNAME"),
                System.getProperty("DB_PASSWORD")
            ).use { conn ->
                conn.autoCommit = false
                String(
                    Objects.requireNonNull(
                        Thread.currentThread().contextClassLoader.getResourceAsStream("it-test/tests/${sqlScript}")
                    ).readAllBytes()
                )
                    .replace("^--.*".toRegex(RegexOption.MULTILINE), "")
                    .split(";\\R".toRegex())
                    .dropLastWhile { it.isEmpty() }
                    .map { "$it;".trim() }
                    .toTypedArray()
                    .forEach { sqlInstruction ->
                        val ps: PreparedStatement = conn.prepareStatement(sqlInstruction)
                        ps.execute()
                        conn.commit()
                    }
            }
        } catch (e: SQLException) {
            println(e.message)
        } catch (e: IOException) {
            println(e.message)
        }
    }
    
    @Throws(Exception::class)
    fun performLiquibaseUpdate() {
        Class.forName(MySQLContainerManager.MYSQL_CONTAINER?.driverClassName)
        DriverManager.getConnection(
            MySQLContainerManager.MYSQL_CONTAINER?.jdbcUrl,
            MySQLContainerManager.MYSQL_CONTAINER?.username,
            MySQLContainerManager.MYSQL_CONTAINER?.password
        ).use { connection ->

            val database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(JdbcConnection(connection))
            val changeLogFile = "/db/changelog/db.changeLog-master.yaml"

            val scopeObjects = mapOf(
                Scope.Attr.database.name to database,
                Scope.Attr.resourceAccessor.name to ClassLoaderResourceAccessor()
            )

            Scope.child(scopeObjects) {
                val updateCommand = CommandScope(*UpdateCommandStep.COMMAND_NAME).apply {
                    addArgumentValue(DbUrlConnectionCommandStep.DATABASE_ARG, database)
                    addArgumentValue(UpdateCommandStep.CHANGELOG_FILE_ARG, changeLogFile)
                    addArgumentValue(UpdateCommandStep.CONTEXTS_ARG, Contexts("all").toString())
                    addArgumentValue(UpdateCommandStep.LABEL_FILTER_ARG, LabelExpression().originalString)
                    addArgumentValue(
                        ChangeExecListenerCommandStep.CHANGE_EXEC_LISTENER_ARG,
                        null as ChangeExecListener?
                    )
                    addArgumentValue(DatabaseChangelogCommandStep.CHANGELOG_PARAMETERS, ChangeLogParameters(database))

                    setOutput(OutputStream.nullOutputStream()) // Suppress output to stdout (logging will still occur)
                }
                updateCommand.execute()
            }
        }
    }

    @Throws(Exception::class)
    fun performLiquibaseDrop() {
        Class.forName(MySQLContainerManager.MYSQL_CONTAINER?.driverClassName)
        DriverManager.getConnection(
            MySQLContainerManager.MYSQL_CONTAINER?.jdbcUrl,
            MySQLContainerManager.MYSQL_CONTAINER?.username,
            MySQLContainerManager.MYSQL_CONTAINER?.password
        ).use { conn ->
            val liquibase =
                Liquibase("/db/changelog/db.changeLog-master.yaml", ClassLoaderResourceAccessor(), JdbcConnection(conn))
            liquibase.dropAll()
        }
    }
}
