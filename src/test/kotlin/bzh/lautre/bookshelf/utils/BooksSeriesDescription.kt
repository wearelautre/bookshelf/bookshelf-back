package bzh.lautre.bookshelf.utils

data class BooksSeriesDescription (
    val id: Long = 1,
    val count: Long = 3,
    val readCount: Long = 0,
    val total: Long = 5,
    val status: String = "UNKNOWN",
)