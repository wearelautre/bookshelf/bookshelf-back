package bzh.lautre.bookshelf.utils

data class BooksDescription(
    val tome: Long,
    val series: BooksSeriesDescription = BooksSeriesDescription(),
    val bookType: Long = 1,
    val editor: Long = 1,
)