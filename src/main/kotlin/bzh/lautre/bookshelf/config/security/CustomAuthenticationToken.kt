package bzh.lautre.bookshelf.config.security

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class CustomAuthenticationToken(
    private val userDetails: UserDetails,
    authorities: MutableCollection<out GrantedAuthority> = mutableSetOf()
) : AbstractAuthenticationToken(authorities) {
    override fun isAuthenticated(): Boolean = true
    override fun getCredentials(): String = userDetails.email
    override fun getPrincipal(): UserDetails = userDetails
}