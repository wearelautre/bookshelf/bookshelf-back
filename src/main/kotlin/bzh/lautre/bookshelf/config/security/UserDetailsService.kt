package bzh.lautre.bookshelf.config.security

import bzh.lautre.bookshelf.model.Account
import bzh.lautre.bookshelf.repository.AccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserDetailsService(
    @Autowired private val accountRepository: AccountRepository
) {
    fun getUserFromDb(username: String): Account {
        return accountRepository.findByEmail(username)
            .orElseThrow { throw UsernameNotFoundException(String.format("Account [%s] not found", username)) }


    }

}
