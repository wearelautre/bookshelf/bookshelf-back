package bzh.lautre.bookshelf.config.security

data class UserDetails(
    val id: Long,
    val email: String,
    val name: String
)