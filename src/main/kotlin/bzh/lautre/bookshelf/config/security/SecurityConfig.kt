package bzh.lautre.bookshelf.config.security

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authorization.AuthorizationDecision
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.access.intercept.RequestAuthorizationContext
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter
import java.util.function.Supplier


@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true)
class SecurityConfig(
    private val customDetailsService: UserDetailsService
) {

    @Value("\${spring.security.oauth2.resourceserver.jwt.jwk-set-uri}")
    private val jwkSetUri: String? = null

    @Bean
    fun jwtDecoder(): JwtDecoder {
        return NimbusJwtDecoder.withJwkSetUri(jwkSetUri).build()
    }


    @Bean
    fun filterChain(http: HttpSecurity): SecurityFilterChain {
        return http
            .oauth2ResourceServer { oAuth2ResourceServerConfigurer ->
                oAuth2ResourceServerConfigurer
                    .jwt {
                        it
                            .decoder(jwtDecoder())
                            .jwtAuthenticationConverter { jwt ->
                                val account = customDetailsService.getUserFromDb(jwt.getClaim("email"))
                                CustomAuthenticationToken(
                                    UserDetails(
                                        id = account.id!!,
                                        email = account.email!!,
                                        name = account.login!!
                                    ),
                                    mutableSetOf(SimpleGrantedAuthority(account.role.name))
                                )
                            }

                    }
            }
            .authorizeHttpRequests {
                it.requestMatchers("/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html").permitAll()
                    .requestMatchers(HttpMethod.GET).permitAll()
                    .requestMatchers(HttpMethod.POST, "/libraries/{accountId}/**")
                    .access { authentication, context -> getAuthorizationDecision(authentication, context) }
                    .requestMatchers(HttpMethod.PUT, "/libraries/{accountId}/**")
                    .access { authentication, context -> getAuthorizationDecision(authentication, context) }
                    .requestMatchers(HttpMethod.PATCH, "/libraries/{accountId}/**")
                    .access { authentication, context -> getAuthorizationDecision(authentication, context) }
                    .requestMatchers(HttpMethod.DELETE, "/libraries/{accountId}/**")
                    .access { authentication, context -> getAuthorizationDecision(authentication, context) }
                    .requestMatchers(HttpMethod.POST).hasRole("ADMIN")
                    .requestMatchers(HttpMethod.PATCH).hasRole("ADMIN")
                    .requestMatchers(HttpMethod.DELETE).hasRole("ADMIN")
                    .requestMatchers(HttpMethod.PUT).hasRole("ADMIN")
                    .requestMatchers("/tasks/**").hasRole("ADMIN")
            }
            .sessionManagement { it.sessionCreationPolicy(SessionCreationPolicy.STATELESS) }
            .csrf { it.disable() }
            .build()
    }

    fun getAuthorizationDecision(
        authentication: Supplier<Authentication>,
        context: RequestAuthorizationContext
    ): AuthorizationDecision {
        val accountId = context.variables["accountId"]

        if (accountId != null) {
            print(authentication.get())
            print(authentication.get().principal as UserDetails)
            return AuthorizationDecision(
                (authentication.get().principal as UserDetails).id == accountId.toLong()
            )
        }
        return AuthorizationDecision(false)
    }

    @Bean
    fun corsFilter(): CorsFilter {
        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration()
        config.allowCredentials = true
        config.addAllowedOriginPattern("*")
        config.addAllowedHeader("*")
        config.addAllowedMethod("*")
        source.registerCorsConfiguration("/**", config)
        return CorsFilter(source)
    }

}
