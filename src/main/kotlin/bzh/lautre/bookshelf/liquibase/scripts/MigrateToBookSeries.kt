package bzh.lautre.bookshelf.liquibase.scripts

import liquibase.change.custom.CustomTaskChange
import liquibase.database.Database
import liquibase.database.jvm.JdbcConnection
import liquibase.exception.ValidationErrors
import liquibase.resource.ResourceAccessor
import java.sql.ResultSet

class MigrateToBookSeries : CustomTaskChange {

    private var resourceAccessor: ResourceAccessor? = null

    override fun getConfirmationMessage(): String {
        return "Migrate to BookSeries"
    }

    override fun setUp() {
    }

    override fun setFileOpener(resourceAccessor: ResourceAccessor?) {
        this.resourceAccessor = resourceAccessor
    }

    override fun validate(batabase: Database?): ValidationErrors {
        return ValidationErrors()
    }

    override fun execute(database: Database) {
        val dbConn: JdbcConnection = database.connection as JdbcConnection

        getOldData(dbConn)
            .map {
                val statement = dbConn.prepareStatement(""" 
                        INSERT INTO bookshelf.book_series (book_isbn, series_tome, series_id)
                        VALUES (?, ?, ?);
                    """)
                statement.setString(1, it.first)
                statement.setInt(2, it.second)
                statement.setLong(3, it.third)
                statement
            }
            .forEach { it.executeUpdate() }
    }

    private fun getOldData(dbConn: JdbcConnection): List<Triple<String, Int, Long>> {
        val data: MutableList<Triple<String, Int, Long>> = mutableListOf()
        val result: ResultSet = dbConn.createStatement().executeQuery(
            "SELECT b.isbn as book_isbn, b.tome as book_tome, b.series_id as series_id FROM book b"
        )
        while (result.next()) {
            data.add(Triple(result.getString("book_isbn"), result.getInt("book_tome"), result.getLong("series_id")))
        }
        return data
    }
}

