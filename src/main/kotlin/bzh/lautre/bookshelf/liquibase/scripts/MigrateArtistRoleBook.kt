package bzh.lautre.bookshelf.liquibase.scripts

import liquibase.change.custom.CustomTaskChange
import liquibase.database.Database
import liquibase.database.jvm.JdbcConnection
import liquibase.exception.ValidationErrors
import liquibase.resource.ResourceAccessor
import java.sql.PreparedStatement
import java.sql.ResultSet

class MigrateArtistRoleBook : CustomTaskChange {

    private var resourceAccessor: ResourceAccessor? = null

    override fun getConfirmationMessage(): String {
        // TODO add really message
        return "YEAH !!"
    }

    override fun setUp() {
    }

    override fun setFileOpener(resourceAccessor: ResourceAccessor?) {
        this.resourceAccessor = resourceAccessor
    }

    override fun validate(batabase: Database?): ValidationErrors {
        return ValidationErrors()
    }

    override fun execute(database: Database) {
        val dbConn: JdbcConnection = database.connection as JdbcConnection
        saveNewData(dbConn, getOldData(dbConn))
    }

    private fun getOldData(dbConn: JdbcConnection): HashMap<Pair<Long, Long>, MutableList<Long>> {
        val map: HashMap<Pair<Long, Long>, MutableList<Long>> = HashMap()
        val result: ResultSet = dbConn.createStatement().executeQuery(
            "SELECT distinct book_id, role_list_id, artist_id " +
                    "FROM book_artist_role " +
                    "INNER JOIN book_artist_role_role_list ON book_artist_roles_id = id"
        )
        while (result.next()) {
            val bookRole = Pair(result.getLong("book_id"), result.getLong("role_list_id"))
            if (map.containsKey(bookRole)) {
                map[bookRole]!!.add(result.getLong("artist_id"))
            } else {
                map[bookRole] = mutableListOf(result.getLong("artist_id"))
            }
        }
        return map
    }

    private fun saveNewData(dbConn: JdbcConnection, map: HashMap<Pair<Long, Long>, MutableList<Long>>) {
        val list: MutableList<PreparedStatement> = emptyList<PreparedStatement>().toMutableList()
        map.forEach { (t, u) ->
            run {
                list.add(preparedContractStatement(dbConn, t))
                list.addAll(u.map { preparedContractArtistStatement(dbConn, t, it) })
            }
        }

        list.forEach { it.executeUpdate() }
    }

    private fun preparedContractArtistStatement(dbConn: JdbcConnection, t: Pair<Long, Long>, artist: Long): PreparedStatement {
        val statement = dbConn.prepareStatement("INSERT INTO contract_artists value(?, ?, ?)")
        statement.setLong(1, t.first)
        statement.setLong(2, t.second)
        statement.setLong(3, artist)
        return statement
    }

    private fun preparedContractStatement(dbConn: JdbcConnection, t: Pair<Long, Long>): PreparedStatement {
        val statement = dbConn.prepareStatement("INSERT INTO contract value(?, ?)")
        statement.setLong(1, t.first)
        statement.setLong(2, t.second)
        return statement
    }
}

