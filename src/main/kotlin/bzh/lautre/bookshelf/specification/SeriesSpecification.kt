package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.*
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root
import org.springframework.data.jpa.domain.Specification

class SeriesSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Series>(criteria) {

    companion object {
        fun distinct(distinct: Boolean = true): Specification<Series> {
            return Specification { _, query: CriteriaQuery<*>, _ ->
                query.distinct(distinct)
                null
            }
        }
    }

    override fun toPredicate(root: Root<Series>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return when (this.criteria.key) {
            "bookType" ->
                build(
                    root.join<Series, BookType>("bookType"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )

            "libAccount" -> {
                val libraryItemRoot = root.join<Series, BookSeries>("bookList")
                    .join<BookSeries, Book>("book")
                    .join<Book, LibraryItem>("libraries")
                build(
                    libraryItemRoot,
                    this.criteria.operation, "account_id", this.criteria.value, builder
                )
            }

            "bookStatus" -> {
                val bookRoot = root.join<Series, BookSeries>("bookList").join<BookSeries, Book>("book")
                build(
                    bookRoot,
                    this.criteria.operation, "status", this.criteria.value, builder
                )
            }

            "bookPossessionStatus" -> {
                val bookRoot = root.join<Series, BookSeries>("bookList").join<BookSeries, Book>("book")
                return build(
                    bookRoot,
                    this.criteria.operation, "possessionStatus", this.criteria.value, builder
                )
            }

            "editor" ->
                build(
                    root.join<Series, Editor>("editor"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )

            else -> super.toPredicate(root, query, builder)
        }
    }
}

