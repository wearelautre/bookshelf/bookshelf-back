package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.exception.BadSearchParameterValueException
import bzh.lautre.bookshelf.model.*
import jakarta.persistence.criteria.*
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification


class LibraryItemSpecification(criteria: SpecSearchCriteria) : CommonSpecification<LibraryItem>(criteria) {
    companion object {

        fun orderBy(column: String, asc: Boolean = true): Specification<LibraryItem> {
            return Specification { root: Root<LibraryItem>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                val e: Expression<*> = root.get<String>(column)
                query.orderBy(if (asc) builder.asc(e) else builder.desc(e))
                null
            }
        }

        fun orderLibraryItemByBooks(direction: Sort.Direction, sort: BookSortField): Specification<LibraryItem> {
            return Specification { root: Root<LibraryItem>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                val joinBookSeries = root.join<LibraryItem, Book>("book")
                    .join<Book, BookSeries>("bookSeries")
                CommonBookSpecification.orderCommonBooks(joinBookSeries, direction, sort, query, builder)
                null
            }
        }
    }

    override fun toPredicate(root: Root<LibraryItem>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return when (this.criteria.key) {
            "possessionStatus" -> {
                if (this.criteria.value !== null && !PossessionStatusEnum.entries.map { it.name }
                        .contains(this.criteria.value)) {
                    throw BadSearchParameterValueException(
                        criteria.key!!, this.criteria.value.toString()
                    )
                }

                build(
                    root,
                    criteria.operation,
                    criteria.key,
                    PossessionStatusEnum.valueOf(this.criteria.value!!.toString()),
                    builder
                )
            }

            "status" ->
                build(
                    root,
                    criteria.operation,
                    criteria.key,
                    BookReadStatusEnum.valueOf(this.criteria.value!!.toString()),
                    builder
                )

            "bookType" ->
                build(
                    root.join<LibraryItem, Book>("book")
                        .join<Book, BookSeries>("bookSeries")
                        .join<Book, Series>("series")
                        .join<Series, BookType>("bookType"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )

            "editor" ->
                build(
                    root.join<LibraryItem, Book>("book")
                        .join<Book, BookSeries>("bookSeries")
                        .join<Book, Series>("series")
                        .join<Series, Editor>("editor"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )

            "series" ->
                build(
                    root.join<LibraryItem, Book>("book")
                        .join<Book, BookSeries>("bookSeries")
                        .join<Book, Series>("series"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )

            "account" ->
                build(
                    root.join<LibraryItem, Account>("account"),
                    this.criteria.operation, "id", this.criteria.value, builder
                )

            else -> super.toPredicate(root, query, builder)
        }
    }
}
