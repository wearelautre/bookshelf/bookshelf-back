package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SearchOperation.*
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.exception.BadSearchParameterException
import jakarta.persistence.criteria.*
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification


open class CommonSpecification<T>(
    protected val criteria: SpecSearchCriteria
) : Specification<T> {

    open fun orderBy(direction: Sort.Direction, sort: String): Specification<T> {
        return Specification { root: Root<T>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
            val orderBys = listOf(root.get<String>(sort))

            if (direction == Sort.Direction.ASC) {
                query.orderBy(orderBys.map { builder.asc(it) })
            } else {
                query.orderBy(orderBys.map { builder.desc(it) })
            }
            null
        }
    }

    override fun toPredicate(root: Root<T>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        try {
            return build(root, criteria.operation, criteria.key, criteria.value.toString(), builder)
        } catch (ex: IllegalArgumentException) {
            throw BadSearchParameterException(criteria.key!!, ex.message!!)
        }
    }

    protected fun build(
        root: From<*, *>,
        operation: SearchOperation?,
        key: String?,
        value: Any?,
        builder: CriteriaBuilder
    ): Predicate? {
        return when (operation) {
            EQUALITY -> builder.equal(root.get<Any>(key), value)
            NEGATION -> builder.notEqual(root.get<String>(key), value)
            GREATER_THAN -> builder.greaterThan(root.get(key), value.toString())
            LESS_THAN -> builder.lessThan(root.get(key), value.toString())
            LIKE -> builder.like(builder.lower(root.get(key)), value.toString().lowercase())
            STARTS_WITH -> builder.like(root.get(key), "${value}%")
            ENDS_WITH -> builder.like(root.get(key), "%${value}")
            CONTAINS -> builder.like(builder.lower(root.get(key)), "%${value}%".lowercase())
            IN -> {
                val inTest: CriteriaBuilder.In<String> = builder.`in`(root.get(key))
                "$value".split(",").forEach { inTest.value(it) }
                inTest
            }

            else -> null
        }
    }
}

