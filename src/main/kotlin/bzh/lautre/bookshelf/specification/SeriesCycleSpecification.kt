package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Series
import bzh.lautre.bookshelf.model.SeriesCycle
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root

class SeriesCycleSpecification(criteria: SpecSearchCriteria) : CommonSpecification<SeriesCycle>(criteria) {

    override fun toPredicate(root: Root<SeriesCycle>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return when (this.criteria.key) {
            "series" ->
                build(
                    root.join<SeriesCycle, Series>("series"),
                    this.criteria.operation, "id", this.criteria.value, builder
                )
            else -> super.toPredicate(root, query, builder)
        }
    }

}