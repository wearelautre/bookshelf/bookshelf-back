package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.model.Series
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Predicate
import jakarta.persistence.criteria.Root

class EditorSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Editor>(criteria) {

    override fun toPredicate(root: Root<Editor>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        query.distinct(true)
        return when (this.criteria.key) {
            "bookType" ->
                build(
                    root.join<Book, Series>("series").join<Series, BookType>("bookType"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )
            else -> super.toPredicate(root, query, builder)
        }
    }
}
