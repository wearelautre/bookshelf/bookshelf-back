package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.*
import org.springframework.data.domain.Sort
import jakarta.persistence.criteria.*

class CommonBookSpecification {
    companion object {
        fun <T> orderCommonBooks(
            joinBookSeries: Join<T, BookSeries>,
            direction: Sort.Direction,
            sort: BookSortField,
            query: CriteriaQuery<*>,
            builder: CriteriaBuilder
        ) {
            val tome: Expression<*> = joinBookSeries.get<String>("seriesTome")
            val joinSeries = joinBookSeries.join<T, Series>("series", JoinType.LEFT)
            val seriesName: Expression<*> = joinSeries.get<String>("name")

            val orderBys = if (sort == BookSortField.EDITOR) {
                listOf(joinSeries.join<Series, Editor>("editor").get<String>("name"), seriesName, tome)
            } else {
                listOf(seriesName, tome)
            }

            if (direction == Sort.Direction.ASC) {
                query.orderBy(orderBys.map { builder.asc(it) })
            } else {
                query.orderBy(orderBys.map { builder.desc(it) })
            }
        }
    }
}
