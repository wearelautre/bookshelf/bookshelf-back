package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Artist
import bzh.lautre.bookshelf.model.Contract
import bzh.lautre.bookshelf.model.Role
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import jakarta.persistence.criteria.*

class RoleSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Role>(criteria) {
    companion object {
        fun orderRoles(direction: Sort.Direction, sort: String): Specification<Role> {
            return Specification { root: Root<Role>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                val orderBys = mutableListOf<Expression<*>>()
                if (sort == "count") {
                    query.groupBy(listOf(root.get<Long>("id"), root.get("name")))
                    val countSort = builder.count(
                        root.join<Role, Contract>("contracts", JoinType.LEFT)
                            .join<Contract, Artist>("artists", JoinType.LEFT)
                            .get<Long>("id")
                    )
                    query.orderBy(listOf( builder.desc(countSort), builder.asc(root.get<String>("name"))))
                } else {
                    orderBys.add(root.get<String>(sort))

                    if (direction == Sort.Direction.ASC) {
                        query.orderBy(orderBys.map { builder.asc(it) })
                    } else {
                        query.orderBy(orderBys.map { builder.desc(it) })
                    }
                }
                null
            }
        }
    }
}
