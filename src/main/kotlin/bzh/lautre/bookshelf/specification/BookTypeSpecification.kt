package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.*
import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Join
import jakarta.persistence.criteria.Root
import org.springframework.data.jpa.domain.Specification

class BookTypeSpecification(criteria: SpecSearchCriteria) : CommonSpecification<BookType>(criteria) {

    companion object {
        fun distinct(distinct: Boolean = true): Specification<BookType> {
            return Specification { _, query: CriteriaQuery<*>, _ ->
                query.distinct(distinct)
                null
            }
        }

        private fun joinAccount(root: Root<BookType>): Join<LibraryItem, Account>? {
            return root.join<BookType, Series>("seriesList")
                .join<Series, Book>("bookList")
                .join<BookSeries, Book>("book")
                .join<Book, LibraryItem>("libraries")
                .join<LibraryItem, Account>("account")

        }

        fun getAllOrderByBookCount(accountId: Int? = null): Specification<BookType> {
            return Specification { root: Root<BookType>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                if (accountId != null) {
                    query.where(
                        builder.equal(
                            joinAccount(root)?.get<Long>("id"), accountId
                        )
                    )
                }

                query.groupBy(listOf(root.get<Long>("id"), root.get("name")))
                query.orderBy(
                    builder.desc(
                        builder.count(
                            root
                                .join<BookType, Series>("seriesList")
                                .join<Series, Book>("bookList")
                                .get<String>("isbn")
                        )
                    )
                )
                null
            }
        }

        fun getAllOrderByBookCountForAccount(accountId: Int): Specification<BookType> {
            return getAllOrderByBookCount(accountId)
        }
    }
}