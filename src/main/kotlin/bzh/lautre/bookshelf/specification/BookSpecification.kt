package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.*
import jakarta.persistence.criteria.*
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification

class BookSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Book>(criteria) {
    companion object {
        fun orderBy(column: String, asc: Boolean = true): Specification<Book> {
            return Specification { root: Root<Book>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                val e: Expression<*> = root.get<String>(column)
                query.orderBy(if (asc) builder.asc(e) else builder.desc(e))
                null
            }
        }

        fun orderBooks(direction: Sort.Direction, sort: BookSortField): Specification<Book> {
            return Specification { root: Root<Book>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                val joinBookSeries = root.join<Book, BookSeries>("bookSeries")
                CommonBookSpecification.orderCommonBooks(joinBookSeries, direction, sort, query, builder)
                null
            }
        }
    }

    override fun toPredicate(root: Root<Book>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return when (this.criteria.key) {
            "bookType" -> build(
                root.join<Book, BookSeries>("bookSeries").join<Book, Series>("series")
                    .join<Series, BookType>("bookType"), this.criteria.operation, "name", this.criteria.value, builder
            )

            "editor" -> build(
                root.join<Book, BookSeries>("bookSeries").join<Book, Series>("series").join<Series, Editor>("editor"),
                this.criteria.operation,
                "name",
                this.criteria.value,
                builder
            )

            "series" -> build(
                root.join<Book, BookSeries>("bookSeries").join<Book, Series>("series"),
                this.criteria.operation,
                "name",
                this.criteria.value,
                builder
            )

            else -> super.toPredicate(root, query, builder)
        }

    }
}
