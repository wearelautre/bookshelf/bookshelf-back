package bzh.lautre.bookshelf.model

enum class PossessionStatusEnum {
    POSSESSED, NOT_POSSESSED
}
