package bzh.lautre.bookshelf.model

import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import jakarta.persistence.*
import lombok.Data

@Data
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "book_type")
open class BookType(
        @Column(unique = true) open var name: String = "",
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(columnDefinition = "BIGINT") open var id: Long? = null,
        @OneToMany(mappedBy = "bookType", cascade = [CascadeType.PERSIST], fetch = FetchType.LAZY, targetEntity = Series::class)
        open val seriesList: MutableList<Series> = emptyList<Series>().toMutableList()
)
