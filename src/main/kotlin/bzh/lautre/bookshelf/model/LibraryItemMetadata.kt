package bzh.lautre.bookshelf.model

import jakarta.persistence.*
import lombok.Data
import java.math.BigDecimal

@Data
@Entity
@Table(name = "library_item_metadata")
class LibraryItemMetadata(
    @Id
    @Column(name = "id")
    //TODO : change column name to library_item_id
    var id: Long? = null,
    @OneToOne
    @JoinColumn(name = "id")
    var libraryItem: LibraryItem? = null,
    @Column(columnDefinition = "Decimal(10,2)")
    var price: BigDecimal? = null,
    var priceCurrency: String? = null,
    var acquisitionDate: String? = null,
    var offered: Boolean = false
) : java.io.Serializable