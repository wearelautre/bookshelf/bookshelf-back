package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import jakarta.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "artist")
open class Artist(
        @Column(unique = true) open var name: String? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) open var id: Long? = null,
        @ManyToMany(mappedBy = "artists", cascade = [CascadeType.ALL], targetEntity = Contract::class)
        open var contracts: List<Contract> = mutableListOf(),
        @OneToMany(mappedBy = "artists", cascade = [CascadeType.ALL], orphanRemoval = true, targetEntity = WebLinks::class)
        open var webLinks: List<WebLinks> = mutableListOf()
)
