package bzh.lautre.bookshelf.model

import lombok.EqualsAndHashCode
import java.io.Serializable
import jakarta.persistence.Column
import jakarta.persistence.Embeddable

@Embeddable
@EqualsAndHashCode
open class ContractId(
    @Column(name = "role_id", nullable = false) var roleId: Long? = null,
    @Column(name = "book_isbn", nullable = false) var bookIsbn: String? = null
): Serializable
