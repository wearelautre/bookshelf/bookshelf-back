package bzh.lautre.bookshelf.model

import java.io.Serializable

import jakarta.persistence.*
import lombok.Data

@Data
@Entity
@Table(name = "series_cycle")
open class SeriesCycle(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) open var id: Long? = null,
    @ManyToOne
    @JoinColumn(name = "series_id")
    open var series: Series? = null,
    @Column open var name: String = "",
    @OneToMany(mappedBy = "seriesCycle", fetch = FetchType.LAZY, targetEntity = BookSeries::class)
    open var bookList: MutableList<BookSeries> = mutableListOf(),
) : Serializable
