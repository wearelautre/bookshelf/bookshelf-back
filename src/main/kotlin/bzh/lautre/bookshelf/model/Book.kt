package bzh.lautre.bookshelf.model

import jakarta.persistence.*
import lombok.Data
import java.time.LocalDateTime

@Data
@Entity
class Book(
    @Id var isbn: String? = null,
    var arkId: String? = null,
    var title: String? = null,
    var year: String? = null,
    var collection: String? = null,
    var cover: String? = null,
    var creationDate: LocalDateTime = LocalDateTime.now(),
    var lastUpdateDate: LocalDateTime = LocalDateTime.now(),
    @OneToMany(mappedBy = "book", cascade = [CascadeType.ALL], orphanRemoval = true, targetEntity = Contract::class)
    var contracts: MutableList<Contract> = mutableListOf(),
    @PrimaryKeyJoinColumn
    @OneToOne(mappedBy = "book", cascade = [CascadeType.ALL])
    var bookSeries: BookSeries? = null,
    @PrimaryKeyJoinColumn
    @OneToMany(mappedBy = "book", cascade = [CascadeType.ALL])
    var libraries: List<LibraryItem> = mutableListOf(),
) {
    @OneToOne(mappedBy = "book", cascade = [CascadeType.ALL])
    @PrimaryKeyJoinColumn
    var metadata: BookMetadata? = null
}
