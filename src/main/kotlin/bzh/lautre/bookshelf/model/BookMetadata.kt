package bzh.lautre.bookshelf.model

import jakarta.persistence.*
import lombok.Data

@Data
@Entity
@Table(name = "book_metadata")
open class BookMetadata(
    @Id
    @Column(name = "book_isbn")
    open var isbn: String? = null,
    @OneToOne
    @MapsId
    @JoinColumn(name = "book_isbn")
    open var book: Book? = null,
    @Column(columnDefinition = "Decimal(10,2)")
    open var pageCount: Long? = 0,
) : java.io.Serializable