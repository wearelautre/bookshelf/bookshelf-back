package bzh.lautre.bookshelf.model

import lombok.Data
import java.util.*

import jakarta.persistence.*

@Data
@Entity
@Table(name = "task")
open class Task(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) open var id: Long? = null,
    open var createDate: Date = Date(),
    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    open var type: TaskTypeEnum = TaskTypeEnum.ADD_BOOK,
    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    open var status: TaskStatusEnum = TaskStatusEnum.TODO,
    @Column(unique = true)
    open var extra: String = "",
)
