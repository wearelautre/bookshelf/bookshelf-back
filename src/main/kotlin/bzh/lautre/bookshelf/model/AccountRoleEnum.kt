package bzh.lautre.bookshelf.model

enum class AccountRoleEnum {
    ROLE_ADMIN, ROLE_USER
}
