package bzh.lautre.bookshelf.model

enum class PublishedStatusEnum {
    PLANNED,
    ON_GOING,
    FINISHED,
    UNKNOWN;
}