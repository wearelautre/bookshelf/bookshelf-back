package bzh.lautre.bookshelf.model

import jakarta.persistence.*
import lombok.Data
import lombok.NoArgsConstructor
import java.io.Serializable
import java.time.LocalDate

@Data
@NoArgsConstructor
@Entity
@Table(name = "loan")
open class Loan(
    @ManyToOne open var libraryItem: LibraryItem = LibraryItem(),
    @ManyToOne open var borrower: Borrower = Borrower(),
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) open var id: Long? = null,
    @Column(nullable = false) open var borrowDate: LocalDate = LocalDate.now(),
    @Column open var returnDate: LocalDate? = null
) : Serializable
