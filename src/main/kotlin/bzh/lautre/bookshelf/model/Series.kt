package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import java.io.Serializable
import java.util.*

import jakarta.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "series")
open class Series(
    @Column open var name: String = "",
    @ManyToOne(targetEntity = Editor::class, fetch = FetchType.EAGER)
    open var editor: Editor = Editor(),
    @ManyToOne(fetch = FetchType.LAZY)
    open var bookType: BookType = BookType(),
    open var oneShot: Boolean = false,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Long? = null,
    @OneToMany(mappedBy = "series", fetch = FetchType.LAZY, targetEntity = BookSeries::class)
    open var bookList: MutableList<BookSeries> = mutableListOf(),
    @OneToMany(mappedBy = "series", orphanRemoval = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL], targetEntity = SeriesCycle::class)
    open var cycles: MutableList<SeriesCycle> = mutableListOf(),
    @Enumerated(EnumType.STRING) @Column(length = 8)
    open var publishedStatus: PublishedStatusEnum = PublishedStatusEnum.UNKNOWN,
    open var publishedCount: Int = 0,
    open var lastReadBookDate: Date = Date()
) : Serializable
