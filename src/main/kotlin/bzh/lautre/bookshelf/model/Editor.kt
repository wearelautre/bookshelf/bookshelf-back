package bzh.lautre.bookshelf.model

import jakarta.persistence.*
import lombok.Data

@Data
@Entity
@Table(name = "editor")
open class Editor(
    @Column(unique = true) open var name: String = "",
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(columnDefinition = "BIGINT") open var id: Long? = null,
    @OneToMany(mappedBy = "editor", cascade = [CascadeType.ALL], orphanRemoval = true, targetEntity = Series::class)
    open var series: Set<Series> = mutableSetOf()
)
