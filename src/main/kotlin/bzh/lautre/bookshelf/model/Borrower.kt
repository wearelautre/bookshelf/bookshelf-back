package bzh.lautre.bookshelf.model

import lombok.NoArgsConstructor
import jakarta.persistence.*
import lombok.Data

@Data
@NoArgsConstructor
@Entity
@Table(name = "borrower")
open class Borrower(
        @Column(unique = true) open var name: String? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) open var id: Long? = null,
        @OneToMany(mappedBy = "borrower", orphanRemoval = true, fetch = FetchType.LAZY, cascade = [CascadeType.ALL], targetEntity = Loan::class)
        open var loans: MutableList<Loan> = mutableListOf()
)
