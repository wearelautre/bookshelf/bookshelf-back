package bzh.lautre.bookshelf.model

import lombok.Data
import jakarta.persistence.*

@Data
@Entity
@Table(name = "account")
class Account(
    @Column(nullable = false, length = 100) var email: String? = null,
    @Column(nullable = false, length = 100) var login: String? = null,
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    var role: AccountRoleEnum = AccountRoleEnum.ROLE_USER,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null
) {
    data class Builder(
        var email: String? = null,
        var login: String? = null,
        var role: AccountRoleEnum = AccountRoleEnum.ROLE_USER
    ) {
        fun email(email: String) = apply { this.email = email }
        fun role(role: AccountRoleEnum) = apply { this.role = role }
        fun login(login: String) = apply { this.login = login }
        fun build() = Account(email, login, role)
    }
}
