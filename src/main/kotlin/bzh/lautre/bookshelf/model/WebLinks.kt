package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import jakarta.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "web_links")
open class WebLinks(
        @Column open var value: String? = null,
        @Enumerated(EnumType.STRING) @Column open var type: WebLinksTypeEnum = WebLinksTypeEnum.OTHER,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) open var id: Long? = null,
        @ManyToOne open var artists: Artist? = null
)
