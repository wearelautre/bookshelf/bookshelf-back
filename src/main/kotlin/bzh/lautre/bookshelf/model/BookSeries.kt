package bzh.lautre.bookshelf.model

import jakarta.persistence.*
import lombok.Data

@Data
@Entity
@Table(name = "book_series")
open class BookSeries(
    @Id
    @Column(name = "book_isbn")
    open var isbn: String? = null,
    @MapsId
    @OneToOne
    @JoinColumn(name = "book_isbn")
    open var book: Book? = null,
    @ManyToOne
    open var series: Series? = null,
    open var seriesTome: Int? = null,
    @JoinColumn(name = "series_cycle_id")
    @ManyToOne
    open var seriesCycle: SeriesCycle? = null,
    open var cycleTome: Int? = null
)