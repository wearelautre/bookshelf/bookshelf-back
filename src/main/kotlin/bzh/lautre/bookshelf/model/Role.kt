package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor

import jakarta.persistence.*

@Data
@NoArgsConstructor
@Entity(name = "Role")
@Table(name = "role")
open class Role(
        @Column(unique = true) open var name: String? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) open var id: Long? = null,
        @OneToMany(mappedBy = "role", cascade = [CascadeType.ALL], targetEntity = Contract::class)
        open var contracts: List<Contract> = listOf()
)
