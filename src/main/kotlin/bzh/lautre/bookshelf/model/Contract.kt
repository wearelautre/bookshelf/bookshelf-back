package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import java.io.Serializable
import jakarta.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "contract")
open class Contract(
    @ManyToOne @MapsId("roleId") open var role: Role = Role(),
    @ManyToMany(targetEntity = Artist::class)
    @JoinTable(name = "contract_artists")
    open var artists: List<Artist> = mutableListOf(),
    @ManyToOne @MapsId("bookIsbn") open var book: Book? = null,
    @EmbeddedId open var id: ContractId = ContractId(role.id, book?.isbn)
): Serializable
