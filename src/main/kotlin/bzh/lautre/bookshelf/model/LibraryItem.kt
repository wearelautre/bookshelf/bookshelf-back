package bzh.lautre.bookshelf.model

import jakarta.persistence.*
import lombok.Data
import java.time.LocalDateTime

@Data
@Entity
@Table(name = "library_item")
class LibraryItem(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @OneToMany(mappedBy = "libraryItem", cascade = [CascadeType.ALL], targetEntity = Loan::class)
    var loans: List<Loan> = mutableListOf(),
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    // TODO : rename column name to book_isbn
    @JoinColumn(name = "isbn", nullable = false)
    var book: Book? = null,
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "account_id", nullable = false)
    var account: Account? = null,
    @Enumerated(EnumType.STRING) @Column(length = 7, nullable = false)
    var status: BookReadStatusEnum = BookReadStatusEnum.UNREAD,
    @Enumerated(EnumType.STRING) @Column(length = 13, nullable = false)
    var possessionStatus: PossessionStatusEnum = PossessionStatusEnum.POSSESSED,
    var lastUpdateDate: LocalDateTime = LocalDateTime.now(),
    var additionDate: LocalDateTime = LocalDateTime.now()
) {
    @OneToOne(mappedBy = "libraryItem", cascade = [CascadeType.ALL])
    @PrimaryKeyJoinColumn
    var metadata: LibraryItemMetadata? = null
}
