package bzh.lautre.bookshelf.api.v1.model.manual

import bzh.lautre.bookshelf.api.v1.model.TaskTypeEnumDTO
import com.fasterxml.jackson.annotation.JsonProperty
import io.swagger.v3.oas.annotations.media.Schema
import jakarta.validation.Valid

/**
 * 
 * @param type 
 */
data class TaskCountDTO(

    @field:Valid
    @Schema(example = "null", description = "")
    @field:JsonProperty("type") var type: TaskTypeEnumDTO? = null
) : kotlin.collections.HashMap<String, java.math.BigDecimal>()

