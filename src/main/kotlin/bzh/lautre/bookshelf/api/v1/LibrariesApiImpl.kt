package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.LibraryItemMapper
import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.business.LibraryItemBusiness
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.exception.BadParameterException
import bzh.lautre.bookshelf.exception.ResourceNotFoundException
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.model.LibraryItem
import bzh.lautre.bookshelf.specification.LibraryItemSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime
import java.util.*
import java.util.stream.Collectors

@RestController
@Tag(name = "library")
class LibrariesApiImpl(
    private val libraryItemBusiness: LibraryItemBusiness,
    private val libraryItemMapper: LibraryItemMapper,
    private val bookTypeBusiness: BookTypeBusiness,
) : LibrariesApi {

    override fun searchBooksInAccountLibrary(
        accountId: Int,
        search: String,
        page: Int,
        size: Int,
        direction: String,
        sort: String
    ): ResponseEntity<List<MinimalLibraryItemDTO>> {
        val itemPage: Page<LibraryItem> =
            this.libraryItemBusiness.search(
                resolveSpecificationFromInfixExpr("account:$accountId|$search"),
                page,
                size,
                Sort.Direction.fromString(direction),
                BookSortField.valueOf(sort.uppercase())
            )

        return ResponseEntity(
            itemPage.stream().map(libraryItemMapper::mapToMinimal)
                .collect(Collectors.toCollection { LinkedList<MinimalLibraryItemDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/books"),
            HttpStatus.OK
        )
    }

    override fun getLastAddedItems(accountId: Int, page: Int, size: Int): ResponseEntity<PageOfMinimalLibraryItemDTO> {
        return ResponseEntity.ok(
            mapToMinimalLibraryItemDTO(
                this.libraryItemBusiness.getLastAdded(
                    resolveSpecificationFromInfixExpr("account:$accountId"), page, size
                )
            )
        )
    }

    override fun getNextItemsToRead(
        accountId: Int,
        page: Int,
        size: Int
    ): ResponseEntity<PageOfMinimalLibraryItemWithCoverDTO> {
        return ResponseEntity.ok(
            mapToMinimalLibraryItemWithCoverDTO(this.libraryItemBusiness.getNextBookToRead(accountId, page, size))
        )
    }

    override fun getProfileBookStatus(accountId: Int): ResponseEntity<StatInfoDTO> {
        return ResponseEntity.ok(
            StatInfoDTO(
                libraryItemBusiness.allReadItems(accountId).size.toLong(),
                libraryItemBusiness.allItems(accountId).size.toLong(),
            )
        )
    }

    override fun getProfileInformation(accountId: Int): ResponseEntity<LibraryDTO> {
        val items = libraryItemBusiness.allItems(accountId)
        val itemsCount = items.size.toLong()
        val pageBookTypes = bookTypeBusiness.getTop3BookTypesInBooksNumber(accountId)

        val library = LibraryDTO(
            lastAddedItems = mapToMinimalLibraryItemDTO(
                this.libraryItemBusiness.getLastAdded(
                    resolveSpecificationFromInfixExpr("account:$accountId"), 0, 5
                )
            ),
            nextItemsToRead = mapToMinimalLibraryItemWithCoverDTO(
                this.libraryItemBusiness.getNextBookToRead(
                    accountId,
                    0,
                    5
                )
            ),
            itemsStatInfo = StatInfoDTO(
                libraryItemBusiness.allReadItems(accountId).size.toLong(),
                itemsCount,
            ),
            libraryCount = LibraryCountDTO(
                itemsCount,
                pageBookTypes.totalElements,
                pageBookTypes.map {
                    KeyValueDTO(
                        it.name,
                        getCountForBookType(it, items).toBigDecimal()
                    )
                }.toMutableList()
            )
        )

        return ResponseEntity.ok(library)
    }

    override fun patchBookInLibrary(
        accountId: Int,
        id: Long,
        libraryItemDTO: LibraryItemDTO
    ): ResponseEntity<LibraryItemDTO> {

        if (id != libraryItemDTO.id) {
            throw BadParameterException("path id and body id mismatch")
        }

        val item: LibraryItem = this.libraryItemBusiness.findById(id)
            .orElseThrow { ResourceNotFoundException("LIBRARY ITEM", id.toString()) }
        item.lastUpdateDate = LocalDateTime.now()

        return ResponseEntity.ok(
            libraryItemMapper.mapToLibraryItem(
                this.libraryItemBusiness.save(
                    this.libraryItemMapper.mapInLibraryItem(
                        libraryItemDTO,
                        item
                    )
                )
            )
        )
    }

    private fun getCountForBookType(bookType: BookType, items: List<LibraryItem>): Long {
        return items.filter { it.book?.bookSeries?.series?.bookType?.id == bookType.id }.size.toLong()
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<LibraryItem> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<LibraryItem> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { LibraryItemSpecification(it!!) }
    }

    private fun mapToMinimalLibraryItemDTO(items: Page<LibraryItem>): PageOfMinimalLibraryItemDTO {
        return PageOfMinimalLibraryItemDTO(
            currentPage = items.pageable.pageNumber.toLong(),
            totalElements = items.totalElements,
            totalPages = items.totalPages.toLong(),
            list = items.stream().map(libraryItemMapper::mapToMinimal)
                .collect(Collectors.toCollection { LinkedList<MinimalLibraryItemDTO>() })
        )
    }

    private fun mapToMinimalLibraryItemWithCoverDTO(items: Page<LibraryItem>): PageOfMinimalLibraryItemWithCoverDTO {
        return PageOfMinimalLibraryItemWithCoverDTO(
            currentPage = items.pageable.pageNumber.toLong(),
            totalElements = items.totalElements,
            totalPages = items.totalPages.toLong(),
            list = items.stream().map(libraryItemMapper::mapToMinimalWithCover)
                .collect(Collectors.toCollection { LinkedList<MinimalLibraryItemWithCoverDTO>() })
        )
    }
}

