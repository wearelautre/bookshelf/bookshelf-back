package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.TaskDTO
import bzh.lautre.bookshelf.model.Task
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface TaskMapper {

    fun map(task: Task): TaskDTO

    fun toDto(taskList: List<Task>): List<TaskDTO>

    @InheritInverseConfiguration
    fun map(taskDTO: TaskDTO): Task

    @InheritInverseConfiguration
    fun toEntity(taskDTOList: List<TaskDTO>): List<Task>

}
