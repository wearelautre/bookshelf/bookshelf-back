package bzh.lautre.bookshelf.api.v1.util

class SearchCriteria(
    var key: String? = null,
    var operation: String? = null,
    var value: Any? = null
)
