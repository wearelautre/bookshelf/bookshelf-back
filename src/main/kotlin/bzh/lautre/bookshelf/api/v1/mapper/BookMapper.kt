package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.BookDTO
import bzh.lautre.bookshelf.api.v1.model.BookMetadataDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalBookDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalBookWithCoverDTO
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.business.EditorBusiness
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookMetadata
import bzh.lautre.bookshelf.model.BookReadStatusEnum
import org.mapstruct.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Mapper(
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ON_IMPLICIT_CONVERSION,
    componentModel = "spring",
    uses = [BookTypeBusiness::class, SeriesMapper::class, RoleMapper::class, ArtistMapper::class, EditorMapper::class],
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
@Component
abstract class BookMapper {

    @Autowired
    lateinit var editorBusiness: EditorBusiness

    @Mapping(target = "editor", source = "bookSeries.series.editor")
    @Mapping(target = "series", source = "bookSeries")
    @Mapping(target = "metadata", source = ".")
    abstract fun map(book: Book): BookDTO

    abstract fun map(books: List<Book>): List<BookDTO>

    @Mapping(target = "contracts", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "metadata", source = ".")
    @Mapping(target = "arkId", source = "metadata.arkId")
    abstract fun map(book: BookDTO): Book

    @Mapping(target = "arkId", source = "arkId")
    @Mapping(target = "pageCount", source = "metadata.pageCount")
    abstract fun mapMetadata(book: Book): BookMetadataDTO

    @Mapping(target = "pageCount", source = "metadata.pageCount")
    abstract fun mapMetadata(book: BookDTO): BookMetadata

    @AfterMapping
    fun afterMapping(bookDto: BookDTO, @MappingTarget book: Book) {
        book.metadata?.isbn = book.isbn
        book.bookSeries?.isbn = book.isbn
    }

    @Condition
    fun isNotEmpty(value: String?): Boolean {
        return !value.isNullOrEmpty()
    }

    @Condition
    fun isNotNull(value: Number?): Boolean {
        return value != null
    }

    @Condition
    fun isNotEmpty(value: BookReadStatusEnum?): Boolean {
        return value != null
    }

    @Mapping(target = "contracts", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "bookSeries", source = "series")
    abstract fun map(bookDTO: BookDTO, @MappingTarget book: Book): Book

    @AfterMapping
    fun addEditorFromBook(bookDTO: BookDTO, @MappingTarget book: Book) {
        book.bookSeries?.series?.editor =
            editorBusiness.findEditorByIdOrName(bookDTO.editor?.id, bookDTO.editor?.name!!)
    }

    @Mapping(target = "contracts", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "bookSeries", ignore = true)
    abstract fun mapGeneralData(bookDTO: MinimalBookDTO, @MappingTarget book: Book): Book

    @Mapping(target = "editor", source = "bookSeries.series.editor", qualifiedBy = [MinimalDTO::class])
    @Mapping(target = "series", source = "bookSeries")
    abstract fun mapToMinimal(book: Book): MinimalBookDTO


    abstract fun mapToMinimal(books: List<Book>): List<MinimalBookDTO>

    @Mapping(target = "editor", source = "bookSeries.series.editor", qualifiedBy = [MinimalDTO::class])
    @Mapping(target = "series", source = "bookSeries")
    abstract fun mapToMinimalWithCover(book: Book): MinimalBookWithCoverDTO

    abstract fun mapToMinimalWithCover(books: List<Book>): List<MinimalBookWithCoverDTO>
}
