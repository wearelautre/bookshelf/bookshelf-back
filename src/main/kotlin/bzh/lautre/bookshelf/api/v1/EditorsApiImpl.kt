package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.EditorMapper
import bzh.lautre.bookshelf.api.v1.model.EditorDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.EditorBusiness
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.specification.EditorSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors

@RestController
@Tag(name = "editor")
open class EditorsApiImpl(
    private val business: EditorBusiness,
    private val mapper: EditorMapper
) : EditorsApi {

    override fun searchEditors(
        search: String,
        page: Int,
        size: Int,
        direction: String,
        allResults: Boolean
    ): ResponseEntity<List<EditorDTO>> {
        return search(search, page, size, direction, allResults)
    }

    override fun searchEditorsAutocomplete(
        search: String
    ): ResponseEntity<List<EditorDTO>> {
        return this.search(search, 0, 5, "ASC", false)
    }

    private fun search(
        search: String,
        page: Int,
        size: Int,
        direction: String,
        allResults: Boolean
    ): ResponseEntity<List<EditorDTO>> {
        val itemPage: Page<Editor> =
            this.business.search(
                resolveSpecificationFromInfixExpr(search),
                page,
                size,
                Sort.Direction.fromString(direction),
                allResults
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList<EditorDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/editors"),
            HttpStatus.OK
        )
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Editor> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Editor> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { EditorSpecification(it!!) }
    }
}
