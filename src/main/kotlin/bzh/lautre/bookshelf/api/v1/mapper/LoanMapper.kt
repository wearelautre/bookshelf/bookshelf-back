package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.LoanDTO
import bzh.lautre.bookshelf.api.v1.model.LoanWithBookDetailsDTO
import bzh.lautre.bookshelf.model.Loan
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(
    componentModel = "spring",
    uses = [LibraryItemMapper::class]
)
interface LoanMapper {

    @Mapping(target = "libraryItem", source = "libraryItem.id")
    fun map(loan: Loan): LoanDTO

    @Mapping(target = "libraryItem", ignore = true)
    fun mapToEntity(loan: LoanDTO): Loan

    fun mapWithBookDetails(loan: Loan): LoanWithBookDetailsDTO
    fun mapWithBookDetails(loans: List<Loan>): MutableList<LoanWithBookDetailsDTO>
}
