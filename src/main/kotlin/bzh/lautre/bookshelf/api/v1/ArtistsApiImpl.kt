package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.ArtistMapper
import bzh.lautre.bookshelf.api.v1.mapper.BookMapper
import bzh.lautre.bookshelf.api.v1.model.ArtistDTO
import bzh.lautre.bookshelf.api.v1.model.BookWithRoleDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalArtistDTO
import bzh.lautre.bookshelf.api.v1.model.RoleDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.ArtistBusiness
import bzh.lautre.bookshelf.exception.ResourceNotFoundException
import bzh.lautre.bookshelf.model.Artist
import bzh.lautre.bookshelf.specification.ArtistSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.transaction.Transactional
import org.springframework.data.domain.Page
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors

@RestController
@Tag(name = "artist")
open class ArtistsApiImpl(
    private val business: ArtistBusiness,
    private val mapper: ArtistMapper,
    private val mapperBook: BookMapper
) : ArtistsApi {

    override fun searchArtists(
        search: String,
        page: Int,
        size: Int
    ): ResponseEntity<List<ArtistDTO>> {
        val itemPage: Page<Artist> =
            this.business.search(resolveSpecificationFromInfixExpr(search), page, size)

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/authors"),
            HttpStatus.OK
        )
    }

    override fun searchArtistsAutocomplete(
        name: String,
        withRolesFirst: String?,
        withSeriesFirst: String?
    ): ResponseEntity<List<MinimalArtistDTO>> {
        val itemPage: Page<Artist> =
            this.business.searchWithMultipleOrder(
                name,
                0,
                5,
                withRolesFirst,
                withSeriesFirst
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::mapToMinimal).collect(Collectors.toCollection { LinkedList() }),
            HttpStatus.OK
        )
    }

    override fun getArtistById(id: Long): ResponseEntity<ArtistDTO> {
        return ok(this.business.findById(id).map { mapper.map(it) }
            .orElseThrow { throw ResourceNotFoundException("ARTIST", id.toString()) })
    }

    override fun updateArtist(id: Long, artistDTO: ArtistDTO): ResponseEntity<ArtistDTO> {
        return if (id == artistDTO.id!!.toLong()) {
            val currentArtist = this.business.findById(id)
                .orElseThrow { throw ResourceNotFoundException("ARTIST", id.toString()) }

            val artists = this.mapper.map(artistDTO)
            artists.contracts = currentArtist.contracts
            this.business.save(artists)
            val carById = this.business.findById(id).map { mapper.map(it) }
                .orElseThrow { throw ResourceNotFoundException("ARTIST", id.toString()) }
            ok(carById)
        } else {
            ResponseEntity.status(400).build()
        }
    }

    override fun deleteArtist(id: Long): ResponseEntity<Unit> {
        val artist = this.business.findById(id)
            .orElseThrow { throw ResourceNotFoundException("ARTIST", id.toString()) }

        this.business.delete(artist)
        return if (this.business.findById(id).isPresent)
            ResponseEntity.status(500).build()
        else
            ResponseEntity.noContent().build()
    }

    override fun getArtistContracts(id: Long): ResponseEntity<List<BookWithRoleDTO>> {
        return ok(this.business.getBooksByRole(id).map {
            BookWithRoleDTO(
                RoleDTO(it.role.id, it.role.name),
                it.books.map { book -> mapperBook.mapToMinimal(book) }.toMutableList()
            )
        })
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Artist> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Artist> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { ArtistSpecification(it!!) }
    }
}

