package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.WebLinksDTO
import bzh.lautre.bookshelf.model.WebLinks
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface WebLinksMapper {

    fun map(webLinks: WebLinks): WebLinksDTO

    @InheritInverseConfiguration
    @Mapping(target = "artists", ignore = true)
    fun map(webLinks: WebLinksDTO): WebLinks

}
