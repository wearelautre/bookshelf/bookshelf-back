package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.TaskMapper
import bzh.lautre.bookshelf.api.v1.model.PageOfTaskDTO
import bzh.lautre.bookshelf.api.v1.model.TaskDTO
import bzh.lautre.bookshelf.api.v1.model.TaskStatusEnumDTO
import bzh.lautre.bookshelf.api.v1.model.TaskTypeEnumDTO
import bzh.lautre.bookshelf.api.v1.model.manual.TaskCountDTO
import bzh.lautre.bookshelf.business.TaskBusiness
import bzh.lautre.bookshelf.exception.ResourceNotFoundException
import bzh.lautre.bookshelf.model.TaskStatusEnum
import bzh.lautre.bookshelf.model.TaskTypeEnum
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
@Tag(name = "task")
open class TasksApiImpl(
    private val business: TaskBusiness,
    private val mapper: TaskMapper
) : TasksApi {

    override fun addTask(taskDTO: TaskDTO): ResponseEntity<TaskDTO> {
        return ok(mapper.map(business.save(mapper.map(taskDTO))))
    }

    override fun getTasksCountPerTypeForStatus(status: TaskStatusEnumDTO): ResponseEntity<TaskCountDTO> {
        val taskCountDTO = TaskCountDTO()
        val todoList = this.business.getAllTasksPerStatus(TaskStatusEnum.valueOf(status.name))
        taskCountDTO["ALL"] = BigDecimal(todoList.size)
        todoList.forEach {
            if (taskCountDTO.keys.contains(it.type.name)) {
                val value = taskCountDTO[it.type.name] as BigDecimal
                taskCountDTO[it.type.name] = value.plus(BigDecimal.ONE)
            } else {
                taskCountDTO[it.type.name] = BigDecimal.ONE
            }
        }
        return ok(taskCountDTO)
    }

    override fun deleteTask(id: Long): ResponseEntity<Unit> {
        val tasks = this.business.findById(id)
            .orElseThrow { throw ResourceNotFoundException("TASK", id.toString()) }

        this.business.delete(tasks)
        return if (this.business.findById(id).isPresent)
            ResponseEntity.status(500).build()
        else
            ResponseEntity.noContent().build()
    }

    override fun getTasks(
        type: TaskTypeEnumDTO,
        status: TaskStatusEnumDTO,
        page: Int,
        size: Int
    ): ResponseEntity<PageOfTaskDTO> {
        val pageOfTask = business.getPageOfLastTaskPerStatusAndType(
            TaskTypeEnum.valueOf(type.name),
            TaskStatusEnum.valueOf(status.name),
            page,
            size
        )

        return ok(
            PageOfTaskDTO(
                currentPage = pageOfTask.pageable.pageNumber.toLong(),
                totalElements = pageOfTask.totalElements,
                totalPages = pageOfTask.totalPages.toLong(),
                list = mapper.toDto(pageOfTask.content).toMutableList()
            )
        )
    }
}

