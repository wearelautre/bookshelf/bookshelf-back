package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.EditorDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalEditorDTO
import bzh.lautre.bookshelf.model.Editor
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface EditorMapper {

    fun map(editor: Editor): EditorDTO

    @MinimalDTO
    fun mapToMinimal(editor: Editor): MinimalEditorDTO

    @InheritInverseConfiguration(name = "map")
    fun map(editor: EditorDTO): Editor

}
