package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.SeriesCycleDTO
import bzh.lautre.bookshelf.model.SeriesCycle
import org.mapstruct.Mapper
import org.springframework.stereotype.Component

@Mapper(
    componentModel = "spring"
)
@Component
abstract class SeriesCycleMapper {

    abstract fun map(seriesCycle: SeriesCycle): SeriesCycleDTO

}
