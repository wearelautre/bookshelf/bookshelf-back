package bzh.lautre.bookshelf.api.v1.util

import bzh.lautre.bookshelf.api.v1.util.SearchOperation.Companion.SIMPLE_OPERATION_SET
import com.google.common.base.Joiner
import lombok.extern.slf4j.Slf4j
import java.util.regex.Pattern

@Slf4j
class Utils {
    companion object {
        fun sanitizeIsbn(isbn: String): String {
            return isbn
                .replace("-", "")
                .replace(" ", "")
                .trim { it <= ' ' }
        }
    }

}
