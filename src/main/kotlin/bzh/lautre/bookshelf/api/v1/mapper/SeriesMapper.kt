package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.business.EditorBusiness
import bzh.lautre.bookshelf.business.SeriesBusiness
import bzh.lautre.bookshelf.exception.BadParameterException
import bzh.lautre.bookshelf.model.*
import jakarta.transaction.Transactional
import org.mapstruct.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Mapper(
    nullValueCheckStrategy = NullValueCheckStrategy.ON_IMPLICIT_CONVERSION,
    nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL,
    componentModel = "spring",
    uses = [SeriesBusiness::class],
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
@Component
abstract class SeriesMapper {

    @Autowired
    lateinit var seriesBusiness: SeriesBusiness

    @Autowired
    lateinit var editorBusiness: EditorBusiness

    @Autowired
    lateinit var bookTypeBusiness: BookTypeBusiness

    @Transactional
    @Mapping(source = "editor.name", target = "editor")
    @Mapping(source = ".", target = "status")
    abstract fun mapToSeriesBook(series: Series): SeriesDTO

    //    @Mapping(source = "bookList", target = "possessedCount", qualifiedByName = ["getSeriesBookCount"])
    //    @Mapping(source = "bookList", target = "readCount", qualifiedByName = ["getSeriesReadBookCount"])
    @Mapping(source = "publishedCount", target = "totalCount")
    @Mapping(source = "publishedStatus", target = "status")
    abstract fun mapToSeriesStatus(series: Series): SeriesStatusDTO

    @Mapping(source = "seriesTome", target = "tome")
    @Mapping(source = "series", target = "status")
    @Mapping(source = "series", target = ".")
    @Mapping(source = ".", target = "seriesCycle", qualifiedByName = ["setSeriesCycle"])
    abstract fun mapToMinimal(series: BookSeries): BookSeriesDTO

    @Condition
    fun isNotNull(value: BookSeriesCycleDTO?): Boolean {
        return value != null
    }

    @Named("setSeriesCycle")
    fun setSeriesCycle(series: BookSeries): BookSeriesCycleDTO? {
        var seriesCycle: BookSeriesCycleDTO? = null
        if (series.seriesCycle != null) {
            seriesCycle = BookSeriesCycleDTO()
            seriesCycle.id = series.seriesCycle!!.id
            seriesCycle.name = series.seriesCycle!!.name
            seriesCycle.tome = series.cycleTome!!.toLong()
        }
        return seriesCycle
    }

    @Mapping(target = "seriesTome", source = "tome")
    @Mapping(target = "cycleTome", source = "seriesCycle.tome")
    abstract fun mapToSeriesBook(bookSeries: BookSeriesDTO): BookSeries

    abstract fun mapToSeriesCycle(seriesCycle: SeriesCycleDTO): SeriesCycle

    abstract fun mapToSeriesCycle(seriesCycle: BookSeriesCycleDTO): SeriesCycle

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "name", ignore = true)
    @Mapping(target = "editor", source = ".", qualifiedByName = ["getEditor"])
    abstract fun addEditor(editor: EditorDTO, @MappingTarget series: Series)

    @Mapping(target = "bookList", source = "id", qualifiedByName = ["getBookList"])
    @Mapping(target = "editor", source = "editor", qualifiedByName = ["getEditor"])
    @Mapping(target = "cycles", source = "cycles", defaultExpression = "java(emptyList())")
    @Mapping(target = "publishedStatus", source = "status.status")
    @Mapping(target = "publishedCount", source = "status.totalCount")
    abstract fun mapToSeriesBook(series: SeriesDTO): Series

    @AfterMapping
    fun afterMapping(seriesDTO: SeriesDTO, @MappingTarget series: Series) {
        series.cycles.forEach { it.series = series }
    }

    @Named("getBookList")
    fun getBookList(id: Long?): List<BookSeries> {
        return if (id != null) {
            this.seriesBusiness.findById(id).orElse(Series()).bookList
        } else {
            emptyList()
        }
    }

//    @Named("getSeriesBookCount")
//    fun getSeriesBookCount(bookList: List<BookSeries>, accountId: Long): Int {
//        return bookList.filter { it.book?.libraries?.find { it.id == accountId }?.possessionStatus === PossessionStatusEnum.POSSESSED }.size
//    }
//
//    @Named("getSeriesReadBookCount")
//    fun getSeriesReadBookCount(bookList: List<BookSeries>, accountId: Long): Int {
//        return bookList.filter { (it.book is Book) }.filter { (it.book as Book).status === BookReadStatusEnum.READ }.size
//    }

    @Named("getEditor")
    fun getEditor(editor: String): Editor {
        return this.editorBusiness.getByName(editor)
    }

    @Named("getEditor")
    fun getEditor(editor: EditorDTO): Editor {
        return this.editorBusiness.findEditorByIdOrName(editor.id, editor.name)
    }

    @Named("getSeries")
    fun map(bookSeries: BookSeriesDTO): Series {
        return initSeries(bookSeries, bookSeries.id != null)
    }

    private fun initSeries(bookSeries: BookSeriesDTO, withId: Boolean): Series {
        val series = if (withId) {
            seriesBusiness.findById(bookSeries.id!!).orElseGet { Series() }
        } else {
            Series()
        }
        if (bookSeries.name.isNotEmpty()) {
            series.name = bookSeries.name
        }

        series.oneShot = if (bookSeries.oneShot == null) false else bookSeries.oneShot!!
        if (bookSeries.bookType != null) {
            series.bookType = initBookType(bookSeries.bookType!!)
        } else {
            throw BadParameterException("the BookType is mandatory")
        }
        return series
    }

    private fun initBookType(bookTypeDTO: MinimalBookTypeDTO): BookType {
        val bookType = if (bookTypeDTO.id != null) {
            this.bookTypeBusiness.findById(bookTypeDTO.id!!).orElseGet { BookType() }
        } else {
            BookType()
        }
        if (bookTypeDTO.name != null) {
            bookType.name = bookTypeDTO.name!!
        }

        return bookType
    }

}
