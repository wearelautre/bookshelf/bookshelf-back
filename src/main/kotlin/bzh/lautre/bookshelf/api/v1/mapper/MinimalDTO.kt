package bzh.lautre.bookshelf.api.v1.mapper

import org.mapstruct.Qualifier

@Qualifier
@Target(AnnotationTarget.FUNCTION)
annotation class MinimalDTO