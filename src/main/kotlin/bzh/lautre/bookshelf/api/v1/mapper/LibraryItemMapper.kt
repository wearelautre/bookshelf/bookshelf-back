package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.LibraryItemDTO
import bzh.lautre.bookshelf.api.v1.model.LibraryItemMetadataDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalLibraryItemDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalLibraryItemWithCoverDTO
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.model.LibraryItem
import bzh.lautre.bookshelf.model.LibraryItemMetadata
import org.mapstruct.*
import org.springframework.stereotype.Component


@Mapper(
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ON_IMPLICIT_CONVERSION,
    componentModel = "spring",
    uses = [BookTypeBusiness::class, SeriesMapper::class, RoleMapper::class, ArtistMapper::class, EditorMapper::class, BookMapper::class],
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
@Component
interface LibraryItemMapper {

    @Mappings(
        Mapping(
            target = "lent",
            expression = "java(libraryItem.getLoans().stream().anyMatch(loan -> loan.getReturnDate() == null))"
        ),
    )
    fun mapToMinimal(libraryItem: LibraryItem): MinimalLibraryItemDTO

    fun mapToMinimalWithCover(libraryItem: LibraryItem): MinimalLibraryItemWithCoverDTO

    fun mapToLibraryItem(libraryItem: LibraryItem): LibraryItemDTO

    fun mapInLibraryItem(libraryItemDTO: LibraryItemDTO, @MappingTarget libraryItem: LibraryItem): LibraryItem
    fun mapInLibraryItemMetadata(
        libraryItemMetadataDTO: LibraryItemMetadataDTO,
        @MappingTarget libraryItemMetadata: LibraryItemMetadata
    ): LibraryItemMetadata
}
