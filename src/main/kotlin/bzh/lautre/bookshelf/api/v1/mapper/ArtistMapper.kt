package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.ArtistDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalArtistDTO
import bzh.lautre.bookshelf.model.Artist
import bzh.lautre.bookshelf.model.Contract
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Named

@Mapper(
    componentModel = "spring",
    uses = [WebLinksMapper::class]
)
abstract class ArtistMapper {

    @Mapping(target = "roles", source = "contracts", qualifiedByName = ["getRolesFromContracts"])
    abstract fun map(artist: Artist): ArtistDTO

    abstract fun mapToMinimal(artist: Artist): MinimalArtistDTO

    @InheritInverseConfiguration
    @Mapping(target = "contracts", ignore = true)
    abstract fun map(artist: ArtistDTO): Artist

    abstract fun map(artist: MinimalArtistDTO): Artist

    @Named("getRolesFromContracts")
    fun getRolesFromContracts(contracts: List<Contract>): MutableList<String> {
        return contracts.map { it.role.name!! }.distinct().toMutableList()
    }
}
