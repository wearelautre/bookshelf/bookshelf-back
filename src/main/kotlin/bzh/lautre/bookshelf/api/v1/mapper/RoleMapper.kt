package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.RoleDTO
import bzh.lautre.bookshelf.model.Role
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface RoleMapper {

    fun map(role: Role): RoleDTO

    @InheritInverseConfiguration
    @Mapping(target = "contracts", ignore = true)
    fun map(role: RoleDTO): Role

}
