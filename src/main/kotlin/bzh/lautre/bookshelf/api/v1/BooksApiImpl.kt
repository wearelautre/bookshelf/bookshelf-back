package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.ArtistMapper
import bzh.lautre.bookshelf.api.v1.mapper.BookMapper
import bzh.lautre.bookshelf.api.v1.mapper.RoleMapper
import bzh.lautre.bookshelf.api.v1.mapper.SeriesMapper
import bzh.lautre.bookshelf.api.v1.model.BookDTO
import bzh.lautre.bookshelf.api.v1.model.ContractDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalBookDTO
import bzh.lautre.bookshelf.api.v1.model.PutBooksSearchCover200ResponseDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.api.v1.util.Utils.Companion.sanitizeIsbn
import bzh.lautre.bookshelf.business.BookBusiness
import bzh.lautre.bookshelf.business.BookMacroBusiness
import bzh.lautre.bookshelf.business.CoverBusiness
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.exception.BookAlreadyExistException
import bzh.lautre.bookshelf.exception.CoverSaveException
import bzh.lautre.bookshelf.exception.ResourceNotFoundException
import bzh.lautre.bookshelf.model.*
import bzh.lautre.bookshelf.specification.BookSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.validation.Valid
import org.springframework.core.io.Resource
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.time.LocalDateTime.now
import java.util.*
import java.util.stream.Collectors

@RestController
@Tag(name = "book")
class BooksApiImpl(
    private val bookBusiness: BookBusiness,
    private val bookMacroBusiness: BookMacroBusiness,
    private val coverBusiness: CoverBusiness,
    private val bookMapper: BookMapper,
    private val artistMapper: ArtistMapper,
    private val roleMapper: RoleMapper,
    private val seriesMapper: SeriesMapper
) : BooksApi {
    override fun searchBooks(
        search: String,
        page: Int,
        size: Int,
        direction: String,
        sort: String
    ): ResponseEntity<List<MinimalBookDTO>> {
        val itemPage: Page<Book> =
            this.bookBusiness.search(
                resolveSpecificationFromInfixExpr(search),
                page,
                size,
                Sort.Direction.fromString(direction),
                BookSortField.valueOf(sort.uppercase())
            )

        return ResponseEntity(
            itemPage.stream().map(bookMapper::mapToMinimal)
                .collect(Collectors.toCollection { LinkedList<MinimalBookDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/books"),
            HttpStatus.OK
        )
    }

    override fun saveBook(@Valid bookDTO: BookDTO): ResponseEntity<BookDTO> {
        // Control if creation is possible
        if (bookDTO.series == null || bookDTO.editor == null) {
            // TODO create real error message
            return ResponseEntity.badRequest().build()
        }

        bookDTO.isbn = sanitizeIsbn(bookDTO.isbn!!)

        val optionalBook = this.bookBusiness.findByIsbn(bookDTO.isbn!!)
        if (optionalBook.isPresent) {
            throw BookAlreadyExistException(optionalBook.get().isbn!!)
        }

        val book = this.bookMapper.map(bookDTO)
        book.creationDate = now()
        book.lastUpdateDate = now()

        val savedBook = bookMacroBusiness.macroSaveBook(
            getSeries(bookDTO),
            seriesMapper.mapToSeriesBook(bookDTO.series!!),
            getSeriesCycle(bookDTO),
            book,
            bookDTO.contracts?.map { getContract(it) }!!
        )

        return ResponseEntity
            .created(getLocationURI(savedBook))
            .body(bookMapper.map(savedBook))

    }

    fun getSeries(minimalBookDTO: BookDTO): Series {
        val series = seriesMapper.map(minimalBookDTO.series!!)
        seriesMapper.addEditor(minimalBookDTO.editor!!, series)
        return series
    }

    fun getSeriesCycle(minimalBookDTO: MinimalBookDTO): SeriesCycle? {
        return if (minimalBookDTO.series!!.seriesCycle != null) {
            seriesMapper.mapToSeriesCycle(minimalBookDTO.series?.seriesCycle!!)
        } else {
            null
        }
    }

    fun getSeriesCycle(bookDTO: BookDTO): SeriesCycle? {
        return if (bookDTO.series!!.seriesCycle != null) {
            seriesMapper.mapToSeriesCycle(bookDTO.series?.seriesCycle!!)
        } else {
            null
        }
    }

    private fun getContract(contractDto: ContractDTO): Contract {
        return Contract(
            roleMapper.map(contractDto.role!!),
            contractDto.artists!!.map { artistMapper.map(it) }
        )
    }

    private fun getLocationURI(book: Book): URI {
        return ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(book.isbn)
            .toUri()
    }

    override fun deleteBook(isbn: String): ResponseEntity<Unit> {
        val book = this.bookBusiness.findById(isbn)
            .orElseThrow { ResourceNotFoundException("BOOK", isbn) }
        this.bookBusiness.delete(book)
        return if (this.bookBusiness.findById(book.isbn!!).isPresent) {
            throw InternalError("The resource book $isbn hasn't been removed")
        } else {
            ResponseEntity.noContent().build()
        }
    }

    override fun updateBook(isbn: String, bookDTO: BookDTO): ResponseEntity<BookDTO> {
        if (bookDTO.isbn === null) {
            return ResponseEntity.badRequest().build()
        }

        bookDTO.isbn = sanitizeIsbn(bookDTO.isbn!!)

        if (isbn != bookDTO.isbn) {
            // TODO create real error message
            return ResponseEntity.badRequest().build()
        }

        val sanitizedIsbn = sanitizeIsbn(isbn)
        this.bookBusiness.findById(sanitizedIsbn)
            .orElseThrow { ResourceNotFoundException("BOOK", isbn) }

        val book = this.bookMapper.map(bookDTO)
        book.lastUpdateDate = now()

        val savedBook = bookMacroBusiness.macroSaveBook(
            getSeries(bookDTO),
            seriesMapper.mapToSeriesBook(bookDTO.series!!),
            getSeriesCycle(bookDTO),
            book,
            bookDTO.contracts?.map { getContract(it) }!!
        )

        return ResponseEntity.ok().body(this.bookMapper.map(savedBook))
    }

    override fun patchBook(isbn: String, minimalBookDTO: MinimalBookDTO): ResponseEntity<BookDTO> {
        if (minimalBookDTO.isbn != null) {
            minimalBookDTO.isbn = sanitizeIsbn(minimalBookDTO.isbn!!)
        }

        if (minimalBookDTO.isbn != null && isbn != minimalBookDTO.isbn) {
            // TODO create real error message
            return ResponseEntity.badRequest().build()
        }

        val sanitizedIsbn = sanitizeIsbn(isbn)
        val book: Book = this.bookBusiness.findById(sanitizedIsbn)
            .orElseThrow { ResourceNotFoundException("BOOK", isbn) }
        book.lastUpdateDate = now()

        var series: Series? = null
        var seriesBook: BookSeries? = null
        var seriesCycle: SeriesCycle? = null
        if (minimalBookDTO.series != null) {
            series = seriesMapper.map(minimalBookDTO.series!!)
            seriesBook = seriesMapper.mapToSeriesBook(minimalBookDTO.series!!)
            seriesCycle = getSeriesCycle(minimalBookDTO)
        }

        val savedBook = bookMacroBusiness.macroSaveBook(
            series,
            seriesBook,
            seriesCycle,
            this.bookMapper.mapGeneralData(minimalBookDTO, book),
            listOf()
        )

        return ResponseEntity.ok().body(this.bookMapper.map(savedBook))
    }

    override fun getBookByIsbn(isbn: String): ResponseEntity<BookDTO> {
        return ResponseEntity.ok(this.bookBusiness.findById(isbn).map { bookMapper.map(it) }
            .orElseThrow { ResourceNotFoundException("BOOK", isbn) })
    }

    override fun putBooksSearchCover(isbn: String, file: Resource?): ResponseEntity<PutBooksSearchCover200ResponseDTO> {
        return if (this.coverBusiness.uploadCover(file!!, isbn)) {
            ResponseEntity.ok(PutBooksSearchCover200ResponseDTO("$isbn.jpg"))
        } else {
            throw CoverSaveException("Error during the save of $isbn cover")
        }
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Book> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Book> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { BookSpecification(it!!) }
    }
}

