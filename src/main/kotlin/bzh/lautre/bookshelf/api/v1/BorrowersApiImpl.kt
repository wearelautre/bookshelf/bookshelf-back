package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.BorrowerMapper
import bzh.lautre.bookshelf.api.v1.mapper.LoanMapper
import bzh.lautre.bookshelf.api.v1.model.BorrowerDTO
import bzh.lautre.bookshelf.api.v1.model.LoanDTO
import bzh.lautre.bookshelf.api.v1.model.LoanWithBookDetailsDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.BorrowerBusiness
import bzh.lautre.bookshelf.business.LibraryItemBusiness
import bzh.lautre.bookshelf.business.LoanBusiness
import bzh.lautre.bookshelf.exception.BookAlreadyBorrowedException
import bzh.lautre.bookshelf.exception.NotPossessedBookException
import bzh.lautre.bookshelf.exception.ResourceNotFoundException
import bzh.lautre.bookshelf.model.Borrower
import bzh.lautre.bookshelf.model.LibraryItem
import bzh.lautre.bookshelf.model.Loan
import bzh.lautre.bookshelf.model.PossessionStatusEnum
import bzh.lautre.bookshelf.specification.BorrowerSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.created
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.*
import java.util.stream.Collectors

@RestController
@Tag(name = "borrower")
class BorrowersApiImpl(
    private val business: BorrowerBusiness,
    private val loanBusiness: LoanBusiness,
    private val libraryItemBusiness: LibraryItemBusiness,
    private val mapper: BorrowerMapper,
    private val loanMapper: LoanMapper
) : BorrowersApi {

    override fun searchBorrowers(
        search: String,
        page: Int,
        size: Int,
        direction: String
    ): ResponseEntity<List<BorrowerDTO>> {
        return this.search(search, page, size, direction)
    }

    override fun searchBorrowersAutocomplete(search: String): ResponseEntity<List<BorrowerDTO>> {
        return this.search(search, 0, 5, "ASC")
    }

    private fun search(
        search: String,
        page: Int,
        size: Int,
        direction: String
    ): ResponseEntity<List<BorrowerDTO>> {
        val itemPage: Page<Borrower> =
            this.business.search(
                resolveSpecificationFromInfixExpr(search),
                page,
                size,
                Sort.Direction.fromString(direction)
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/borrowers"),
            HttpStatus.OK
        )
    }

    override fun createBorrower(borrowerDTO: BorrowerDTO): ResponseEntity<BorrowerDTO> {
        val createdBorrower = this.business.save(this.mapper.map(borrowerDTO))

        return created(
            ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdBorrower.id)
                .toUri()
        )
            .body(this.mapper.map(createdBorrower))
    }

    override fun getBorrowerById(id: Long): ResponseEntity<BorrowerDTO> {
        return this.business.findById(id)
            .map { ok(mapper.map(it)) }
            .orElseThrow { throw ResourceNotFoundException("BORROWER", id.toString()) }
    }

    override fun updateBorrower(id: Long, borrowerDTO: BorrowerDTO): ResponseEntity<BorrowerDTO> {
        return if (id == borrowerDTO.id!!.toLong()) {
            checkBorrowerPresence(id)

            ok(mapper.map((this.business.save(this.mapper.map(borrowerDTO)))))
        } else {
            ResponseEntity.status(400).build()
        }
    }

    override fun deleteBorrower(id: Long): ResponseEntity<Unit> {
        this.business.delete(checkBorrowerPresence(id))
        return if (this.business.findById(id).isPresent)
            ResponseEntity.status(500).build()
        else
            ResponseEntity.noContent().build()
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Borrower> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Borrower> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { BorrowerSpecification(it!!) }
    }

    override fun createLoan(id: Long, loanDTO: LoanDTO): ResponseEntity<LoanDTO> {
        val borrower = checkBorrowerPresence(id)

        val libraryItem = checkBookPresence(loanDTO.libraryItem.toLong())
        isBookCurrentlyBorrowed(libraryItem)

        val loan = loanMapper.mapToEntity(loanDTO)
        loan.id = null
        loan.libraryItem = libraryItem
        loan.borrower = borrower
        val createdLoan = loanBusiness.save(loan)

        return created(
            ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdLoan.id)
                .toUri()
        )
            .body(this.loanMapper.map(createdLoan))
    }

    override fun updateLoan(id: Long, subId: Long, loanDTO: LoanDTO): ResponseEntity<LoanWithBookDetailsDTO> {
        val borrower = checkBorrowerPresence(id)

        return if (subId == loanDTO.id!!.toLong()) {
            checkLoanPresence(subId)

            val libraryItem = checkBookPresence(loanDTO.libraryItem.toLong())

            val loan = this.loanMapper.mapToEntity(loanDTO)
            loan.libraryItem = libraryItem
            loan.borrower = borrower
            return ok(this.loanMapper.mapWithBookDetails(this.loanBusiness.save(loan)))
        } else {
            ResponseEntity.status(400).build()
        }
    }

    override fun getBorrowersLoansById(id: Long): ResponseEntity<List<LoanWithBookDetailsDTO>> {
        val borrower = checkBorrowerPresence(id)

        return ok(loanMapper.mapWithBookDetails(borrower.loans))
    }

    override fun deleteLoan(id: Long, subId: Long): ResponseEntity<Unit> {
        checkBorrowerPresence(id)
        val loan = checkLoanPresence(subId)

        this.loanBusiness.delete(loan)
        return if (this.loanBusiness.findById(subId).isPresent)
            ResponseEntity.status(500).build()
        else
            ResponseEntity.noContent().build()
    }

    private fun checkBorrowerPresence(id: Long): Borrower {
        return this.business.findById(id)
            .orElseThrow { throw ResourceNotFoundException("BORROWER", id.toString()) }
    }

    private fun checkLoanPresence(id: Long): Loan {
        return this.loanBusiness.findById(id)
            .orElseThrow { throw ResourceNotFoundException("LOAN", id.toString()) }
    }

    private fun checkBookPresence(libraryItemId: Long): LibraryItem {
        val libraryItem = this.libraryItemBusiness.findById(libraryItemId)
            .orElseThrow { throw ResourceNotFoundException("LIBRARY ITEM", "$libraryItemId") }

        if (libraryItem.possessionStatus == PossessionStatusEnum.NOT_POSSESSED) {
            throw NotPossessedBookException(libraryItem.book?.isbn!!)
        }

        return libraryItem
    }

    private fun isBookCurrentlyBorrowed(libraryItem: LibraryItem) {
        if (libraryItem.loans.any { it.returnDate == null }) {
            throw BookAlreadyBorrowedException(libraryItem.book?.isbn!!)
        }
    }
}

