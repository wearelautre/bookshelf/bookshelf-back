package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.BookTypeMapper
import bzh.lautre.bookshelf.api.v1.model.BookTypeDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalBookTypeDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.exception.ResourceNotFoundException
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.specification.BookTypeSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.*
import java.util.stream.Collectors
import jakarta.validation.Valid

@RestController
@Tag(name = "bookType")
open class BookTypesApiImpl(
    private val business: BookTypeBusiness,
    private val mapper: BookTypeMapper
) : BookTypesApi {

    override fun searchBookTypes(search: String, page: Int, size: Int): ResponseEntity<List<BookTypeDTO>> {
        val itemPage: Page<BookType> = this.business.search(resolveSpecificationFromInfixExpr(search)!!, page, size)

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList<BookTypeDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/bookTypes"),
            HttpStatus.OK
        )
    }

    override fun searchBookTypesAutocomplete(
        search: String
    ): ResponseEntity<List<MinimalBookTypeDTO>> {
        val itemPage: Page<BookType> = this.business.search(
            resolveSpecificationFromInfixExpr(search)!!,
            0,
            5,
            Sort.Direction.DESC
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::mapToMinimal).collect(Collectors.toCollection { LinkedList<MinimalBookTypeDTO>() }),
            HttpStatus.OK
        )
    }

    override fun getBookTypeById(id: Long): ResponseEntity<BookTypeDTO> {
        return ResponseEntity.ok(this.business.findById(id).map { mapper.map(it) }
            .orElseThrow { throw ResourceNotFoundException("BOOK_TYPE", id.toString()) })
    }

    override fun saveBookType(@Valid bookTypeDTO: BookTypeDTO): ResponseEntity<BookTypeDTO> {
        val createdBookType = this.business.save(this.mapper.map(bookTypeDTO))

        return ResponseEntity
                .created(ServletUriComponentsBuilder
                        .fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(createdBookType.id)
                        .toUri())
                .body(this.mapper.map(createdBookType))
    }

    override fun deleteBookType(id: Long): ResponseEntity<Unit> {
        val bookType = this.business.findById(id)
            .orElseThrow { throw ResourceNotFoundException("BOOK_TYPE", id.toString()) }

        this.business.delete(bookType)
        return if (this.business.findById(id).isPresent)
            ResponseEntity.status(500).build()
        else
            ResponseEntity.noContent().build()
    }

    override fun updateBookType(id: Long, bookTypeDTO: BookTypeDTO): ResponseEntity<BookTypeDTO> {
        return if (id == bookTypeDTO.id!!.toLong()) {
            this.business.findById(id)
                .orElseThrow { throw ResourceNotFoundException("BOOK_TYPE", id.toString()) }

            ResponseEntity.ok().body(this.mapper.map(this.business.save(this.mapper.map(bookTypeDTO))))
        } else {
            ResponseEntity.status(400).build()
        }
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<BookType>? {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<BookType> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { BookTypeSpecification(it!!) }
    }
}

