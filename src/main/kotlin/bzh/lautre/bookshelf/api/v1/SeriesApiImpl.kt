package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.SeriesCycleMapper
import bzh.lautre.bookshelf.api.v1.mapper.SeriesMapper
import bzh.lautre.bookshelf.api.v1.model.SeriesCycleDTO
import bzh.lautre.bookshelf.api.v1.model.SeriesDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.SeriesBusiness
import bzh.lautre.bookshelf.business.SeriesCycleBusiness
import bzh.lautre.bookshelf.exception.ResourceNotFoundException
import bzh.lautre.bookshelf.model.Series
import bzh.lautre.bookshelf.model.SeriesCycle
import bzh.lautre.bookshelf.specification.SeriesCycleSpecification
import bzh.lautre.bookshelf.specification.SeriesSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors

@RestController
@Tag(name = "series")
open class SeriesApiImpl(
    private val seriesCycleBusiness: SeriesCycleBusiness,
    private val seriesCycleMapper: SeriesCycleMapper,
    private val business: SeriesBusiness,
    private val mapper: SeriesMapper
) : SeriesApi {

    override fun searchSeries(
        search: String,
        page: Int,
        size: Int,
        direction: String,
        allResults: Boolean
    ): ResponseEntity<List<SeriesDTO>> {
       return this.search(search, page, size, direction, allResults)
    }

    override fun searchSeriesAutocomplete(
        search: String
    ): ResponseEntity<List<SeriesDTO>> {
       return this.search(search, 0, 5, "ASC", false)
    }

    override fun searchSeriesCycleAutocomplete(
        id: Long,
        search: String
    ): ResponseEntity<List<SeriesCycleDTO>> {
        val searchString = listOfNotNull(search, "series:$id").joinToString("|")
        val itemPage: Page<SeriesCycle> =
            this.seriesCycleBusiness.search(
                resolveSpecificationFromInfixExprSeriesCycle(searchString),
                0,
                5,
                Sort.Direction.ASC,
                false
            )

        return ResponseEntity(
            itemPage.stream().map(seriesCycleMapper::map).collect(Collectors.toCollection { LinkedList<SeriesCycleDTO>() }),
            HttpStatus.OK
        )
    }

    private fun search(
        search: String,
        page: Int,
        size: Int,
        direction: String,
        allResults: Boolean?
    ): ResponseEntity<List<SeriesDTO>> {
        val itemPage: Page<Series> =
            this.business.search(
                resolveSpecificationFromInfixExpr(search),
                page,
                size,
                Sort.Direction.fromString(direction),
                allResults!!
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::mapToSeriesBook).collect(Collectors.toCollection { LinkedList<SeriesDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/series"),
            HttpStatus.OK
        )
    }

    override fun updateSeries(id: Long, seriesDTO: SeriesDTO): ResponseEntity<SeriesDTO> {
        return if (id == seriesDTO.id!!.toLong()) {
            this.business.findById(id)
                .orElseThrow { throw ResourceNotFoundException("SERIES", id.toString()) }

            ResponseEntity.ok(mapper.mapToSeriesBook(this.business.save(this.mapper.mapToSeriesBook(seriesDTO))))
        } else {
            ResponseEntity.status(400).build()
        }
    }

    override fun deleteSeries(id: Long): ResponseEntity<Unit> {
        val series = this.business.findById(id)
            .orElseThrow { throw ResourceNotFoundException("SERIES", id.toString()) }

        this.business.delete(series)
        return if (this.business.findById(id).isPresent)
            ResponseEntity.status(500).build()
        else
            ResponseEntity.noContent().build()
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Series> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Series> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { SeriesSpecification(it!!) }
    }

    protected fun resolveSpecificationFromInfixExprSeriesCycle(searchParameters: String): Specification<SeriesCycle> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<SeriesCycle> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { SeriesCycleSpecification(it!!) }
    }
}

