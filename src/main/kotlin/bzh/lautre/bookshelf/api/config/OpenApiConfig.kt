package bzh.lautre.bookshelf.api.config

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.security.OAuthFlow
import io.swagger.v3.oas.annotations.security.OAuthFlows
import io.swagger.v3.oas.annotations.security.SecurityScheme

@OpenAPIDefinition(
    info = io.swagger.v3.oas.annotations.info.Info(
        title = "My App",
        description = "Some long and useful description",
        version = "v1"
    )
)
@SecurityScheme(
    name = "security_auth",
    type = SecuritySchemeType.OAUTH2,
    flows = OAuthFlows(
        authorizationCode = OAuthFlow(
            authorizationUrl = "\${bookshelf.oauth2.server.url}/realms/\${bookshelf.oauth2.server.realm}/protocol/openid-connect/auth",
            tokenUrl = "\${bookshelf.oauth2.server.url}/realms/\${bookshelf.oauth2.server.realm}/protocol/openid-connect/token",
        )
    )
)
open class OpenApiConfig
