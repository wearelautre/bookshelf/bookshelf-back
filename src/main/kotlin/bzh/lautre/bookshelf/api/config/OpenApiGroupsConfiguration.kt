package bzh.lautre.bookshelf.api.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityRequirement
import org.springdoc.core.utils.Constants
import org.springdoc.core.models.GroupedOpenApi
import org.springdoc.core.customizers.OpenApiCustomizer
import org.springdoc.core.customizers.OperationCustomizer
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.web.method.HandlerMethod

@Configuration
open class OpenApiGroupsConfiguration {

    @Bean
    @Profile("!prod")
    open fun actuatorApi(
        actuatorOpenApiCustomizer: OpenApiCustomizer?,
        actuatorCustomizer: OperationCustomizer,
        endpointProperties: WebEndpointProperties,
        @Value("\${springdoc.version}") appVersion: String
    ): GroupedOpenApi {
        return GroupedOpenApi.builder()
            .group("Actuator")
            .pathsToMatch(endpointProperties.basePath + Constants.ALL_PATTERN)
            .addOpenApiCustomizer(actuatorOpenApiCustomizer)
            .addOpenApiCustomizer { openApi: OpenAPI ->
                openApi.info(
                    Info().title("Actuator API").version(appVersion)
                )
            }
            .addOperationCustomizer(actuatorCustomizer)
            .pathsToExclude("/health/*")
            .build()
    }

    @Bean
    open fun usersGroup(@Value("\${springdoc.version}") appVersion: String): GroupedOpenApi {
        return GroupedOpenApi.builder().group("books")
            .addOperationCustomizer { operation: Operation, _: HandlerMethod ->
                operation.addSecurityItem(SecurityRequirement().addList("security_auth"))
                operation
            }
            .addOpenApiCustomizer { openApi: OpenAPI ->
                openApi.info(
                    Info().title("Books API").version(appVersion)
                )
            }
            .packagesToScan("bzh.lautre.bookshelf")
            .build()
    }

}