package bzh.lautre.bookshelf

import io.sentry.Sentry
import io.sentry.SentryOptions.TracesSamplerCallback
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.PropertySource

@PropertySource("classpath:versions.properties", "classpath:git.properties")
@SpringBootApplication
open class BookshelfBackApplication : SpringBootServletInitializer()

@Value("\${sentry.dsn}")
private var sentryDsn: String = ""

@Value("\${sentry.environment}")
private var sentryEnvironment: String = ""

fun main(args: Array<String>) {
    Sentry.init { options ->
        options.dsn = sentryDsn
        options.environment = sentryEnvironment
        options.tracesSampler = TracesSamplerCallback { context ->
            val ctx = context.customSamplingContext
            if (ctx != null) {
                when (ctx["url"]) {
                    "/health" -> 0.0
                    else -> 0.1
                }
            } else {
                0.1
            }
        }
    }

    SpringApplication.run(BookshelfBackApplication::class.java, *args)
}
