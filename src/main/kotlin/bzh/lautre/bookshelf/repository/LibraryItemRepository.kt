package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.BookReadStatusEnum
import bzh.lautre.bookshelf.model.LibraryItem
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface LibraryItemRepository : JpaRepository<LibraryItem, Long>, JpaSpecificationExecutor<LibraryItem> {
    fun findAllByAccountId(accountId: Long): List<LibraryItem>
    fun findAllByAccountIdAndStatusIn(accountId: Long, status: List<BookReadStatusEnum>): List<LibraryItem>
}