package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Borrower
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface BorrowerRepository : JpaRepository<Borrower, Long>, JpaSpecificationExecutor<Borrower> {
    fun findByName(name: String): Optional<Borrower>
}