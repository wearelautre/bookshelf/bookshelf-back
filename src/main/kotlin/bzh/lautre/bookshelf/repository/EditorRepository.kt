package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Editor
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface EditorRepository : JpaRepository<Editor, Long>, JpaSpecificationExecutor<Editor> {
    fun findByName(name: String): Optional<Editor>
}
