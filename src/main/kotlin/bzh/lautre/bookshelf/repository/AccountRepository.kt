package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Account
import bzh.lautre.bookshelf.model.AccountRoleEnum
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface AccountRepository : JpaRepository<Account, String> {
    fun countByRole(accountRoleEnum: AccountRoleEnum): Long
    fun findByEmail(email: String): Optional<Account>
}
