package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.LibraryItem
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface LibraryItemRepositoryCustom {
    fun getNextBookToRead(accountId: Int, page: Pageable): Page<LibraryItem>
}