package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.LibraryItem
import jakarta.persistence.EntityManager
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component


val nextUnreadItem = """
    select library_item.*
    from library_item,
             (select min(bs.series_tome) as min_tome, bs.series_id
              from book_series bs
                       join library_item li on bs.book_isbn = li.isbn
              where li.status in ('UNREAD', 'READING') and li.account_id = :accountId
              group by bs.series_id) as min_series
                 JOIN bookshelf.book_series bs1
                      on bs1.series_id = min_series.series_id and bs1.series_tome = min_series.min_tome
                 JOIN bookshelf.series s on bs1.series_id = s.id
        where library_item.isbn = bs1.book_isbn
        order by s.last_read_book_date desc   
""".trimIndent()

@Component
class LibraryItemRepositoryCustomImpl(
    private val entityManager: EntityManager
) : LibraryItemRepositoryCustom {

    override fun getNextBookToRead(
        accountId: Int, page: Pageable
    ): Page<LibraryItem> {

        val query = entityManager.createNativeQuery(nextUnreadItem, LibraryItem::class.java)
            .setParameter("accountId", accountId)
        val countQuery = entityManager.createNativeQuery("select count(*) from ( $nextUnreadItem ) as subquery;")
            .setParameter("accountId", accountId)

        query.firstResult = page.pageNumber * page.pageSize
        query.maxResults = page.pageSize


        return PageImpl(query.resultList as List<LibraryItem>, page, countQuery.singleResult as Long)


    }
}