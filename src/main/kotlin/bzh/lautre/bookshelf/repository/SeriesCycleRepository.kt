package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.SeriesCycle
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface SeriesCycleRepository : JpaRepository<SeriesCycle, Long>, JpaSpecificationExecutor<SeriesCycle> {
    fun getByName(name: String): Optional<SeriesCycle>

}