package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Artist
import jakarta.persistence.EntityManager
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Component

val noFilterQuery = """
    select artist_id as id, name
    from v_artist_series_role_count
    WHERE name like :name
    GROUP BY artist_id, name
    ORDER BY SUM(count) DESC,
        name
""".trimIndent()

val roleFilterQuery = """
    select artist_id as id, name
from (select artist_id,
             roles,
             name,
             SUM(count) as count,
             row_number() OVER (
                 partition by artist_id, name
                 ORDER BY
                     IF(roles = :roles, 0, 1),
                     SUM(vasrc.count) DESC,
                     vasrc.name
                 )      AS rn
      from v_artist_series_role_count as vasrc
      GROUP BY artist_id, roles, name) subquery
WHERE name like :name
  and rn = 1
GROUP BY artist_id, roles, name
ORDER BY IF(roles = :roles, 0, 1),
         subquery.count DESC,
         subquery.name
""".trimIndent()

val seriesFilterQuery = """
    select artist_id as id, name
from (select artist_id,
             series,
             name,
             SUM(count) as count,
             row_number() OVER (
                 partition by artist_id, name
                 ORDER BY
                     IF(series = :series, 0, 1),
                     SUM(vasrc.count) DESC,
                     vasrc.name
                 )      AS rn
      from v_artist_series_role_count as vasrc
      GROUP BY artist_id, series, name) subquery
WHERE name like :name
  and rn = 1
GROUP BY artist_id, series, name
ORDER BY IF(series = :series, 0, 1),
         subquery.count DESC,
         subquery.name
""".trimIndent()

val roleSeriesFilterQuery = """
    select artist_id as id, name
    from (select artist_id,
                 roles,
                 name,
                 series,
                 count,
                 row_number() OVER (
                     partition by artist_id
                     ORDER BY
                         IF(roles = :roles, 0, 1),
                         IF(series = :series, 0, 1),
                         count DESC,
                         name DESC
                     ) AS rn
          from v_artist_series_role_count) subquery
    WHERE name like :name
      AND rn = 1
    ORDER BY IF(subquery.roles = :roles, 0, 1),
             IF(subquery.series = :series, 0, 1),
             subquery.count DESC,
             subquery.name
""".trimIndent()

@Component
class ArtistRepositoryCustomImpl(
    private val entityManager: EntityManager
) : ArtistRepositoryCustom {
    override fun getArtistWithSpecificSort(
        name: String,
        withRolesFirst: String?,
        withSeriesFirst: String?,
        page: Pageable
    ): Page<Artist> {

        var query = entityManager.createNativeQuery(noFilterQuery, Artist::class.java)
            .setParameter("name", "%$name%")
        var countQuery = entityManager.createNativeQuery("select count(*) from ( $noFilterQuery ) as subquery;")
            .setParameter("name", "%$name%")
        if (withRolesFirst != null && withSeriesFirst != null) {
            query = entityManager.createNativeQuery(roleSeriesFilterQuery, Artist::class.java)
                .setParameter("name", "%$name%")
                .setParameter("roles", withRolesFirst)
                .setParameter("series", withSeriesFirst)
            countQuery = entityManager.createNativeQuery("select count(*) from ( $roleSeriesFilterQuery ) as subquery;")
                .setParameter("name", "%$name%")
                .setParameter("roles", withRolesFirst)
                .setParameter("series", withSeriesFirst)
        } else if (withSeriesFirst == null && withRolesFirst != null) {
            query = entityManager.createNativeQuery(roleFilterQuery, Artist::class.java)
                .setParameter("name", "%$name%")
                .setParameter("roles", withRolesFirst)
            countQuery = entityManager.createNativeQuery("select count(*) from ( $roleFilterQuery ) as subquery;")
                .setParameter("name", "%$name%")
                .setParameter("roles", withRolesFirst)
        } else if (withRolesFirst == null && withSeriesFirst != null) {
            query = entityManager.createNativeQuery(seriesFilterQuery, Artist::class.java)
                .setParameter("name", "%$name%")
                .setParameter("series", withSeriesFirst)
            countQuery = entityManager.createNativeQuery("select count(*) from ( $seriesFilterQuery ) as subquery;")
                .setParameter("name", "%$name%")
                .setParameter("series", withSeriesFirst)
        }

        query.firstResult = page.pageNumber * page.pageSize
        query.maxResults = page.pageSize

        return PageImpl(query.resultList as List<Artist>, page, countQuery.singleResult as Long)
    }
}