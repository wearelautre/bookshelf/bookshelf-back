package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Book
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface BookRepository : JpaRepository<Book, String>, JpaSpecificationExecutor<Book> {
    fun findByIsbn(isbn: String): Optional<Book>
    fun findAllByBookSeries_Series_BookType_Name(name: String): List<Book>
}
