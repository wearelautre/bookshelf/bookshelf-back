package bzh.lautre.bookshelf.exception

class BookAlreadyExistException(isbn: String) : Exception("The book [${isbn}] is already created")
