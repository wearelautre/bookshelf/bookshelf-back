package bzh.lautre.bookshelf.exception

class NotPossessedBookException(val id: String) : Exception("The book is sold")
