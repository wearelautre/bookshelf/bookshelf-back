package bzh.lautre.bookshelf.exception

class BadSearchParameterValueException(val parameter: String, val parameterValue: String) : Exception()