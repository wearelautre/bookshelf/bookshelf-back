package bzh.lautre.bookshelf.exception

import bzh.lautre.bookshelf.api.v1.model.ErrorDTO
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class ExceptionControllerAdvice {

    @ExceptionHandler
    fun handleResourceNotFound(ex: ResourceNotFoundException): ResponseEntity<ErrorDTO> {
        return ResponseEntity(
            ErrorDTO(
                HttpStatus.NOT_FOUND.value().toString(),
                "${ex.message}: ${ex.entity} ${ex.id}",
                "RESOURCE_NOT_FOUND"
            ),
            HttpStatus.NOT_FOUND
        )
    }

    @ExceptionHandler
    fun handleBookSold(ex: NotPossessedBookException): ResponseEntity<ErrorDTO> {
        return ResponseEntity(
            ErrorDTO(
                HttpStatus.BAD_REQUEST.value().toString(),
                "${ex.message}: ${ex.id}",
                "BOOK_SOLD"
            ),
            HttpStatus.BAD_REQUEST
        )
    }

    @ExceptionHandler
    fun handleBookAlreadyBorrowed(ex: BookAlreadyBorrowedException): ResponseEntity<ErrorDTO> {
        return ResponseEntity(
            ex.message?.let {
                ErrorDTO(
                    HttpStatus.CONFLICT.value().toString(), it, "BOOK_ALREADY_BORROWED"
                )
            },
            HttpStatus.CONFLICT
        )
    }

    @ExceptionHandler
    fun handleBookAlreadyCreated(ex: BookAlreadyExistException): ResponseEntity<ErrorDTO> {
        return ResponseEntity(
            ex.message?.let {
                ErrorDTO(
                    HttpStatus.CONFLICT.value().toString(), it, "BOOK_ALREADY_CREATED"
                )
            },
            HttpStatus.CONFLICT
        )
    }

    @ExceptionHandler
    fun handleIllegalStateException(ex: BadSearchParameterException): ResponseEntity<ErrorDTO> {
        return ResponseEntity(
            ErrorDTO(
                HttpStatus.BAD_REQUEST.value().toString(),
                "The search is not available on parameter [${ex.parameter}]",
                "WRONG_SEARCH_PARAMETER"
            ),
            HttpStatus.BAD_REQUEST
        )
    }

    @ExceptionHandler
    fun handleIllegalStateException(ex: BadSearchParameterValueException): ResponseEntity<ErrorDTO> {
        return ResponseEntity(
            ErrorDTO(
                HttpStatus.BAD_REQUEST.value().toString(),
                "${ex.parameterValue} is not a correct value for the parameter [${ex.parameter}]",
                "WRONG_SEARCH_PARAMETER_VALUE"
            ),
            HttpStatus.BAD_REQUEST
        )
    }

    @ExceptionHandler(value = [CustomInternalErrorException::class])
    fun handleInternalErrors(
        exception: CustomInternalErrorException,
        request: WebRequest
    ): ResponseEntity<ErrorDTO> {
        return ResponseEntity.status(500).body(
            ErrorDTO(
                HttpStatus.INTERNAL_SERVER_ERROR.toString(),
                exception.message!!,
                exception.error
            )
        )
    }
}