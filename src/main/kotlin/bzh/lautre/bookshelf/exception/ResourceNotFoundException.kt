package bzh.lautre.bookshelf.exception

class ResourceNotFoundException(val entity: String, val id: String) : Exception("The resource doesn't exists")
