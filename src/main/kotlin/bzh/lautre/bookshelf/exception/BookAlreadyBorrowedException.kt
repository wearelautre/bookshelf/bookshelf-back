package bzh.lautre.bookshelf.exception

class BookAlreadyBorrowedException(val isbn: String) : Exception("The book [${isbn}] is already borrowed")
