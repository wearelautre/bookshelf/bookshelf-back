package bzh.lautre.bookshelf.exception

class BadSearchParameterException(val parameter: String, val errorDetail: String) : Exception()