package bzh.lautre.bookshelf.exception

class BadParameterException(message: String) : CustomInternalErrorException(message, "BAD_PARAMETER")
