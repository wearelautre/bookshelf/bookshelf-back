package bzh.lautre.bookshelf.business

import org.springframework.core.io.Resource

interface CoverBusiness {
    fun saveCover(coverFilename: String?): String?
    fun uploadCover(file: Resource, filename: String): Boolean
    fun delete(coverFilename: String?): Boolean
}
