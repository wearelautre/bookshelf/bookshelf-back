package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.Book
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import java.util.*

interface BookBusiness : EntityBusiness<Book, String> {

    val allBooks: List<Book>

    fun search(
        specs: Specification<Book>,
        page: Int = 0,
        size: Int = 5,
        direction: Sort.Direction,
        sort: BookSortField
    ): Page<Book>

    fun getByBookType(name: String): List<Book>
    fun getLastAdded(page: Int = 0, size: Int = 5): Page<Book>
    fun findByIsbn(isbn: String): Optional<Book>
    fun getAllByIsbn(isbns: List<String>): List<Book>
    fun getByIsbn(isbn: String): Book
}
