package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.Editor
import org.springframework.stereotype.Component
import java.util.*

@Component
interface EditorBusiness: EntityBusiness<Editor, Long> {
    val allEditors: List<Editor>

    fun findByName(name: String): Optional<Editor>
    fun getByName(name: String): Editor
    fun findEditorByIdOrName(id: Long?, name: String): Editor
}
