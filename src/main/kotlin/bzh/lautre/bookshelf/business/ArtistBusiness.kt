package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.business.model.BooksByRole
import bzh.lautre.bookshelf.model.Artist
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Component
interface ArtistBusiness: EntityBusiness<Artist, Long> {
    fun getBooksByRole(id: Long): List<BooksByRole>
    fun searchWithMultipleOrder(
        name: String = "",
        page: Int = 0,
        size: Int = 5,
        withRolesFirst: String?,
        withSeriesFirst: String?
    ): Page<Artist>{
        TODO("Not yet implemented")
    }
}
