package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.Series
import org.springframework.data.domain.Page

interface SeriesBusiness : EntityBusiness<Series, Long> {

    val allSeries: List<Series>
    val count: Long

    fun getUnfinishedSeriesForAccount(accountId: Long, page: Int, size: Int): Page<Series>
}
