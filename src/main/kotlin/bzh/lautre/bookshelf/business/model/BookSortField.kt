package bzh.lautre.bookshelf.business.model


enum class BookSortField {
    EDITOR, SERIES
}
