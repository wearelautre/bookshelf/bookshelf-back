package bzh.lautre.bookshelf.business

import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
interface EntityBusiness<T, ID> {

    fun search(
        specs: Specification<T>,
        page: Int = 0,
        size: Int = 5,
        direction: Sort.Direction = Sort.Direction.ASC,
        allResults: Boolean = false
    ): Page<T>{
        TODO("Not yet implemented")
    }
    fun search(
        specs: Specification<T>,
        page: Int = 0,
        size: Int = 5,
        direction: Sort.Direction = Sort.Direction.ASC,
        sort: String
    ): Page<T>{
        TODO("Not yet implemented")
    }
    fun save(t: T): T{
        TODO("Not yet implemented")
    }
    fun delete(t: T): Boolean{
        TODO("Not yet implemented")
    }
    fun deleteById(id: ID): Boolean {
        TODO("Not yet implemented")
    }
    fun findById(id: ID): Optional<T>{
        TODO("Not yet implemented")
    }

}
