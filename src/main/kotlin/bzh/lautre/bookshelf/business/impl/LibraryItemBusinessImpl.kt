package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.LibraryItemBusiness
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.BookReadStatusEnum
import bzh.lautre.bookshelf.model.LibraryItem
import bzh.lautre.bookshelf.repository.LibraryItemRepository
import bzh.lautre.bookshelf.repository.LibraryItemRepositoryCustom
import bzh.lautre.bookshelf.specification.LibraryItemSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class LibraryItemBusinessImpl(
    private val libraryItemRepository: LibraryItemRepository,
    private val libraryItemRepositoryCustom: LibraryItemRepositoryCustom,
) : LibraryItemBusiness {

    override fun allItems(accountId: Int): List<LibraryItem> {
        return this.libraryItemRepository.findAllByAccountId(accountId.toLong())
    }

    override fun allReadItems(accountId: Int): List<LibraryItem> {
        return this.libraryItemRepository.findAllByAccountIdAndStatusIn(
            accountId.toLong(), listOf(BookReadStatusEnum.READ)
        )
    }

    override fun search(
        specs: Specification<LibraryItem>,
        page: Int,
        size: Int,
        direction: Sort.Direction,
        sort: BookSortField
    ): Page<LibraryItem> {
        return this.libraryItemRepository.findAll(
            specs.and(LibraryItemSpecification.orderLibraryItemByBooks(direction, sort)),
            PageRequest.of(page, size)
        )
    }

    override fun getLastAdded(specs: Specification<LibraryItem>, page: Int, size: Int): Page<LibraryItem> {
        return this.libraryItemRepository.findAll(
            specs.and(LibraryItemSpecification.orderBy("additionDate", false)),
            PageRequest.of(page, size)
        )
    }

    override fun getNextBookToRead(accountId: Int, page: Int, size: Int): Page<LibraryItem> {
        return this.libraryItemRepositoryCustom.getNextBookToRead(
            accountId,
            PageRequest.of(page, size)
        )
    }

    override fun findById(id: Long): Optional<LibraryItem> {
        return this.libraryItemRepository.findById(id)
    }

    override fun save(libraryItem: LibraryItem): LibraryItem {
        return this.libraryItemRepository.save(libraryItem)
    }
}


