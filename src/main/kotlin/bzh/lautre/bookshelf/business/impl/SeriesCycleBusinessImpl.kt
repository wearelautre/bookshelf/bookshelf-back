package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.SeriesCycleBusiness
import bzh.lautre.bookshelf.model.SeriesCycle
import bzh.lautre.bookshelf.repository.SeriesCycleRepository
import bzh.lautre.bookshelf.specification.SeriesCycleSpecification
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Component
@Transactional
open class SeriesCycleBusinessImpl(
    private val repository: SeriesCycleRepository,
) : SeriesCycleBusiness {

    override fun search(
        specs: Specification<SeriesCycle>,
        page: Int,
        size: Int,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<SeriesCycle> {
        return this.repository.findAll(
            specs.and(SeriesCycleSpecification(SpecSearchCriteria()).orderBy(direction, "name")),
            if (allResults) {
                Pageable.unpaged()
            } else {
                PageRequest.of(page, size)
            }
        )
    }

    override fun save(t: SeriesCycle): SeriesCycle {
        t.name = t.name.trim()
        return if (t.id != null) {
            try {
                Optional.of(repository.save(t))
            } catch (e: DataIntegrityViolationException) {
                repository.getByName(t.name)
            }
        } else {
            repository.getByName(t.name)
        }.orElseGet { this.repository.save(t) }
    }
}
