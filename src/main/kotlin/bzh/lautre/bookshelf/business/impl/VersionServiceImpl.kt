package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.VersionService
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class VersionServiceImpl: VersionService {

    private final val PIPELINE_ID_PLACEHOLDER = "@env.CI_PIPELINE_ID@"
    private final val JOB_ID_PLACEHOLDER = "@env.CI_JOB_ID@"

    @Value("\${git.tags}")
    private val tags: List<String> = emptyList()

    @Value("\${ci-pipeline-id}")
    private val pipelineId: String = "0"

    @Value("\${ci-job-id}")
    private val jobId: String = "0"

    @Value("\${git.commit.id.abbrev}")
    private val commitSha: String = "0"

    override fun getVersionInformation(): VersionService.VersionInformation {
        val pipelineId = if (this.pipelineId == PIPELINE_ID_PLACEHOLDER) {
            "0"
        } else {
            this.pipelineId
        }

        val jobId = if (this.jobId == JOB_ID_PLACEHOLDER) {
            "0"
        } else {
            this.jobId
        }

        return VersionService.VersionInformation(
            tags.firstOrNull() ?: commitSha,
            pipelineId,
            jobId,
            commitSha
        )
    }
}
