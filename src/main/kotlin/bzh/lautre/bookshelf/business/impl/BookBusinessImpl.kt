package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.BookBusiness
import bzh.lautre.bookshelf.business.CoverBusiness
import bzh.lautre.bookshelf.business.TaskBusiness
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.PossessionStatusEnum
import bzh.lautre.bookshelf.repository.BookRepository
import bzh.lautre.bookshelf.specification.BookSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class BookBusinessImpl(
    private val bookRepository: BookRepository,
    private val coverBusiness: CoverBusiness,
    private val taskBusiness: TaskBusiness
) : BookBusiness {

    override fun search(
        specs: Specification<Book>,
        page: Int,
        size: Int,
        direction: Sort.Direction,
        sort: BookSortField
    ): Page<Book> {
        return this.bookRepository.findAll(
            specs.and(BookSpecification.orderBooks(direction, sort)),
            PageRequest.of(page, size)
        )
    }

    override fun getLastAdded(page: Int, size: Int): Page<Book> {
        return this.bookRepository.findAll(
            BookSpecification(
                SpecSearchCriteria(
                    null,
                    "possessionStatus",
                    SearchOperation.NEGATION,
                    PossessionStatusEnum.NOT_POSSESSED
                )
            ).and(
                BookSpecification.orderBy("creationDate", false)
            ),
            PageRequest.of(page, size)
        )
    }

    override val allBooks: List<Book>
        get() = this.bookRepository.findAll()

    override fun save(t: Book): Book {
        t.isbn = t.isbn?.trim()
        t.arkId = t.arkId?.trim()
        t.title = t.title?.trim()
        t.year = t.year?.trim()
        t.collection = t.collection?.trim()

        t.cover = this.coverBusiness.saveCover(t.cover)

        val createdBook = bookRepository.save(t)
        taskBusiness.cleanAddBookTask(createdBook.isbn!!)
        return createdBook
    }

    override fun findByIsbn(isbn: String): Optional<Book> {
        return bookRepository.findByIsbn(isbn)
    }

    override fun getByBookType(name: String): List<Book> {
        return this.bookRepository.findAllByBookSeries_Series_BookType_Name(name)
    }

    override fun findById(id: String): Optional<Book> {
        return this.bookRepository.findById(id)
    }

    override fun getAllByIsbn(isbns: List<String>): List<Book> {
        return this.bookRepository.findAllById(isbns)
    }

    override fun getByIsbn(isbn: String): Book {
        return this.bookRepository.getReferenceById(isbn)
    }

    override fun delete(t: Book): Boolean {
        this.bookRepository.delete(t)
        return if (this.findById(t.isbn!!).isPresent) {
            this.coverBusiness.delete(t.cover!!)
        } else {
            false
        }
    }
}


