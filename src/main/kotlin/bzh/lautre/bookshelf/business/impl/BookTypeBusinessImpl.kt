package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.repository.BookTypeRepository
import bzh.lautre.bookshelf.specification.BookTypeSpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.*


@Component
class BookTypeBusinessImpl @Autowired
constructor(
    private val repository: BookTypeRepository
) : BookTypeBusiness {
    override val count: Long
        get() = this.repository.count()

    override fun getTop3BookTypesInBooksNumber(): List<BookType> {
        return this.repository.findAll(BookTypeSpecification.getAllOrderByBookCount())
    }

    override val allBookTypes: List<BookType>
        get() = this.repository.findAll()

    override fun search(
        specs: Specification<BookType>,
        page: Int,
        size: Int,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<BookType> {
        return this.repository.findAll(
            specs.and(BookTypeSpecification(SpecSearchCriteria()).orderBy(direction, "name")),
            if (allResults) {
                Pageable.unpaged()
            } else {
                PageRequest.of(page, size)
            }
        )

    }

    override fun getBookTypesForAccount(accountId: Int): List<BookType> {
        return this.repository.findAll(BookTypeSpecification.getAllOrderByBookCountForAccount(accountId))
    }

    override fun getTop3BookTypesInBooksNumber(accountId: Int): Page<BookType> {
        return this.repository.findAll(
            BookTypeSpecification.distinct().and(BookTypeSpecification.getAllOrderByBookCountForAccount(accountId)),
            PageRequest.of(0, 3)
        )
    }

    override fun getByName(name: String): BookType {
        return this.repository.findBookTypeByName(name)
            .orElseGet { BookType(name) }
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    override fun save(t: BookType): BookType {
        t.name = t.name.trim()
        return if (t.id != null) {
            try {
                Optional.of(repository.save(t))
            } catch (e: DataIntegrityViolationException) {
                repository.findBookTypeByName(t.name)
            }
        } else {
            repository.findBookTypeByName(t.name)
        }.orElseGet {
            this.repository.save(t)
        }
    }

    override fun findById(id: Long): Optional<BookType> {
        return this.repository.findById(id)
    }

    override fun delete(t: BookType): Boolean {
        this.repository.delete(t)
        return this.findById(t.id!!).isPresent
    }
}

