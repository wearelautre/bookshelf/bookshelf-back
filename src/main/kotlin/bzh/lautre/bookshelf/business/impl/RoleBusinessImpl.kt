package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.RoleBusiness
import bzh.lautre.bookshelf.model.Role
import bzh.lautre.bookshelf.repository.RoleRepository
import bzh.lautre.bookshelf.specification.RoleSpecification
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class RoleBusinessImpl(
    private val roleRepository: RoleRepository
) : RoleBusiness {

    override fun search(
        specs: Specification<Role>,
        page: Int,
        size: Int,
        direction: Sort.Direction,
        sort: String
    ): Page<Role> {
        val test = specs.and(RoleSpecification.orderRoles(direction, sort))

        return this.roleRepository.findAll(test, PageRequest.of(page, size))
    }

    override fun findById(id: Long): Optional<Role> {
        return this.roleRepository.findById(id)
    }

    override fun save(t: Role): Role {
        return if (t.id != null) {
            try {
                Optional.of(roleRepository.save(t))
            } catch (e: DataIntegrityViolationException) {
                roleRepository.findByName(t.name!!)
            }
        } else {
            roleRepository.findByName(t.name!!)
        }.orElseGet { roleRepository.save(t) }
    }
}
