package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.*
import bzh.lautre.bookshelf.model.*
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional
class BookMacroBusinessImpl(
    private val bookBusiness: BookBusiness,
    private val bookTypeBusiness: BookTypeBusiness,
    private val editorBusiness: EditorBusiness,
    private val seriesBusiness: SeriesBusiness,
    private val artistBusiness: ArtistBusiness,
    private val roleBusiness: RoleBusiness,
    private val seriesCycleBusiness: SeriesCycleBusiness,
    private val contractBusiness: ContractBusiness
) : BookMacroBusiness {
    override fun macroSaveBook(
        series: Series?,
        bookSeries: BookSeries?,
        seriesCycle: SeriesCycle?,
        book: Book,
        contracts: List<Contract>
    ): Book {
        book.metadata?.book = createSeries(series, bookSeries, seriesCycle, book)

        val createdBook = this.bookBusiness.save(book)

        if (contracts.isNotEmpty()) {
            createdBook.contracts.clear()
            createdBook.contracts.addAll(contracts.map {
                contractBusiness.save(createContract(it, createdBook))
            })
        }

        return createdBook
    }

    private fun createContract(contract: Contract, createdBook: Book): Contract {
        return Contract(
            roleBusiness.save(contract.role),
            contract.artists.map { artistBusiness.save(it) },
            createdBook
        )
    }

    fun createSeries(
        series: Series?,
        bookSeries: BookSeries?,
        seriesCycle: SeriesCycle?,
        book: Book
    ): Book {
        if (series != null) {
            series.bookType = bookTypeBusiness.save(series.bookType)
            series.editor = editorBusiness.save(series.editor)
            val dbSeries: Series = seriesBusiness.save(series)

            if (bookSeries != null) {
                bookSeries.series = dbSeries
                dbSeries.bookList.add(bookSeries)
            }

            if (seriesCycle != null) {
                seriesCycle.series = dbSeries
                val dbSeriesCycle: SeriesCycle = seriesCycleBusiness.save(seriesCycle)
                if (bookSeries != null) {
                    bookSeries.seriesCycle = dbSeriesCycle
                }
            }
        }

        if (bookSeries != null) {
            bookSeries.book = book
            bookSeries.isbn = book.isbn
            book.bookSeries = bookSeries
        }

        return book
    }
}