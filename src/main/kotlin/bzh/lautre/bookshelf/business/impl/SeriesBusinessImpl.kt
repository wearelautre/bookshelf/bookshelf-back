package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.SeriesBusiness
import bzh.lautre.bookshelf.exception.ImpossibleSeriesSave
import bzh.lautre.bookshelf.model.BookReadStatusEnum
import bzh.lautre.bookshelf.model.PossessionStatusEnum
import bzh.lautre.bookshelf.model.Series
import bzh.lautre.bookshelf.repository.SeriesRepository
import bzh.lautre.bookshelf.specification.SeriesSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class SeriesBusinessImpl(
    private val seriesRepository: SeriesRepository
) : SeriesBusiness {

    override fun search(
        specs: Specification<Series>,
        page: Int,
        size: Int,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<Series> {
        return this.seriesRepository.findAll(
            specs.and(SeriesSpecification(SpecSearchCriteria()).orderBy(direction, "name")),
            if (allResults) {
                Pageable.unpaged()
            } else {
                PageRequest.of(page, size)
            }
        )
    }

    override fun findById(id: Long): Optional<Series> {
        return seriesRepository.findById(id)
    }

    override val allSeries: List<Series>
        get() = seriesRepository.findAll()

    override val count: Long
        get() = this.seriesRepository.count()


    override fun getUnfinishedSeriesForAccount(accountId: Long, page: Int, size: Int): Page<Series> {
        val inAccountLibrary = SeriesSpecification(
            SpecSearchCriteria(
                null,
                "libAccount",
                SearchOperation.EQUALITY,
                accountId
            )
        )
        val notRead = SeriesSpecification(
            SpecSearchCriteria(
                null,
                "bookStatus",
                SearchOperation.NEGATION,
                BookReadStatusEnum.READ
            )
        )
        val notSold = SeriesSpecification(
            SpecSearchCriteria(
                null,
                "bookPossessionStatus",
                SearchOperation.NEGATION,
                PossessionStatusEnum.NOT_POSSESSED
            )
        )

        val sort = SeriesSpecification.distinct().and(
            SeriesSpecification(SpecSearchCriteria()).orderBy(
                Sort.Direction.DESC,
                "lastReadBookDate"
            )
        )

        return this.seriesRepository.findAll(
            Specification.where(inAccountLibrary.and(notSold.and(notRead)).and(sort)), PageRequest.of(page, size)
        )
    }

    override fun save(t: Series): Series {
        t.name = t.name.trim()

        val optional: Optional<Series> = if (t.id != null) {
            seriesRepository.findById(t.id!!)
        } else {
            seriesRepository.findByNameAndEditorAndBookType(t.name, t.editor, t.bookType)
        }

        return if (optional.isPresent) {
            val dbSeries = optional.get()
            if (!t.oneShot && dbSeries.oneShot && dbSeries.bookList.size > 1) {
                throw ImpossibleSeriesSave("Can't switch the series to One Shot due to the number of book on the series")
            }
            if (t.id == null) {
                t.id = dbSeries.id
            }

            seriesRepository.save(t)
        } else {
            seriesRepository.save(t)
        }
    }

    override fun delete(t: Series): Boolean {
        this.seriesRepository.delete(t)
        return this.findById(t.id!!).isPresent
    }
}
