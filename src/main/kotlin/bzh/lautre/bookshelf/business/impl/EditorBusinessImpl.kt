package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.EditorBusiness
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.repository.EditorRepository
import bzh.lautre.bookshelf.specification.EditorSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class EditorBusinessImpl(
    private val repository: EditorRepository
) : EditorBusiness {

    override val allEditors: List<Editor>
        get() = repository.findAll()

    override fun search(
        specs: Specification<Editor>,
        page: Int,
        size: Int,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<Editor> {
        return this.repository.findAll(
            specs.and(EditorSpecification(SpecSearchCriteria()).orderBy(direction, "name")),
            if (allResults) {
                Pageable.unpaged()
            } else {
                PageRequest.of(page, size)
            }
        )
    }

    override fun findByName(name: String): Optional<Editor> {
        return repository.findByName(name)
    }

    override fun getByName(name: String): Editor {
        return findByName(name).orElseGet { Editor(name) }
    }

    override fun save(t: Editor): Editor {
        t.name = t.name.trim()
        return repository.findByName(t.name.trim())
            .orElseGet { repository.save(t) }
    }

    override fun findEditorByIdOrName(id: Long?, name: String): Editor {
        return if (id != null) {
            this.findById(id).orElseGet { this.getByName(name) }
        } else {
            this.getByName(name)
        }
    }

    override fun findById(id: Long): Optional<Editor> {
        return repository.findById(id)
    }
}
