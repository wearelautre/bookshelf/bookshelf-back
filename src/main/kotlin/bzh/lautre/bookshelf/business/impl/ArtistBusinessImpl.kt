package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.ArtistBusiness
import bzh.lautre.bookshelf.business.BookBusiness
import bzh.lautre.bookshelf.business.RoleBusiness
import bzh.lautre.bookshelf.business.model.BooksByRole
import bzh.lautre.bookshelf.exception.ResourceNotFoundException
import bzh.lautre.bookshelf.model.Artist
import bzh.lautre.bookshelf.repository.ArtistRepository
import bzh.lautre.bookshelf.specification.ArtistSpecification
import jakarta.transaction.Transactional
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*


@Component
class ArtistBusinessImpl(
    private val artistRepository: ArtistRepository,
    private val roleBusiness: RoleBusiness,
    private val bookBusiness: BookBusiness
) : ArtistBusiness {

    override fun search(
        specs: Specification<Artist>,
        page: Int,
        size: Int,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<Artist> {
        return this.artistRepository.findAll(
            specs.and(ArtistSpecification(SpecSearchCriteria("", "name", SearchOperation.EQUALITY, "batou")).orderBy(direction, "name")),
            if (allResults) {
                Pageable.unpaged()
            } else {
                PageRequest.of(page, size)
            }
        )
    }

    override fun searchWithMultipleOrder(
        name: String,
        page: Int,
        size: Int,
        withRolesFirst: String?,
        withSeriesFirst: String?
    ): Page<Artist> {
        return artistRepository.getArtistWithSpecificSort(
            name,
            withRolesFirst,
            withSeriesFirst,
            PageRequest.of(page, size)
        )
    }

    @Transactional
    override fun save(t: Artist): Artist {
        t.webLinks.forEach { it.artists = t }
        return if (t.id != null) {
            try {
                Optional.of(artistRepository.save(t))
            } catch (e: DataIntegrityViolationException) {
                artistRepository.findByName(t.name!!)
            }
        } else {
            artistRepository.findByName(t.name!!)
        }
            .orElseGet { artistRepository.save(t) }
    }

    override fun findById(id: Long): Optional<Artist> {
        return artistRepository.findById(id)
    }

    override fun delete(t: Artist): Boolean {
        this.artistRepository.delete(t)
        return this.findById(t.id!!).isPresent
    }

    override fun getBooksByRole(id: Long): List<BooksByRole> {
        val artist = this.findById(id)
        if (artist.isEmpty) {
            throw ResourceNotFoundException("ARTIST", id.toString())
        }

        val anotherMap = mutableMapOf<Long, MutableList<String>>()
        artist.get().contracts
            .forEach {
                if (anotherMap.containsKey(it.id.roleId)) {
                    anotherMap[it.id.roleId!!]!!.add(it.id.bookIsbn!!)
                } else {
                    anotherMap[it.id.roleId!!] = mutableListOf(it.id.bookIsbn!!)
                }
            }
        return anotherMap.entries.map { (roleId, bookIsbns) ->
            BooksByRole(
                this.roleBusiness.findById(roleId).get(),
                this.bookBusiness.getAllByIsbn(bookIsbns)
            )
        }
    }
}
