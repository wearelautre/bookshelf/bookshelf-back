package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.LibraryItem
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification

interface LibraryItemBusiness : EntityBusiness<LibraryItem, Long> {

    fun allItems(accountId: Int): List<LibraryItem>
    fun allReadItems(accountId: Int): List<LibraryItem>

    fun search(
        specs: Specification<LibraryItem>,
        page: Int = 0,
        size: Int = 5,
        direction: Sort.Direction,
        sort: BookSortField
    ): Page<LibraryItem>

    fun getLastAdded(specs: Specification<LibraryItem>, page: Int = 0, size: Int = 5): Page<LibraryItem>

    fun getNextBookToRead(accountId: Int, page: Int = 0, size: Int = 5): Page<LibraryItem>
}
