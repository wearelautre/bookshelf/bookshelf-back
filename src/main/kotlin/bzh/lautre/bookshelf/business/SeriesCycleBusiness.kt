package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.SeriesCycle

interface SeriesCycleBusiness: EntityBusiness<SeriesCycle, Long>