package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.BookType
import org.springframework.data.domain.Page

interface BookTypeBusiness : EntityBusiness<BookType, Long> {

    val count: Long
    val allBookTypes: List<BookType>
    fun getByName(name: String): BookType
    fun getTop3BookTypesInBooksNumber(): List<BookType>
    fun getTop3BookTypesInBooksNumber(accountId: Int): Page<BookType>
    fun getBookTypesForAccount(accountId: Int): List<BookType>
}
