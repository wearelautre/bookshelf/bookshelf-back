package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.*

interface BookMacroBusiness {
    fun macroSaveBook(
        series: Series?,
        bookSeries: BookSeries?,
        seriesCycle: SeriesCycle?,
        book: Book,
        contracts: List<Contract>
    ): Book
}