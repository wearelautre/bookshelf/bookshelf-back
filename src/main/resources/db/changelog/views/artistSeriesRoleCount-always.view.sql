select row_number() over ()              AS `id`,
       `a`.`id`                          AS `artist_id`,
       `a`.`name`                        AS `name`,
       `s`.`name`                        AS `series`,
       `r`.`name`                        AS `roles`,
       count(`ca`.`contracts_book_isbn`) AS `count`
from ((((`artist` `a` join `contract_artists` `ca` on ((`a`.`id` = `ca`.`artists_id`))) join `book` `b` on ((`ca`.`contracts_book_isbn` = `b`.`isbn`))) join `role` `r` on ((`ca`.`contracts_role_id` = `r`.`id`)))
    join `book_series` `bs` on ((`b`.`isbn` = `bs`.`book_isbn`))
    join `series` `s` on ((`bs`.`series_id` = `s`.`id`)))
group by `a`.`id`, `a`.`name`, `s`.`name`, `r`.`name`;
