create trigger series_update
    after update
    on library_item
    for each row
BEGIN
    IF (OLD.status != 'READ' AND NEW.status = 'READ') THEN
        UPDATE series s
            INNER JOIN book_series bs on NEW.isbn = bs.book_isbn
        SET s.last_read_book_date = NEW.last_update_date
        WHERE s.id = bs.series_id;
    END IF;
END;
